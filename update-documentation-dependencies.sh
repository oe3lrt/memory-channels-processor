#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

APP_NPM="$(command -v npm)" || true
APP_NPX="$(command -v npx)" || true

#/////////////////////////

if [ ! -e "$APP_NPM" ] ; then
    echo "The executable 'npm' wasn't found!"
    exit
fi

if [ ! -e "$APP_NPX" ] ; then
    echo "The executable 'npx' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Update dependencies defined in 'package.json'"
$APP_NPM update
$APP_NPM ci
echo "--------------------"

echo "Done"
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////