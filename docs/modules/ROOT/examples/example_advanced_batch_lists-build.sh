#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

APP_MCP="$(command -v memory-channels-processor)"

#/////////////////////////

set -eu

#/////////////////////////

if [ ! -e "$APP_MCP" ] ; then
    echo "The executable 'memory-channels-processor' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Building codeplugs..."

echo "- Build CSV intermediate files..."

# Repeaters
$APP_MCP --output-file "mcp_tmp_repeaters.csv" --source "oevsv-repeater-db" --band "70cm" --band "2m" --type "fm" --type "d-star" --output-format="csv"

# Channels
$APP_MCP --output-file "mcp_tmp_channels.csv" --source "fm-channels-iaru-r1" --band "2m" --type "fm" --output-format="csv"

# SOTA
$APP_MCP --output-file "mcp_tmp_gps_sota.csv" --source "sota-summits" --output-format="csv" --locator "JN88EF" --country "AUT" --country "HUN" --country "CZE" --country "SVK" --distance-max 65


echo "- Build Icom files..."

# 2m + 70cm
$APP_MCP --output-file "mcp_icom_fm_channels.csv" --source "csv" --csv-input-file "mcp_tmp_channels.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "20" --icom-group-name "Kanäle" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name"

$APP_MCP --output-file "mcp_icom_fm_repeaters_oe1.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "21" --icom-group-name "Wien" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name" --filter "callsign~=OE1"
$APP_MCP --output-file "mcp_icom_fm_repeaters_oe2.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "22" --icom-group-name "Salzburg" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name" --filter "callsign~=OE2"
$APP_MCP --output-file "mcp_icom_fm_repeaters_oe3.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "23" --icom-group-name "Niederösterreich" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name" --filter "callsign~=OE3"
$APP_MCP --output-file "mcp_icom_fm_repeaters_oe4.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "24" --icom-group-name "Burgenland" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name" --filter "callsign~=OE4"
$APP_MCP --output-file "mcp_icom_fm_repeaters_oe5.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "25" --icom-group-name "Oberösterreich" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name" --filter "callsign~=OE5"
$APP_MCP --output-file "mcp_icom_fm_repeaters_oe6.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "26" --icom-group-name "Steiermark" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name" --filter "callsign~=OE6"
$APP_MCP --output-file "mcp_icom_fm_repeaters_oe7.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "27" --icom-group-name "Tirol" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name" --filter "callsign~=OE7"
$APP_MCP --output-file "mcp_icom_fm_repeaters_oe8.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "28" --icom-group-name "Kärnten" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name" --filter "callsign~=OE8"
$APP_MCP --output-file "mcp_icom_fm_repeaters_oe9.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "29" --icom-group-name "Vorarlberg" --icom-type="fm" --sort "freq_rx" --sort "callsign" --sort "name" --filter "callsign~=OE9"

$APP_MCP --output-file "mcp_icom_d-star_repeaters.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --band "2m" --band "70cm" --type "d-star" --output-format="icom" --name-format="name" --icom-group-number "21" --icom-group-name "Austria D-STAR" --icom-type="d-star" --sort "callsign" --sort "freq_rx" --sort "name"
$APP_MCP --output-file "mcp_icom_d-star+fm_repeaters.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --band "2m" --band "70cm" --type "d-star" --type "fm" --output-format="icom" --name-format="name" --icom-group-number "21" --icom-group-name "Austria FM+DSTAR" --icom-type="d-star" --sort "freq_rx" --sort "name" --sort "callsign"

# SOTA
$APP_MCP --output-file "mcp_icom_gps_sota.csv" --source "csv" --csv-input-file "mcp_tmp_gps_sota.csv" --output-format="icom" --icom-group-number "3" --icom-group-name "SOTA Wien" --icom-type="gps" --sort "name" --sort "callsign" --name-format "custom" --name-format-custom "{{ remove_prefix(remove_spaces(callsign), 'OE/') + ' ' + name }}"
$APP_MCP --output-file "mcp_icom_gps_sota.html" --source "csv" --csv-input-file "mcp_tmp_gps_sota.csv" --output-format="map" --name-format="name"


echo "- Build Chirp files..."

$APP_MCP --output-file "mcp_chirp_fm_repeaters.csv" --source "csv" --csv-input-file "mcp_tmp_repeaters.csv" --csv-input-file "mcp_tmp_channels.csv" --type "fm" --output-format="chirp" --name-format "7-char" --sort "freq_rx" --sort "callsign" --sort "name"


echo "Done"
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////