#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

APP_ASCIINEMA="$(command -v asciinema)" || true

#/////////////////////////

if [ ! -e "$APP_ASCIINEMA" ] ; then
    echo "The executable 'asciinema' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Export variables"
export PROMPT="$ "
export VERSION=" "

echo "Generate screencasts"
screencast="$SD/../attachments/demo.cast" "$SD/asciinema-rec_script.sh" "$SD/demo.sh"

echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////