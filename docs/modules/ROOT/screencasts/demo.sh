#!/usr/bin/env bash

# Get some 70cm FM repeaters in Vienna (filter for callsigns containing 'OE1XF')
memory-channels-processor --source "oevsv-repeater-db" --band 70cm --type "fm" --output-format="csv" --filter "callsign~=OE1XF"

# Get some 2m FM simplex frequencies (filter for names containing 'S2')
memory-channels-processor --source "fm-channels-iaru-r1" --band "2m" --type "fm" --output-format="csv" --filter "name~=S2"

# Get version information
memory-channels-processor --version
