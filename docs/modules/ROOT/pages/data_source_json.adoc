= JSON (JavaScript Object Notation)
:navtitle: JSON

Der Import im JSON-Format (JavaScript Object Notation) ermöglicht die Weiterverarbeitung von Daten, die von externen Programmen bereitgestellt werden.
So kann auch eine vom `Memory Channels Processor` exportierte Datei wieder als Datenquelle verwendet werden.
Das ist vor zum Beispiel dann nützlich, wenn man eigene Einträge mit den Daten anderer Quellen mischen will.

Eine Beschreibung zum Export findet man xref:data_target_json.adoc[hier].


:data-source: JSON
:data-source-id: json
:data-source-provider!:
:data-source-type: dynamisch
:data-source-mode!:
:data-source-license!:
:data-source-url!:
:data-source-note!:
include::partial$data_source_box.adoc[]


[#examples]
== Beispiele

.Beispiel Kommandos für JSON-Importe
[source%linenums,bash]
----
# FM: TSV
memory-channels-processor --source "json" --csv-input-file "local-data.json" --band 70cm --band 2m --type "fm" --output-format="csv"

# FM: ÖVSV Repeater-DB
memory-channels-processor --source "oevsv-repeater-db" --source "json" --json-input-file "additional-items.json" --band 70cm --type "fm" --output-file "fm_70cm_gen_oevsv-repeater-db-additional.json" --output-format="json"
memory-channels-processor --source "oevsv-repeater-db" --source "json" --json-input-file "additional-items.json" --band 2m --type "fm" --output-file "fm_2m_gen_oevsv-repeater-db-additional.json" --output-format="json"
memory-channels-processor --source "oevsv-repeater-db" --source "json" --json-input-file "additional-items.json" --band 70cm --band 2m --type "fm" --output-file "fm_2m_70cm_gen_oevsv-repeater-db-additional.json" --output-format="json"

# FM: Simplex-Kanäle
memory-channels-processor --source "fm-channels-iaru-r1" --source "json" --json-input-file "additional-items.json" --band 70cm --type "fm" --output-file "fm_70cm_gen_fm-channels-iaru-r1-additional.json" --output-format="json"
memory-channels-processor --source "fm-channels-iaru-r1" --source "json" --json-input-file "additional-items.json" --band 2m --type "fm" --output-file "fm_2m_gen_fm-channels-iaru-r1-additional.json" --output-format="json"

# FM (70cm, 2m): Mischung ÖVSV Repeater-DB + Simplex-Kanäle
memory-channels-processor --source "oevsv-repeater-db" --source "fm-channels-iaru-r1" --source "json" --json-input-file "additional-items.json" --band 70cm --band 2m --type "fm" --output-file "fm_2m_70cm_gen_oevsv-repeater-db_fm-channels-iaru-r1-additional.json" --output-format="json"

# D-STAR (23cm, 70cm, 2m): ÖVSV Repeater-DB
memory-channels-processor --source "oevsv-repeater-db" --source "json" --json-input-file "additional-items.json" --band 23cm --band 70cm --band 2m --type "d-star" --output-file "d-star_23cm_70cm_2m_gen_oevsv-repeater-db-additional.json" --output-format="json" --sort "callsign" --sort "freq_tx" --sort "name"

# DMR (70cm, 2m): ÖVSV Repeater-DB
memory-channels-processor --source "oevsv-repeater-db" --source "json" --json-input-file "additional-items.json" --band 70cm --band 2m --type "dmr" --output-file "dmr_70cm_2m_gen_oevsv-repeater-db-additional.json" --output-format="json" --sort "callsign" --sort "freq_tx" --sort "name"
----


[#dataset]
== Beispiel-Datensatz

.JSON-Beispiel-Datensatz
[source%linenums,json]
----
include::example$data_target_example.json[tag=**]
----
