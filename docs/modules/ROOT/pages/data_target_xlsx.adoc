= Microsoft Excel (XLSX)
:navtitle: Excel
:page-aliases: export_xlsx.adoc

Der Export im Excel-Format (XLSX) ermöglicht die Weiterverarbeitung der Daten mit Microsoft Excel.


[#examples]
== Beispiele

.Beispiel Kommandos für Excel-Exporte
[source%linenums,bash]
----
# FM: Simplex-Kanäle
memory-channels-processor --source "fm-channels-iaru-r1" --band 70cm --type "fm" --output-file "fm_70cm_gen_fm-channels-iaru-r1.xlsx" --output-format="xlsx"
memory-channels-processor --source "fm-channels-iaru-r1" --band 2m --type "fm" --output-file "fm_2m_gen_fm-channels-iaru-r1.xlsx" --output-format="xlsx"
----