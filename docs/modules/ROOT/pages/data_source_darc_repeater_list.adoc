= DARC Relais Liste
:navtitle: DARC-Relais

Der Deutsche Amateur-Radio-Club e.V. stellt eine Liste mit Analog- und Digitalrelais aus Deutschland unter link:https://relaislisten.darc.de[relaislisten.darc.de, {linkAttributesExternal}] bereit.

[NOTE]
====
Die Daten unter link:https://relaislisten.darc.de[relaislisten.darc.de, {linkAttributesExternal}] werden einmal pro Stunde auf den neuesten Stand gebracht (immer 5 Minuten nach "Punkt").
Daher kann es sein, dass für kurze Zeit gar keine Daten zur Verfügung stehen, da dieser Updatevorgang erkannt wird und diese Datenquelle dann statt unvollständiger Daten für diese kurze Zeit gar keine zurückliefert.

Sollte das passieren, bitte die Anfrage nach etwa einer Minute nochmals durchführen.
====

:data-source: DARC Relais Liste
:data-source-id: darc-repeater-list
:data-source-provider: Deutscher Amateur-Radio-Club e.V.
:data-source-mode: fm, d-star
//:data-source-license:
:data-source-type: dynamisch
:data-source-url: https://relaislisten.darc.de/
//:data-source-note:
include::partial$data_source_box.adoc[]


[#examples]
== Beispiele

.Beispiel Kommandos für die USKA Repeater Liste
[source%linenums,bash]
----
# FM - 70cm
memory-channels-processor --source "darc-repeater-list" --band 70cm --type "fm" --output-file "fm_70cm_gen_darc-repeater-list.csv" --output-format="csv"

# FM - 2m
memory-channels-processor --source "darc-repeater-list" --band 2m --type "fm" --output-file "fm_2m_gen_darc-repeater-list.csv" --output-format="csv"

# FM - 2m + 70cm
memory-channels-processor --source "darc-repeater-list" --band 70cm --band 2m --type "fm" --output-file "fm_2m_70cm_gen_darc-repeater-list.csv" --output-format="csv"
----