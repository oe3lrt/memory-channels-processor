= TSV (Tab Separated Values)
:navtitle: TSV

Der Import im TSV-Format (Tab Separated Values) ermöglicht die Weiterverarbeitung von Daten, die von externen Programmen bereitgestellt werden.
So kann auch eine vom `Memory Channels Processor` exportierte Datei wieder als Datenquelle verwendet werden.
Das ist vor zum Beispiel dann nützlich, wenn man eigene Einträge mit den Daten anderer Quellen mischen will.

Eine Beschreibung zum Export findet man xref:data_target_tsv.adoc[hier].


:data-source: TSV
:data-source-id: tsv
:data-source-provider!:
:data-source-type: dynamisch
:data-source-mode!:
:data-source-license!:
:data-source-url!:
:data-source-note!:
include::partial$data_source_box.adoc[]


[#examples]
== Beispiele

.Beispiel Kommandos für TSV-Importe
[source%linenums,bash]
----
# FM: TSV
memory-channels-processor --source "tsv" --csv-input-file "local-data.tsv" --band 70cm --band 2m --type "fm" --output-format="csv"

# FM: ÖVSV Repeater-DB
memory-channels-processor --source "oevsv-repeater-db" --source "tsv" --tsv-input-file "additional-items.tsv" --band 70cm --type "fm" --output-file "fm_70cm_gen_oevsv-repeater-db-additional.tsv" --output-format="tsv"
memory-channels-processor --source "oevsv-repeater-db" --source "tsv" --tsv-input-file "additional-items.tsv" --band 2m --type "fm" --output-file "fm_2m_gen_oevsv-repeater-db-additional.tsv" --output-format="tsv"
memory-channels-processor --source "oevsv-repeater-db" --source "tsv" --tsv-input-file "additional-items.tsv" --band 70cm --band 2m --type "fm" --output-file "fm_2m_70cm_gen_oevsv-repeater-db-additional.tsv" --output-format="tsv"

# FM: Simplex-Kanäle
memory-channels-processor --source "fm-channels-iaru-r1" --source "tsv" --tsv-input-file "additional-items.tsv" --band 70cm --type "fm" --output-file "fm_70cm_gen_fm-channels-iaru-r1-additional.tsv" --output-format="tsv"
memory-channels-processor --source "fm-channels-iaru-r1" --source "tsv" --tsv-input-file "additional-items.tsv" --band 2m --type "fm" --output-file "fm_2m_gen_fm-channels-iaru-r1-additional.tsv" --output-format="tsv"

# FM (70cm, 2m): Mischung ÖVSV Repeater-DB + Simplex-Kanäle
memory-channels-processor --source "oevsv-repeater-db" --source "fm-channels-iaru-r1" --source "tsv" --tsv-input-file "additional-items.tsv" --band 70cm --band 2m --type "fm" --output-file "fm_2m_70cm_gen_oevsv-repeater-db_fm-channels-iaru-r1-additional.tsv" --output-format="tsv"

# D-STAR (23cm, 70cm, 2m): ÖVSV Repeater-DB
memory-channels-processor --source "oevsv-repeater-db" --source "tsv" --tsv-input-file "additional-items.tsv" --band 23cm --band 70cm --band 2m --type "d-star" --output-file "d-star_23cm_70cm_2m_gen_oevsv-repeater-db-additional.tsv" --output-format="tsv" --sort "callsign" --sort "freq_tx" --sort "name"

# DMR (70cm, 2m): ÖVSV Repeater-DB
memory-channels-processor --source "oevsv-repeater-db" --source "tsv" --tsv-input-file "additional-items.tsv" --band 70cm --band 2m --type "dmr" --output-file "dmr_70cm_2m_gen_oevsv-repeater-db-additional.tsv" --output-format="tsv" --sort "callsign" --sort "freq_tx" --sort "name"
----


[#dataset]
== Beispiel-Datensatz

.TSV-Beispiel-Datensatz
[source%linenums,tsv]
----
include::example$data_target_example.tsv[tag=**]
----
