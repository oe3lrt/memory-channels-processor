#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

APP_NPM="$(command -v npm)" || true
APP_NPX="$(command -v npx)" || true
APP_ZIP="$(command -v zip)" || true

#/////////////////////////

if [ ! -e "$APP_NPM" ] ; then
    echo "The executable 'npm' wasn't found!"
    exit
fi

if [ ! -e "$APP_NPX" ] ; then
    echo "The executable 'npx' wasn't found!"
    exit
fi

if [ ! -e "$APP_ZIP" ] ; then
    echo "The executable 'zip' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

# Install dependencies defined in 'package.json'
echo "Install dependencies defined in 'package.json'"
$APP_NPM update
$APP_NPM ci
echo "--------------------"

# Check and process environment variables
echo "Check and process environment variables"
if [[ "${SKIP_PYI_WIN}" = "true" ]]; then
  MEM_DEV_INSTALLER_WIN="--attribute mem-dev-installer-win=false"
else
  MEM_DEV_INSTALLER_WIN="--attribute mem-dev-installer-win=true"
fi
echo "--------------------"

# Generate resources - see https://github.com/asciidoctor/asciidoctor-tabs/blob/main/docs/use-with-antora.adoc
echo "Generate resources"
mkdir -p "$SD/supplemental-ui/css/vendor/"
cp "$SD/node_modules/@asciidoctor/tabs/dist/css/tabs.css" "$SD/supplemental-ui/css/vendor/tabs.css"
mkdir -p "$SD/supplemental-ui/js/vendor/"
cp "$SD/node_modules/@asciidoctor/tabs/dist/js/tabs.js" "$SD/supplemental-ui/js/vendor/tabs.js"
echo "--------------------"

# Fake resources (for development) that are generated during the CI workflows
if [ ! -e "$SD/docs/modules/ROOT/attachments/memory-channels-processor-windows.zip" ] ; then
  echo "Fake resource 'docs/modules/ROOT/attachments/memory-channels-processor-windows.zip'"
  $APP_ZIP -r -D -v "$SD/docs/modules/ROOT/attachments/memory-channels-processor-windows.zip" "$SD/LICENSE.txt"
  echo "--------------------"
fi

# Run Antora
echo "Run Antora"
$APP_NPX antora generate --stacktrace --attribute generated_timestamp="$(date -u)" $MEM_DEV_INSTALLER_WIN "$@" antora-playbook.yml
echo "--------------------"

echo "Done"
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////