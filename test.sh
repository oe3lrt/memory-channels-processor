#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

APP_TOX="$(command -v tox)"

#/////////////////////////

set -eu

#/////////////////////////

if [ ! -e "$APP_TOX" ] ; then
    echo "The executable 'tox' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Running tests"
$APP_TOX

echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////