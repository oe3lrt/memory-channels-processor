Before raising an issue to the issue tracker, please read through our usage guide:

* https://oe3lrt.gitlab.io/memory-channels-processor

If you feel that your issue can be categorized as a reproducible bug or a feature proposal, please use one of the issue templates provided and include as much information as possible.
