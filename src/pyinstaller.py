#!/usr/bin/env python3

# Allow direct execution
import os
import sys

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import platform

from PyInstaller.__main__ import run as run_pyinstaller

from memory_channels_processor import __version__

OS_NAME, MACHINE, ARCH = sys.platform, platform.machine().lower(), platform.architecture()[0][:2]
if MACHINE in ('x86', 'x86_64', 'amd64', 'i386', 'i686'):
    MACHINE = 'x86' if ARCH == '32' else ''

ROOT_PATH = os.path.realpath(os.path.normpath(os.path.abspath(os.path.join(os.getcwd(), os.path.dirname(__file__)))))


def main():
    opts, version = parse_options(), __version__

    onedir = '--onedir' in opts or '-D' in opts
    if not onedir and '-F' not in opts and '--onefile' not in opts:
        opts.append('--onefile')

    name, final_file = exe(onedir)
    print(f'Building memory-channels-processor v{version} for {OS_NAME} {platform.machine()} with options {opts}\n')
    print(f'Destination: {final_file}\n')

    opts = [
        f'--name={name}',
        f'--icon={ROOT_PATH}/icon.ico',
        #'--upx-exclude=vcruntime140.dll',
        '--noupx',
        '--noconfirm',
        '--python-option="X utf8_mode=1"',
        f'--additional-hooks-dir={ROOT_PATH}/memory_channels_processor/__pyinstaller',
        *opts,
        f'{ROOT_PATH}/memory_channels_processor/__main__.py',
        '--hiddenimport=pkgutil',
        '--hiddenimport=cattr',
        '--hiddenimport=cattr.preconf',
        '--hiddenimport=cattr.preconf.json',
        f'--add-data={ROOT_PATH}/memory_channels_processor/sources/*.xslt:./memory_channels_processor/sources',
        f'--add-data={ROOT_PATH}/memory_channels_processor/targets/*.tpl:./memory_channels_processor/targets',
        f'--add-data={ROOT_PATH}/memory_channels_processor/targets/pdf/*.ttf:./memory_channels_processor/targets/pdf',
        '--console'
    ]

    print(f'Running PyInstaller with {opts}')
    run_pyinstaller(opts)
    set_version_info(final_file, version)


def parse_options():
    # Compatibility with older arguments
    opts = sys.argv[1:]
    if opts[0:1] in (['32'], ['64']):
        if ARCH != opts[0]:
            raise Exception(f'{opts[0]}bit executable cannot be built on a {ARCH}bit system')
        opts = opts[1:]
    return opts


def exe(onedir):
    """@returns (name, path)"""
    name = '_'.join(filter(None, (
        'memory-channels-processor',
        {'win32': '', 'darwin': 'macos'}.get(OS_NAME, OS_NAME),
        MACHINE,
    )))
    return name, ''.join(filter(None, (
        f'{ROOT_PATH}/../dist/',
        onedir and f'{name}/',
        name,
        OS_NAME == 'win32' and '.exe',
    )))


def version_to_list(version):
    version_list = version.split('.')

    try:
        result = list(map(int, version_list)) + [0] * (4 - len(version_list))
    except ValueError:
        version_list.pop()
        result = list(map(int, version_list)) + [0] * (4 - len(version_list))

    return result


def set_version_info(exe, version):
    if OS_NAME == 'win32':
        windows_set_version(exe, version)


def windows_set_version(exe, version):
    from PyInstaller.utils.win32.versioninfo import (
        FixedFileInfo,
        StringFileInfo,
        StringStruct,
        StringTable,
        VarFileInfo,
        VarStruct,
        VSVersionInfo,
    )

    try:
        from PyInstaller.utils.win32.versioninfo import SetVersion
    except ImportError:  # Pyinstaller >= 5.8
        from PyInstaller.utils.win32.versioninfo import write_version_info_to_executable as SetVersion

    version_list = version_to_list(version)
    suffix = MACHINE and f'_{MACHINE}'
    SetVersion(exe, VSVersionInfo(
        ffi=FixedFileInfo(
            filevers=version_list,
            prodvers=version_list,
            mask=0x3F,
            flags=0x0,
            OS=0x4,
            fileType=0x1,
            subtype=0x0,
            date=(0, 0),
        ),
        kids=[
            StringFileInfo([StringTable('040904B0', [
                StringStruct('Comments', f'memory-channels-processor{suffix} Command Line Interface'),
                StringStruct('CompanyName', 'https://gitlab.com/oe3lrt/memory-channels-processor'),
                StringStruct('FileDescription', 'memory-channels-processor%s' % (MACHINE and f' ({MACHINE})')),
                StringStruct('FileVersion', version),
                StringStruct('InternalName', f'memory-channels-processor{suffix}'),
                StringStruct('LegalCopyright', 'Apache License 2.0'),
                StringStruct('OriginalFilename', f'memory-channels-processor{suffix}.exe'),
                StringStruct('ProductName', f'memory-channels-processor{suffix}'),
                StringStruct(
                    'ProductVersion', f'{version}{suffix} on Python {platform.python_version()}'),
            ])]), VarFileInfo([VarStruct('Translation', [0, 1200])]),
        ],
    ))


if __name__ == '__main__':
    main()
