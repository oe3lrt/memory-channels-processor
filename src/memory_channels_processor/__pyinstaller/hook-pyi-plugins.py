# This is a PyInstaller `hook
# <https://pyinstaller.readthedocs.io/en/stable/hooks.html>`_.
# needed to successfully freeze
# the package.
# See the `PyInstaller manual <https://pyinstaller.readthedocs.io/>`_
# for more information.
#
from PyInstaller.utils.hooks import collect_data_files

# Hook global variables
# =====================
#
# For the package ``memory_channels_processor`` to be frozen successfully,
# the module ``memory_channels_processor._hidden`` needs to be frozen, too,
# as well as all data-files.
# This hook takes care about this.
# For more information see
# `hook global variables
# <https://pyinstaller.readthedocs.io/en/stable/hooks.html#hook-global-variables>`_
# in the manual for more information.

hiddenimports = [
    "memory_channels_processor.sources",
    "memory_channels_processor.targets"
]
# The ``excludes`` parameter of `collect_data_files
# <https://pyinstaller.readthedocs.io/en/stable/hooks.html#useful-items-in-pyinstaller-utils-hooks>`_
# excludes ``rthooks.dat`` from the frozen executable, which is only needed when
# freezeing, but not when executing the frozen program.
#datas = collect_data_files('memory_channels_processor', excludes=['__pyinstaller'])
