import argparse
import os
import warnings
from _csv import QUOTE_MINIMAL

import pandas as pd
from pandas import DataFrame

from memory_channels_processor.processor import MemoryChannelsProcessor, flatten_array, is_empty, strip_duplicates
from memory_channels_processor.sources.base_text import TextBaseSource


class TsvSource(TextBaseSource):

    _alias_ = 'tsv'

    def setup_args(self, parser: argparse.ArgumentParser):
        parser.add_argument('--tsv-input-file', '--tsv', dest='tsv_input_files', action='append', help='The tsv input file', metavar="<file>", nargs='*', type=str)

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        tsv_input_files_flat = flatten_array(args.tsv_input_files)

        if not is_empty(tsv_input_files_flat):
            files = []
            for tsv_input_file in tsv_input_files_flat:
                files.append(os.path.realpath(tsv_input_file))

            files = strip_duplicates(files)

            data_frames = []
            for tsv_input_file in files:
                processor.print_verbose(f"Reading file '{tsv_input_file}'")
                with open(file=tsv_input_file, mode='rb') as f:
                    data_frames.append(self.process_data(pd.read_csv(filepath_or_buffer=f, index_col=None, header='infer', quoting=QUOTE_MINIMAL, encoding='utf-8', sep='\t'), processor, args))

            # Append dataframes
            if not is_empty(data_frames):
                with warnings.catch_warnings():
                    # TODO: (Pandas 2.1.0+) Fix deprecated functionality "FutureWarning: The behavior of DataFrame concatenation with empty or all-NA entries is deprecated."
                    warnings.filterwarnings("ignore", category=FutureWarning)
                    data = pd.concat(data_frames, ignore_index=True)

                return data
            else:
                return None
        else:
            print("TsvSource: No file given. Specify a file with option '--tsv-input-file'")
            exit(1)
