import argparse
import os
import warnings

import pandas as pd
from pandas import DataFrame

from memory_channels_processor.processor import MemoryChannelsProcessor, flatten_array, is_empty, strip_duplicates
from memory_channels_processor.sources.base_text import TextBaseSource


class JsonSource(TextBaseSource):

    _alias_ = 'json'

    def setup_args(self, parser: argparse.ArgumentParser):
        parser.add_argument('--json-input-file', '--json', dest='json_input_files', action='append', help='The JSON input file', metavar="<file>", nargs='*', type=str)

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        json_input_files_flat = flatten_array(args.json_input_files)

        if not is_empty(json_input_files_flat):
            files = []
            for json_input_file in json_input_files_flat:
                files.append(os.path.realpath(json_input_file))

            files = strip_duplicates(files)

            data_frames = []
            for json_input_file in files:
                processor.print_verbose(f"Reading file '{json_input_file}'")
                with open(file=json_input_file, mode='rb') as f:
                    data_frames.append(self.process_data(pd.read_json(path_or_buf=f, encoding='utf-8'), processor, args))

            # Append dataframes
            if not is_empty(data_frames):
                with warnings.catch_warnings():
                    # TODO: (Pandas 2.1.0+) Fix deprecated functionality "FutureWarning: The behavior of DataFrame concatenation with empty or all-NA entries is deprecated."
                    warnings.filterwarnings("ignore", category=FutureWarning)
                    data = pd.concat(data_frames, ignore_index=True)

                return data
            else:
                return None
        else:
            print("JsonSource: No file given. Specify a file with option '--json-input-file'")
            exit(1)
