import argparse
from io import BytesIO

import pandas as pd
from pandas import DataFrame

from memory_channels_processor.plugin_source import Source
from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty, get_data_source_col_names, get_band_by_freq,
                                                 SOURCE_TYPE_DYNAMIC, \
                                                 SOURCE_LICENCE_CC_BY_40, SOURCE_LICENCE_CC_BY_40_URL)


class OevsvRepeaterDbSource(Source):

    _alias_ = 'oevsv-repeater-db'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        repeater_db_dtypes = {
            'ctcss_tx': 'float64',
            'ctcss_rx': 'float64',
            'mmdvm': 'bool',
            'solar_power': 'bool',
            'battery_power': 'bool',
            'fm': 'bool',
            'c4fm': 'bool',
            'echolink': 'bool',
            'digital_id': 'int64',
            'dmr': 'bool',
            'dstar': 'bool',
            'tetra': 'bool',
            'other_mode': 'bool',
            'longitude': 'float64',
            'latitude': 'float64',
            'sea_level': 'int64'
        }
        repeater_db_col_drop = [
            'uid',
            'sysop',
            'url',
            'hardware',
            'other_mode_name',
            'comment',
            'created_at',
            'bev_gid',
            'geom'
        ]
        repeater_db_col_map = {
            'frequency_tx': 'freq_tx',
            'frequency_rx': 'freq_rx',
            'kg': 'landmark',
            'pg': 'county',
            'bl': 'state',
            'latitude': 'lat',
            'longitude': 'long'
        }

        if not args.offline:
            repeater_db_api_url = 'https://repeater.oevsv.at/api/trx_list'
            processor.print_verbose(f"Fetching data from '{repeater_db_api_url}'")

            session = processor.requests_session()
            response = session.get(repeater_db_api_url, allow_redirects=True)
            repeater_db_api_url_final = response.url

            if response.history:
                processor.print_verbose(f"Request was redirected from {repeater_db_api_url} to {repeater_db_api_url_final}")

            if response.status_code == 200:
                data = pd.read_json(BytesIO(response.content), dtype=repeater_db_dtypes)
            else:
                processor.print_verbose(f"Failed to retrieve data: {response.status_code}")
                return None
        else:
            processor.print_verbose("Loading data from offline data")

            # TODO: This currently doesn't work. Fix it...
            data = pd.read_json('oevsv_repeater_db.json', dtype=repeater_db_dtypes)

        # Drop unused columns
        processor.print_verbose("Drop unused columns")
        data.drop(columns=repeater_db_col_drop, inplace=True)

        # Swap columns
        data['frequency_tx_tmp'] = data['frequency_tx']
        data['frequency_tx'] = data['frequency_rx']
        data['frequency_rx'] = data['frequency_tx_tmp']

        data['ctcss_tx_tmp'] = data['ctcss_tx']
        data['ctcss_tx'] = data['ctcss_rx']
        data['ctcss_rx'] = data['ctcss_tx_tmp']

        # Filtering - filter invalid values
        data_query = ['not type_of_station.isnull()', 'not band.isnull()']

        # Filter for 'repeater_voice'
        processor.print_verbose("Filter for type: repeater_voice")
        data_query.append("type_of_station == 'repeater_voice'")

        # Filter for 'status'
        processor.print_verbose("Filter out inactive")
        data_query.append("status == 'active'")

        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            processor.print_verbose(f"Filter dataset: {data_query_str}")
            data.query(data_query_str, inplace=True)

        # Generate additional data
        data['band'] = data.apply(self.get_band_tx, axis=1)
        data['band_tx'] = data.apply(self.get_band_tx, axis=1)
        data['band_rx'] = data.apply(self.get_band_rx, axis=1)

        data['dstar_rpt1'] = data.apply(self.get_dstar_rpt1, axis=1)
        data['dstar_rpt2'] = data.apply(self.get_dstar_rpt2, axis=1)

        data['dmr_id'] = data.apply(self.get_dmr_id, axis=1)

        data['loc_exact'] = True
        data['locator'] = None

        # Filter out invalid rows
        data.drop(data[data.frequency_tx == data.frequency_rx].index, inplace=True)
        data.drop(data[data.band_tx != data.band_rx].index, inplace=True)

        # Rename columns
        processor.print_verbose("Optimize dataset...")
        data.rename(columns=repeater_db_col_map, inplace=True)

        # Calculate - Country
        data['country'] = 'Austria'
        data['country_code'] = 'AUT'

        data['scan_group'] = None

        data['source_id'] = 'oevsv-repeater-db'
        data['source_name'] = 'ÖVSV-Repeater Database'
        data['source_provider'] = 'ÖVSV UKW Referat'
        data['source_type'] = SOURCE_TYPE_DYNAMIC
        data['source_license'] = SOURCE_LICENCE_CC_BY_40
        data['source_license_url'] = SOURCE_LICENCE_CC_BY_40_URL
        data['source_url'] = 'https://www.oevsv.at/funkbetrieb/amateurfunkfrequenzen/ukw-referat/'

        # Calculate - Name
        data['name'] = data.apply(self.get_name, axis=1)

        # Return dataset
        return data[get_data_source_col_names()]

    def get_name(self, row) -> str:
        if not is_empty(row['site_name']):
            result = row['site_name'].strip().replace(" Ort", "")
            if result.startswith("Stadt/"):
                result = result.replace("Stadt/", row['city'] + " ")
        else:
            result = ''
        return result

    def get_band_tx(self, row) -> str:
        return get_band_by_freq(row['frequency_tx'])

    def get_band_rx(self, row) -> str:
        return get_band_by_freq(row['frequency_rx'])

    def get_dstar_rpt1(self, row) -> str:
        if not is_empty(row['dstar_rpt1']):
            result = row['dstar_rpt1']
        elif row['dstar']:
            freq_band = get_band_by_freq(row['frequency_tx'])
            if freq_band == "23cm":
                result = "A"
            elif freq_band == "70cm":
                result = "B"
            elif freq_band == "2m":
                result = "C"
            else:
                result = ""
        else:
            result = ""

        return result

    def get_dstar_rpt2(self, row) -> str:
        # See https://wiki.oevsv.at/wiki/Adressierung_bei_Dstar#Repeater2 for details
        if row['dstar']:
            freq_band = get_band_by_freq(row['frequency_tx'])
            if freq_band in ["23cm", "70cm", "2m"]:
                result = "G"
            else:
                result = ""
        else:
            result = ""

        return result

    def get_dmr_id(self, row) -> int:
        if row['dmr']:
            return row['digital_id']
        else:
            return None