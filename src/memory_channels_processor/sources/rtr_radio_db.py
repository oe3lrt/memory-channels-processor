import argparse
import json
import re
from io import BytesIO

import pandas as pd
from latloncalc import latlon
from pandas import DataFrame

from memory_channels_processor.plugin_source import Source
from memory_channels_processor.processor import (MemoryChannelsProcessor, get_data_source_col_names, SOURCE_TYPE_DYNAMIC, \
                                                 SOURCE_LICENCE_PUBLIC_DOMAIN)


class RtrRadioDbSource(Source):

    _alias_ = 'rtr-radio-db'

    radio_db_state_code2name_map = {
        "W": "Wien",
        "NÖ": "Niederösterreich",
        "OÖ": "Oberösterreich",
        "B": "Burgenland",
        "ST": "Steiermark",
        "K": "Kärnten",
        "S": "Salzburg",
        "T": "Tirol",
        "V": "Vorarlberg"
    }
    radio_db_state_name2code_map = {v: k for k, v in radio_db_state_code2name_map.items()}

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        radio_db_dtypes = {
            'funkst_frequenz': 'float64',
            'funkst_leistung_kw': 'float64',
            'gebiet_code': 'int64'
        }
        radio_db_col_map = {
            'programm_liste': 'name',
            'funkst_frequenz': 'freq_tx',
            'funkst_standort': 'landmark'
        }
        radio_db_col_drop = [
            'programm_typ',
            'veranstalter_name',
            'funkst_polarisation',
            'funkst_channel',
            'funkst_name',
            'funkst_leistung_kw',
            'funkst_bundesland',
            'funkst_nord',
            'funkst_ost',
            'funkst_rds',
            'gebiet_code',
            'gebiet_name',
            'veranstalter_name'
        ]

        if not args.offline:
            radio_db_api_url = 'https://data.rtr.at/api/v1/tables/MedienFrequenzbuch.json?programm_typ=H%C3%B6rfunk&gte_funkst_frequenz=87&lte_funkst_frequenz=108'
            processor.print_verbose(f"Fetching data from '{radio_db_api_url}'")

            # Acknowledgement
            processor.print_verbose("Data provided by 'RTR-GmbH – data.rtr.at'")

            session = processor.requests_session()
            response = session.get(radio_db_api_url, allow_redirects=True)
            radio_db_api_url_final = response.url

            if response.history:
                processor.print_verbose(f"Request was redirected from {radio_db_api_url} to {radio_db_api_url_final}")

            if response.status_code == 200:
                data_json = json.load(BytesIO(response.content))

                data = pd.DataFrame.from_dict(data_json['data'])
            else:
                processor.print_verbose(f"Failed to retrieve data: {response.status_code}")
                return None
        else:
            processor.print_verbose("Loading data from offline data")

            # TODO: This currently doesn't work. Fix it...
            data = pd.read_json('rtr_radio_db.json', dtype=radio_db_dtypes)

        # Rename columns
        data.rename(columns=radio_db_col_map, inplace=True)

        # Generate additional data
        data['band'] = "radio"
        data['callsign'] = ""

        data['freq_rx'] = data['freq_tx']

        data['fm'] = True
        data['dmr'] = False
        data['dstar'] = False

        data['dmr_id'] = None

        data['ctcss_tx'] = None
        data['ctcss_rx'] = None

        data['dstar_rpt1'] = None
        data['dstar_rpt2'] = None

        data['loc_exact'] = True
        data['lat'] = data.apply(self.get_lat, axis=1)
        data['long'] = data.apply(self.get_long, axis=1)
        data['locator'] = None
        data['sea_level'] = None

        data['name'] = data.apply(self.get_name, axis=1)

        data['state'] = data.apply(self.get_state, axis=1)
        data['country'] = 'Austria'
        data['country_code'] = 'AUT'

        data['scan_group'] = None

        data['source_id'] = 'rtr-radio-db'
        data['source_name'] = 'Radio Frequencies'
        data['source_provider'] = 'RTR-GmbH - data.rtr.at'
        data['source_type'] = SOURCE_TYPE_DYNAMIC
        data['source_license'] = SOURCE_LICENCE_PUBLIC_DOMAIN
        data['source_license_url'] = None
        data['source_url'] = 'https://www.rtr.at/rtr/service/opendata/OD_Uebersicht.de.html'

        # Drop unused columns
        processor.print_verbose("Drop unused columns")
        data.drop(columns=radio_db_col_drop, inplace=True)

        # Return dataset
        return data[get_data_source_col_names()]

    def get_lat(self, row) -> float:
        if row['funkst_nord'] is not None:
            try:
                regex_result = re.search(r"(?P<d>\d?\d\d)(?P<H>[NSEW])(?P<m>\d\d) (?P<s>\d\d)", row['funkst_nord'])
                if regex_result and len(regex_result.groups()) > 0:
                    coord_degree = int(regex_result.group('d'))
                    coord_minute = int(regex_result.group('m'))
                    coord_second = int(regex_result.group('s'))
                    coord_hemi = regex_result.group('H')

                    coord = latlon.Latitude(degree=coord_degree, minute=coord_minute, second=coord_second)
                    coord.set_hemisphere(coord_hemi)
                    result = round(float(coord), 6)
                else:
                    result = None
            except:
                result = None
        else:
            result = None

        return result

    def get_long(self, row) -> float:
        if row['funkst_ost'] is not None:
            try:
                regex_result = re.search(r"(?P<d>\d?\d\d)(?P<H>[NSEW])(?P<m>\d\d) (?P<s>\d\d)", row['funkst_ost'])
                if regex_result and len(regex_result.groups()) > 0:
                    coord_degree = int(regex_result.group('d'))
                    coord_minute = int(regex_result.group('m'))
                    coord_second = int(regex_result.group('s'))
                    coord_hemi = regex_result.group('H')

                    coord = latlon.Longitude(degree=coord_degree, minute=coord_minute, second=coord_second)
                    coord.set_hemisphere(coord_hemi)
                    result = round(float(coord), 6)
                else:
                    result = None
            except:
                result = None
        else:
            result = None

        return result

    def get_name(self, row) -> str:
        if row['name'] is not None:
            result = row['name']
            mapping = {
                "Radio": ""
            }
            mapping.update(self.radio_db_state_name2code_map)

            for key, value in mapping.items():
                result = result.replace(key, value)

            result = result.strip()
        else:
            result = None

        return result

    def get_state(self, row) -> float:
        if row['funkst_bundesland'] is not None:
            if row['funkst_bundesland'] in self.radio_db_state_code2name_map:
                result = self.radio_db_state_code2name_map[row['funkst_bundesland']]
            else:
                result = None
        else:
            result = None

        return result
