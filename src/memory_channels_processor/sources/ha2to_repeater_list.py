import argparse
from io import StringIO

import pandas as pd
from numpy import float64, int64
from pandas import DataFrame
from pyhamtools.locator import locator_to_latlong

from memory_channels_processor.plugin_source import Source
from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty, get_data_source_col_names, get_band_by_freq, round_float,
                                                 SOURCE_TYPE_DYNAMIC, \
                                                 SOURCE_LICENCE_BY)


class HA2TORepeaterListSource(Source):

    _alias_ = 'ha2to-repeater-list'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        repeater_list_converters = {
            'Callsign': str,
            'QTH/Name': str,
            'DownLink [kHz]': float64,
            'UpLink [kHz]': float64,
            'Offset [kHz]': float64,
            'Mode': str,
            'CTCSS DL/UL [Hz]': str,
            'DMR ID': str,
            'CC': str,
            'ASL': int64,
            'State': str
        }
        repeater_list_col_drop = [
            'Ch new',
            'Ch old',
            'Offset [kHz]',
            'Echolink Node'
        ]
        repeater_list_col_map = {
            'Callsign': 'callsign',
            'QTH/Name': 'name',
            'DownLink [kHz]': 'freq_rx',
            'UpLink [kHz]': 'freq_tx',
            'CTCSS DL/UL [Hz]': 'ctcss_dl_ul',
            'DMR ID': 'dmr_id',
            'CC': 'dmr_cc',
            'QTH Locator': 'locator',
            'ASL': 'sea_level',
            'State': 'state'
        }

        if not args.offline:
            repeater_list_url = 'http://ha2to.orbel.hu/content/repeaters/en/index.html'
            processor.print_verbose(f"Fetching data from '{repeater_list_url}'")

            session = processor.requests_session()
            response = session.get(repeater_list_url, allow_redirects=True)
            repeater_list_url_final = response.url

            if response.history:
                processor.print_verbose(f"Request was redirected from {repeater_list_url} to {repeater_list_url_final}")

            if response.status_code == 200:
                data_frames = pd.read_html(StringIO(response.content.decode('utf-8')), converters=repeater_list_converters, encoding='utf8', match='Callsign')
            else:
                processor.print_verbose(f"Failed to retrieve data: {response.status_code}")
                return None

            if len(data_frames) >= 4:
                data_fm = data_frames[0]  # FM data
                data_dmr = data_frames[1]  # DMR data
                data_dstar = data_frames[3]  # D-STAR data

                data_merge_cols = ['Callsign', 'QTH/Name', 'DownLink [kHz]', 'UpLink [kHz]', 'Ch new', 'Offset [kHz]', 'QTH Locator', 'ASL', 'State']

                data = data_fm.merge(data_dmr, on=data_merge_cols, how='outer', suffixes=(None, '_dmr'))
                data = data.merge(data_dstar, on=data_merge_cols, how='outer', suffixes=(None, '_dstar'))
            else:
                return None

        else:
            return None

        # Rename columns
        data.rename(columns=repeater_list_col_map, inplace=True)

        # Drop unused columns
        processor.print_verbose("Drop unused columns")
        data.drop(columns=repeater_list_col_drop, inplace=True)

        # Filtering - filter invalid values
        data_query = ['not callsign.isnull()', 'not name.isnull()', 'not freq_tx.isnull()', 'not freq_rx.isnull()']

        # Filter for 'status'
        processor.print_verbose("Filter out inactive")
        data_query.append("state == 'active'")

        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            processor.print_verbose(f"Filter dataset: {data_query_str}")
            data.query(data_query_str, inplace=True)

        # Recalculate data
        data['freq_tx'] = data.freq_tx / 1000
        data['freq_rx'] = data.freq_rx / 1000

        # Generate additional data
        data['band'] = data.apply(self.get_band_tx, axis=1)
        data['band_tx'] = data.apply(self.get_band_tx, axis=1)
        data['band_rx'] = data.apply(self.get_band_rx, axis=1)

        data['fm'] = data.apply(self.get_mode_fm, axis=1)
        data['dmr'] = data.apply(self.get_mode_dmr, axis=1)
        data['dstar'] = data.apply(self.get_mode_dstar, axis=1)
        data.drop(columns=['Mode', 'Mode_dmr', 'Mode_dstar'], inplace=True)

        data['ctcss_tx'] = data.apply(self.get_ctcss_tx, axis=1)
        data['ctcss_rx'] = data.apply(self.get_ctcss_rx, axis=1)

        data['dstar_rpt1'] = data.apply(self.get_dstar_rpt1, axis=1)
        data['dstar_rpt2'] = data.apply(self.get_dstar_rpt2, axis=1)

        data['loc_exact'] = False
        data['lat'] = data.apply(self.get_loc_lat, axis=1)
        data['long'] = data.apply(self.get_loc_long, axis=1)
        data['locator'] = None

        # Filter out invalid rows
        data.drop(data[data.freq_tx == data.freq_rx].index, inplace=True)
        data.drop(data[data.band_tx != data.band_rx].index, inplace=True)

        # Add additional data
        processor.print_verbose("Add additional data...")
        data['landmark'] = None
        data['state'] = None
        data['country'] = 'Hungary'
        data['country_code'] = 'HUN'

        data['scan_group'] = None

        data['source_id'] = 'ha2to-repeater-list'
        data['source_name'] = 'HA2TO Repeater List'
        data['source_provider'] = 'HA2TO'
        data['source_type'] = SOURCE_TYPE_DYNAMIC
        data['source_license'] = SOURCE_LICENCE_BY
        data['source_license_url'] = None
        data['source_url'] = 'http://ha2to.orbel.hu/content/repeaters/en/index.html'

        # Return dataset
        return data[get_data_source_col_names()]

    def get_band_tx(self, row) -> str:
        return get_band_by_freq(row['freq_tx'])

    def get_band_rx(self, row) -> str:
        return get_band_by_freq(row['freq_rx'])

    def get_mode_fm(self, row) -> bool:
        return 'FM' in str(row['Mode']).strip().replace("C4FM", "")

    def get_mode_dmr(self, row) -> bool:
        return 'DMR' in str(row['Mode_dmr']).strip()

    def get_mode_dstar(self, row) -> bool:
        return 'D-Star' in str(row['Mode_dstar']).strip()

    def get_dstar_rpt1(self, row) -> str:
        if row['dstar']:
            freq_band = get_band_by_freq(row['freq_tx'])
            if freq_band == "23cm":
                result = "A"
            elif freq_band == "70cm":
                result = "B"
            elif freq_band == "2m":
                result = "C"
            else:
                result = ""
        else:
            result = ""

        return result

    def get_dstar_rpt2(self, row) -> str:
        if row['dstar']:
            freq_band = get_band_by_freq(row['freq_tx'])
            if freq_band in ["23cm", "70cm", "2m"]:
                result = "G"
            else:
                result = ""
        else:
            result = ""

        return result

    def get_ctcss_tx(self, row) -> float64:
        if not is_empty(row['ctcss_dl_ul']):
            parts = str(row['ctcss_dl_ul']).strip().replace('--', '').split('/')
            if len(parts) >= 1 and not is_empty(parts[0]):
                result = float64(parts[0])
            else:
                result = None
        else:
            result = None
        return result

    def get_ctcss_rx(self, row) -> float64:
        if not is_empty(row['ctcss_dl_ul']):
            parts = str(row['ctcss_dl_ul']).strip().replace('--', '').split('/')
            if len(parts) >= 2 and not is_empty(parts[1]):
                result = float64(parts[1])
            else:
                result = None
        else:
            result = None
        return result

    def get_loc_lat(self, row) -> float64:
        if not is_empty(row['locator']):
            try:
                lat, long = locator_to_latlong(row['locator'])
                result = float64(round_float(lat, 5))
            except:
                result = None
        else:
            result = None

        return result

    def get_loc_long(self, row) -> float64:
        if not is_empty(row['locator']):
            try:
                lat, long = locator_to_latlong(row['locator'])
                result = float64(round_float(long, 5))
            except:
                result = None
        else:
            result = None

        return result
