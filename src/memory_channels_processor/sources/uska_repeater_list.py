import argparse
import re
from io import BytesIO

import pandas as pd
from pandas import DataFrame

from memory_channels_processor.plugin_source import Source
from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty, get_data_source_col_names, get_band_by_freq,
                                                 SOURCE_TYPE_DYNAMIC)


class UskaRepeaterListSource(Source):

    _alias_ = 'uska-repeater-list'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        uska_repeater_list_dtypes = {
            'QRG TX': 'float64',
            'QRG RX': 'float64',
            'Call': 'object',
            'QTH': 'object',
            'Locator': 'object',
            'Alt.': 'object',
            'Remarks': 'object',
            'Status': 'object'
        }
        uska_repeater_list_col_map = {
            'QRG TX': 'freq_rx',
            'QRG RX': 'freq_tx',
            'Call': 'callsign',
            'QTH': 'name',
            'Locator': 'locator',
            'Alt.': 'altitude',
            'Remarks': 'remarks',
            'Status': 'status'
        }
        uska_repeater_list_col_drop = [
            'altitude',
            'remarks',
            'status'
        ]

        if not args.offline:
            uska_repeater_xls_url = 'https://uska.ch/wp-content/uploads/2024/08/240817-USKA_Frequenzliste_Voice.xls'
            processor.print_verbose(f"Fetching data from '{uska_repeater_xls_url}'")

            # Acknowledgement
            processor.print_verbose("Data provided by 'USKA Union Schweizerischer Kurzwellen Amateure – uska.ch'")

            session = processor.requests_session()
            response = session.get(uska_repeater_xls_url, allow_redirects=True)
            uska_repeater_xls_url_final = response.url

            if response.history:
                processor.print_verbose(f"Request was redirected from {uska_repeater_xls_url} to {uska_repeater_xls_url_final}")

            if response.status_code == 200:
                data_object = pd.ExcelFile(BytesIO(response.content), engine='xlrd')
                data = data_object.parse(sheet_name=0, usecols='B:I', skiprows=(0, 1, 2, 3, 4, 5, 6, 8), header=0, dtype=uska_repeater_list_dtypes, verbose=args.verbose)
            else:
                processor.print_verbose(f"Failed to retrieve data: {response.status_code}")
                return None
        else:
            processor.print_verbose("Loading data from offline data")

            # TODO: This currently doesn't work. Fix it...
            data = pd.read_excel('uska_repeater.xls', dtype=uska_repeater_list_dtypes)

        # Rename columns
        data.rename(columns=uska_repeater_list_col_map, inplace=True)

        # Filtering - filter invalid values
        data_query = ['not freq_tx.isnull()', 'not freq_rx.isnull()', 'not callsign.isnull()']

        # Filter for 'status'
        processor.print_verbose("Filter out inactive")
        data_query.append("status == 1")

        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            processor.print_verbose(f"Filter dataset: {data_query_str}")
            data.query(data_query_str, inplace=True)

        # Process data
        data['sea_level'] = data.apply(self.clean_altitude, axis=1)

        # Generate additional data
        data['band'] = data.apply(self.get_band_tx, axis=1)
        data['band_tx'] = data.apply(self.get_band_tx, axis=1)
        data['band_rx'] = data.apply(self.get_band_rx, axis=1)

        data['fm'] = data.apply(self.get_fm, axis=1)
        data['dmr'] = data.apply(self.get_dmr, axis=1)
        data['dstar'] = data.apply(self.get_dstar, axis=1)

        data['dmr_id'] = data.apply(self.get_dmr_id, axis=1)

        data['ctcss_tx'] = data.apply(self.get_ctcss_tx, axis=1)
        data['ctcss_rx'] = None

        data['dstar_rpt1'] = data.apply(self.get_dstar_rpt1, axis=1)
        data['dstar_rpt2'] = data.apply(self.get_dstar_rpt2, axis=1)

        data['lat'] = None
        data['long'] = None
        data['loc_exact'] = False

        data['landmark'] = None
        data['state'] = None
        data['country'] = data.apply(self.get_country, axis=1)
        data['country_code'] = data.apply(self.get_country_code, axis=1)

        data['scan_group'] = None

        data['source_id'] = 'uska-repeater-list'
        data['source_name'] = 'USKA Repeater List'
        data['source_provider'] = 'USKA Netz- und Frequenzkoordination'
        data['source_type'] = SOURCE_TYPE_DYNAMIC
        data['source_license'] = None
        data['source_license_url'] = None
        data['source_url'] = 'https://www.uska.ch/bandplan/'

        # Filter out invalid rows
        data.drop(data[data.freq_tx == data.freq_rx].index, inplace=True)
        data.drop(data[data.band_tx != data.band_rx].index, inplace=True)

        # Drop unused columns
        processor.print_verbose("Drop unused columns")
        data.drop(columns=uska_repeater_list_col_drop, inplace=True)

        # Return dataset
        return data[get_data_source_col_names()]

    def clean_altitude(self, row) -> int:
        if not is_empty(row['altitude']):
            if row['altitude'].strip().endswith('m'):
                result = int(row['altitude'].strip()[:-1])
            else:
                result = int(row['altitude'].strip())
        else:
            result = None
        return result

    def get_band_tx(self, row) -> str:
        return get_band_by_freq(row['freq_tx'])

    def get_band_rx(self, row) -> str:
        return get_band_by_freq(row['freq_rx'])

    def get_fm(self, row) -> bool:
        if not is_empty(row['remarks']):
            if 'NFM' in row['remarks'] or 'FM' in row['remarks']:
                result = True
            else:
                result = False
        else:
            result = False
        return result

    def get_dmr(self, row) -> bool:
        if not is_empty(row['remarks']):
            if 'DMR' in row['remarks']:
                result = True
            else:
                result = False
        else:
            result = False
        return result

    def get_dstar(self, row) -> bool:
        if not is_empty(row['remarks']):
            if 'D-STAR' in row['remarks'] or 'DSTAR' in row['remarks']:
                result = True
            else:
                result = False
        else:
            result = False
        return result

    def get_dmr_id(self, row) -> int:
        if row['dmr'] and not is_empty(row['remarks']):
            try:
                regex_result = re.search(r"#(?P<id>\d+)", row['remarks'])
                if regex_result and len(regex_result.groups()) > 0:
                    result = int(regex_result.group('id'))
                else:
                    result = None
            except:
                result = None
        else:
            result = None

        return result

    def get_ctcss_tx(self, row) -> float:
        if not is_empty(row['remarks']):
            try:
                regex_result = re.search(r"T(?P<t>\d+\.\d)", row['remarks'])
                if regex_result and len(regex_result.groups()) > 0:
                    tone = regex_result.group('t')

                    result = round(float(tone), 1)
                else:
                    result = None
            except:
                result = None
        else:
            result = None
        return result

    def get_dstar_rpt1(self, row) -> str:
        if row['dstar']:
            freq_band = get_band_by_freq(row['freq_tx'])
            if freq_band == "23cm":
                result = "A"
            elif freq_band == "70cm":
                result = "B"
            elif freq_band == "2m":
                result = "C"
            else:
                result = ""
        else:
            result = ""

        return result

    def get_dstar_rpt2(self, row) -> str:
        if row['dstar']:
            freq_band = get_band_by_freq(row['freq_tx'])
            if freq_band in ["23cm", "70cm", "2m"]:
                result = "G"
            else:
                result = ""
        else:
            result = ""

        return result

    def get_country(self, row) -> str:
        if not is_empty(row['callsign']):
            if row['callsign'].strip().lower().startswith('hb0'):
                result = 'Liechtenstein'
            elif row['callsign'].strip().lower().startswith('hb4') or row['callsign'].strip().lower().startswith('hb9'):
                result = 'Switzerland'
            else:
                result = None
        else:
            result = None
        return result

    def get_country_code(self, row) -> str:
        if not is_empty(row['callsign']):
            if row['callsign'].strip().lower().startswith('hb0'):
                result = 'LIE'
            elif row['callsign'].strip().lower().startswith('hb4') or row['callsign'].strip().lower().startswith('hb9'):
                result = 'CHE'
            else:
                result = None
        else:
            result = None
        return result
