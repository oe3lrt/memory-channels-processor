import argparse
import re
from io import BytesIO

import pandas as pd
from numpy import float64
from pandas import DataFrame
from pyhamtools.locator import locator_to_latlong

from memory_channels_processor.plugin_source import Source
from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty, get_data_source_col_names, get_band_by_freq, round_float,
                                                 SOURCE_TYPE_DYNAMIC)


class SlovakiaRepeaterListSource(Source):

    _alias_ = 'slovakia-repeater-list'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        repeater_list_fm_2m_dtypes = {
            ' QRA   CALL': 'object',
            'QRG TX MHz': 'float64',
            'QRG RX MHz': 'float64',
            'QTH': 'object',
            'Locator': 'object',
            'ASL m': 'object',
            'PWR W': 'object',
            'RX': 'object',
            'TX': 'object',
            'Linked': 'object',
            'Sysop': 'object',
            'Notes': 'object'
        }
        repeater_list_fm_2m_col_map = {
            ' QRA   CALL': 'callsign',
            'QRG TX MHz': 'freq_rx',
            'QRG RX MHz': 'freq_tx',
            'QTH': 'name',
            'Locator': 'locator',
            'ASL m': 'sea_level',
            'PWR W': 'pwr',
            'RX': 'ctcss_rx_raw',
            'TX': 'ctcss_tx_raw',
            'Linked': 'linked',
            'Sysop': 'sysop',
            'Notes': 'notes'
        }
        repeater_list_fm_70cm_dtypes = {
            ' QRA   CALL': 'object',
            'QRG TX MHz': 'float64',
            'QRG RX MHz': 'float64',
            'QTH': 'object',
            'Locator': 'object',
            'ASL m': 'object',
            'PWR W': 'object',
            'Activation RX': 'object',
            'Encoder  TX': 'object',
            'Linked': 'object',
            'Sysop': 'object',
            'Poznámky / Notes': 'object'
        }
        repeater_list_fm_70cm_col_map = {
            ' QRA   CALL': 'callsign',
            'QRG TX MHz': 'freq_rx',
            'QRG RX MHz': 'freq_tx',
            'QTH': 'name',
            'Locator': 'locator',
            'ASL m': 'sea_level',
            'PWR W': 'pwr',
            'Activation RX': 'ctcss_rx_raw',
            'Encoder  TX': 'ctcss_tx_raw',
            'Linked': 'linked',
            'Sysop': 'sysop',
            'Poznámky / Notes': 'notes'
        }
        repeater_list_dv_70cm_dtypes = {
            ' QRA   CALL': 'object',
            'QRG TX MHz': 'float64',
            'QRG RX MHz': 'float64',
            'QTH': 'object',
            'Locator': 'object',
            'ASL m': 'object',
            'PWR W': 'object',
            'Mode': 'object',
            'Sysop': 'object',
            'Poznámky / Notes': 'object'
        }
        repeater_list_dv_70cm_col_map = {
            ' QRA   CALL': 'callsign',
            'QRG TX MHz': 'freq_rx',
            'QRG RX MHz': 'freq_tx',
            'QTH': 'name',
            'Locator': 'locator',
            'ASL m': 'sea_level',
            'PWR W': 'pwr',
            'Mode': 'mode',
            'Sysop': 'sysop',
            'Poznámky / Notes': 'notes'
        }
        repeater_list_col_drop = [
            'ctcss_rx_raw',
            'ctcss_tx_raw',
            'pwr',
            'linked',
            'linked_fm_70cm',
            'sysop',
            'notes'
        ]

        if not args.offline:
            repeater_list_url = 'https://docs.google.com/spreadsheet/ccc?key=1zESLDMqCZjRBGs5hR82UFdwoSOh-O2NoK7HvF2Z7PJc&output=xlsx'
            processor.print_verbose(f"Fetching data from '%s'" % repeater_list_url)

            session = processor.requests_session()
            response = session.get(repeater_list_url, allow_redirects=True)
            repeater_list_url_final = response.url

            if response.history:
                processor.print_verbose(f"Request was redirected from {repeater_list_url} to {repeater_list_url_final}")

            if response.status_code == 200:
                data_object = pd.ExcelFile(BytesIO(response.content), engine='openpyxl')

                # FM 2m data
                data_fm_2m = data_object.parse(sheet_name='2m FM', usecols='A:L', header=0,
                                               dtype=repeater_list_fm_2m_dtypes, verbose=args.verbose)
                data_fm_2m.rename(columns=repeater_list_fm_2m_col_map, inplace=True)
                data_fm_2m['fm'] = True

                # FM 70cm data
                data_fm_70cm = data_object.parse(sheet_name='70cm FM', usecols='A:L', header=0,
                                                 dtype=repeater_list_fm_70cm_dtypes, verbose=args.verbose)
                data_fm_70cm.rename(columns=repeater_list_fm_70cm_col_map, inplace=True)
                data_fm_70cm['fm'] = True

                # DV 70cm data
                data_dv_70cm = data_object.parse(sheet_name='70cm DV', usecols='A:J', header=0,
                                                 dtype=repeater_list_dv_70cm_dtypes, verbose=args.verbose)
                data_dv_70cm.rename(columns=repeater_list_dv_70cm_col_map, inplace=True)
                data_dv_70cm['fm'] = data_dv_70cm.apply(self.get_mode_fm, axis=1)
                data_dv_70cm['ctcss_rx_raw'] = ''
                data_dv_70cm['ctcss_tx_raw'] = ''
            else:
                processor.print_verbose(f"Failed to retrieve data: {response.status_code}")
                return None

            data_merge_cols = ['callsign', 'freq_rx', 'freq_tx', 'fm', 'ctcss_rx_raw', 'ctcss_tx_raw', 'name',
                               'locator', 'sea_level', 'pwr', 'sysop', 'notes']

            data = data_fm_2m.merge(data_fm_70cm, on=data_merge_cols, how='outer', suffixes=(None, '_fm_70cm'))
            data = data.merge(data_dv_70cm, on=data_merge_cols, how='outer', suffixes=(None, '_dv_70cm'))

        else:
            return None

        # Filtering - filter invalid values
        data_query = ['not callsign.isnull()', 'not name.isnull()', 'not freq_tx.isnull()', 'not freq_rx.isnull()']

        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            processor.print_verbose(f"Filter dataset: %s" % data_query_str)
            data.query(data_query_str, inplace=True)

        # Generate additional data
        data['band'] = data.apply(self.get_band_tx, axis=1)
        data['band_tx'] = data.apply(self.get_band_tx, axis=1)
        data['band_rx'] = data.apply(self.get_band_rx, axis=1)

        data['ctcss_tx'] = data.apply(self.get_ctcss, args=('ctcss_tx_raw',), axis=1)
        data['ctcss_rx'] = data.apply(self.get_ctcss, args=('ctcss_rx_raw',), axis=1)

        data['dmr'] = data.apply(self.get_mode_dmr, axis=1)
        data['dstar'] = data.apply(self.get_mode_dstar, axis=1)
        data.drop(columns=['mode'], inplace=True)

        data['dmr_id'] = data.apply(self.get_dmr_id, axis=1)

        data['dstar_rpt1'] = data.apply(self.get_dstar_rpt1, axis=1)
        data['dstar_rpt2'] = data.apply(self.get_dstar_rpt2, axis=1)

        data['loc_exact'] = False
        data['lat'] = data.apply(self.get_loc_lat, axis=1)
        data['long'] = data.apply(self.get_loc_long, axis=1)

        # Filter out invalid rows
        data.drop(data[data.freq_tx == data.freq_rx].index, inplace=True)
        data.drop(data[data.band_tx != data.band_rx].index, inplace=True)
        data.drop(data[data.callsign.str.contains('\\?')].index, inplace=True)

        # Rename columns
        processor.print_verbose("Optimize dataset...")
        data['landmark'] = None
        data['state'] = None
        data['country'] = 'Slovakia'
        data['country_code'] = 'SVK'

        data['scan_group'] = None

        data['source_id'] = 'slovakia-repeater-list'
        data['source_name'] = 'Slovakia Repeater List'
        data['source_provider'] = 'OM1AEG'
        data['source_type'] = SOURCE_TYPE_DYNAMIC
        data['source_license'] = None
        data['source_license_url'] = None
        data['source_url'] = 'https://sites.google.com/site/prevadzace/zoznam-om-prev%C3%A1dza%C4%8Dov'

        # Drop unused columns
        processor.print_verbose("Drop unused columns")
        data.drop(columns=repeater_list_col_drop, inplace=True)

        # Return dataset
        return data[get_data_source_col_names()]

    def get_band_tx(self, row) -> str:
        return get_band_by_freq(row['freq_tx'])

    def get_band_rx(self, row) -> str:
        return get_band_by_freq(row['freq_rx'])

    def get_mode_fm(self, row) -> bool:
        return 'FM' in str(row['mode']).strip()

    def get_mode_dmr(self, row) -> bool:
        return 'DMR' in str(row['mode']).strip()

    def get_mode_dstar(self, row) -> bool:
        return 'D-STAR' in str(row['mode']).strip()

    def get_dmr_id(self, row) -> int:
        if row['dmr'] and not is_empty(row['notes']):
            try:
                regex_result = re.search(r"DMR ID (?P<id>\d+)", row['notes'])
                if regex_result and len(regex_result.groups()) > 0:
                    result = int(regex_result.group('id'))
                else:
                    result = None
            except:
                result = None
        else:
            result = None

        return result

    def get_dstar_rpt1(self, row) -> str:
        if row['dstar']:
            freq_band = get_band_by_freq(row['freq_tx'])
            if freq_band == "23cm":
                result = "A"
            elif freq_band == "70cm":
                result = "B"
            elif freq_band == "2m":
                result = "C"
            else:
                result = ""
        else:
            result = ""

        return result

    def get_dstar_rpt2(self, row) -> str:
        if row['dstar']:
            freq_band = get_band_by_freq(row['freq_tx'])
            if freq_band in ["23cm", "70cm", "2m"]:
                result = "G"
            else:
                result = ""
        else:
            result = ""

        return result

    def get_ctcss(self, row, row_name: str) -> float64:
        if row_name in row and not is_empty(row[row_name]):
            try:
                regex_result = re.search(r"^(T(SQ)? )?(?P<ctcss>[\d]+\.\d)(Hz)?$", row[row_name])
                if regex_result and len(regex_result.groups()) > 0:
                    result = float64(regex_result.group('ctcss'))
                else:
                    result = None
            except:
                result = None
        else:
            result = None

        return result

    def get_loc_lat(self, row) -> float64:
        if not is_empty(row['locator']):
            try:
                lat, long = locator_to_latlong(row['locator'])
                result = float64(round_float(lat, 5))
            except:
                result = None
        else:
            result = None

        return result

    def get_loc_long(self, row) -> float64:
        if not is_empty(row['locator']):
            try:
                lat, long = locator_to_latlong(row['locator'])
                result = float64(round_float(long, 5))
            except:
                result = None
        else:
            result = None

        return result
