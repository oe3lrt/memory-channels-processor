<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/DSTAR-Repeater">
  <repeaters>
    <xsl:for-each select="country">
      <xsl:for-each select="repeater">
        <repeater>
          <country_code><xsl:value-of select="../@code"/></country_code>
          <call><xsl:value-of select="@call"/></call>
          <active><xsl:value-of select="@active"/></active>
          <xsl:copy-of select="*" />
        </repeater>
      </xsl:for-each>
    </xsl:for-each>
  </repeaters>
  </xsl:template>
</xsl:stylesheet>