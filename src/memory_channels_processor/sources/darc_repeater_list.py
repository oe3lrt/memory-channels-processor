import argparse
from _csv import QUOTE_MINIMAL
import re
from io import StringIO

import pandas as pd
from numpy import float64, int64
from pandas import DataFrame
from pyhamtools.locator import locator_to_latlong

from bs4 import BeautifulSoup
from titlecase import titlecase

from memory_channels_processor.plugin_source import Source
from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty, get_data_source_col_names, get_band_by_freq, round_float,
                                                 SOURCE_TYPE_DYNAMIC, \
                                                 SOURCE_LICENCE_CC_BY_40, SOURCE_LICENCE_CC_BY_40_URL)


class DarcRepeaterListSource(Source):

    _alias_ = 'darc-repeater-list'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        darc_repeater_date_format = '%d/%m/%Y'
        darc_repeater_dtypes = {
            'Call': 'str',
            'QRG': 'float64',
            'Input': 'str',
            'Locator': 'str',
            'Info': 'str',
            'Breite': 'str',
            'Länge': 'str',
            'CTCSS': 'str',
            'Mode/Node': 'str'
        }
        darc_repeater_parse_dates = []
        darc_repeater_col_drop = []
        darc_repeater_col_map = {
            'QRG': 'freq_rx',
            'Input': 'freq_tx_str',
            'Locator': 'locator',
            'Info': 'name',
            'CTCSS': 'remarks',
            'Mode/Node': 'mode_node'
        }

        if not args.offline:
            data_frames = []
            darc_repeater_query_locators=['JO44DV', 'JO64MM', 'JN48AA', 'JN68VR']

            # Acknowledgement
            processor.print_verbose("Data provided by 'DARC Deutscher Amateur-Radio-Club e.V. – darc.de'")

            for darc_repeater_query_locator in darc_repeater_query_locators:
                darc_repeater_url = f'https://relaislisten.darc.de/cgi-bin/relais.pl?sel=gridsq&gs={darc_repeater_query_locator}&dxcc=DL&maxgateways=600&printas=csv&kmmls=km'
                darc_repeater_url = darc_repeater_url + f'&type_dl3el=1'
                darc_repeater_url = darc_repeater_url + f'&type_dm=1'
                processor.print_verbose(f"Fetching data from '{darc_repeater_url}'")

                session = processor.requests_session()
                response = session.get(darc_repeater_url, allow_redirects=True, headers={"Content-Type": "text/html; charset=utf-8"})
                darc_repeater_url_final = response.url

                if response.history:
                    processor.print_verbose(f"Request was redirected from {darc_repeater_url} to {darc_repeater_url_final}")

                if response.status_code == 200:
                    # Clean HTML tags
                    soup = BeautifulSoup(response.content, features='html.parser') #, from_encoding='iso-8859-1')

                    if args.verbose:
                        print("============ CSV")
                        print("'" + soup.get_text() + "'")

                    if soup.get_text().strip() == 'Cacheupdate is running, please come again in 30s':
                        if args.verbose:
                            print(f"API seems to be down: {soup.get_text().strip()}")

                        # Clear cache
                        session.cache.delete(urls=[darc_repeater_url, darc_repeater_url_final])

                        return None
                    else:
                        # Convert to DataFrame
                        data_frame = pd.read_csv(filepath_or_buffer=StringIO(soup.get_text()), sep=';', decimal=',', index_col=None, header='infer', quoting=QUOTE_MINIMAL, encoding='utf-8', encoding_errors='strict', skiprows=0, skipfooter=3, engine='python', dtype=darc_repeater_dtypes, parse_dates=darc_repeater_parse_dates, dayfirst=True, date_format=darc_repeater_date_format)

                        # Drop last column ("Entfernung zu ...")
                        data_frame.drop(columns=data_frame.columns[-1], axis=1, inplace=True)

                        #if args.verbose:
                        #    print("============ DF")
                        #    data_frame.info()
                        #    print(data_frame)

                        # Append
                        data_frames.append(data_frame)

                else:
                    processor.print_verbose(f"Failed to retrieve data: {response.status_code}")

            if len(data_frames) > 0:
                data_merge_cols = ['Call', 'QRG', 'Input', 'Locator', 'Info', 'Breite', 'Länge', 'CTCSS', 'Mode/Node']
                data = data_frames[0]
                for i in range(1, len(data_frames)):
                    data = data.merge(data_frames[i], on=data_merge_cols, how='outer')

                processor.print_verbose(f"Found {len(data)} raw entries")
            else:
                return None
        else:
            processor.print_verbose("Loading data from offline data")

            # TODO: This currently doesn't work. Fix it...
            data = pd.read_json('darc-repeater.json', dtype=darc_repeater_dtypes)

        # Rename columns
        data.rename(columns=darc_repeater_col_map, inplace=True)

        # Drop unused columns
        if len(darc_repeater_col_drop) > 0:
            processor.print_verbose("Drop unused columns")
            data.drop(columns=darc_repeater_col_drop, inplace=True)

        # Clean up data
        data['freq_tx'] = data.apply(self.get_freq_tx, axis=1)
        data['callsign'] = data.apply(self.get_callsign, axis=1)

        # Filtering - filter invalid values
        data_query = ['not callsign.isnull()', 'not name.isnull()', 'not freq_tx.isnull()', 'not freq_rx.isnull()']

        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            processor.print_verbose(f"Filter dataset: {data_query_str}")
            data.query(data_query_str, inplace=True)
            data.reset_index(drop=True, inplace=True)

        # Generate additional data
        processor.print_verbose("Optimize dataset...")
        data['band'] = data.apply(self.get_band_tx, axis=1)
        data['band_tx'] = data.apply(self.get_band_tx, axis=1)
        data['band_rx'] = data.apply(self.get_band_rx, axis=1)

        data['fm'] = data.apply(self.get_mode_fm, axis=1)
        data['dmr'] = data.apply(self.get_mode_dmr, axis=1)
        data['dstar'] = False

        # Filtering - Filter for 'mode'
        data_query = []
        data_query_or = ['fm', 'dmr', 'dstar']
        if not is_empty(data_query_or):
            data_query_or_str = '(' + ') or ('.join(data_query_or) + ')'
            data_query.append(data_query_or_str)

        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            processor.print_verbose(f"Filter dataset: {data_query_str}")
            data.query(data_query_str, inplace=True)
            data.reset_index(drop=True, inplace=True)

        # Filter out invalid rows
        data.drop(data[data.freq_tx == data.freq_rx].index, inplace=True)
        data.drop(data[data.band_tx != data.band_rx].index, inplace=True)

        data['country_code'] = "DEU"
        data['country'] = "Germany"

        # Calculate - Name
        data['name'] = data.apply(self.get_name, axis=1)

        # Calculate D-STAR data
        data['dstar_rpt1'] = None
        data['dstar_rpt2'] = None

        # Calculate DMR data
        data['dmr_id'] = data.apply(self.get_dmr_id, axis=1)

        # Calculate tone data
        data['ctcss_tx'] = data.apply(self.get_ctcss_tx, axis=1)
        data['ctcss_rx'] = None

        # Add additional data
        data['scan_group'] = None

        data['state'] = None
        data['landmark'] = None

        data['sea_level'] = None

        data['loc_exact'] = False
        data['lat'] = data.apply(self.get_loc_lat, axis=1)
        data['long'] = data.apply(self.get_loc_long, axis=1)

        data['source_id'] = 'darc-repeater-list'
        data['source_name'] = 'DARC Relais Liste'
        data['source_provider'] = 'DARC'
        data['source_type'] = SOURCE_TYPE_DYNAMIC
        data['source_license'] = None
        data['source_license_url'] = None
        data['source_url'] = "https://relaislisten.darc.de/"

        # Return dataset
        return data[get_data_source_col_names()]

    def get_freq_tx(self, row) -> float64:
        if not is_empty(row['freq_tx_str']):
            value = str(row['freq_tx_str'])
            if value.lower().startswith('simp'):
                result = row['freq_rx']
            else:
                result = float(value.replace(',', '.'))
        else:
            result = None

        return result

    def get_callsign(self, row) -> str:
        if not is_empty(row['Call']):
            # Replace not needed spaces
            result = row['Call'].strip()
            # Remove everything after a space in the middle
            if ' ' in result:
                result = result.split(' ')[0]
        else:
            result = ''
        return result

    def get_band_tx(self, row) -> str:
        return get_band_by_freq(row['freq_tx'])

    def get_band_rx(self, row) -> str:
        return get_band_by_freq(row['freq_rx'])

    def get_mode_fm(self, row) -> bool:
        name_lower = str(row['name']).strip().lower()
        return is_empty(row['mode_node']) and not (
                name_lower.endswith(' (C4FM)'.lower()) or
                name_lower.endswith(' (NXDN)'.lower()) or
                name_lower.endswith(' (P25)'.lower()) or
                name_lower.endswith('(RTTY)'.lower()) or
                name_lower.endswith(' (Tetra)'.lower()) or
                name_lower.endswith(', Tetra'.lower()) or
                ' (Tetra '.lower() in name_lower or
                ' APCO25'.lower() in name_lower
        )

    def get_mode_dmr(self, row) -> bool:
        if not is_empty(row['mode_node']) and not is_empty(row['remarks']):
            mode_node = str(row['mode_node'])
            remarks = str(row['remarks'])
            if (mode_node.lower().startswith('dm#') or mode_node.lower().startswith('bm#')) and remarks.lower().startswith('cc'):
                return True
        return False

    def get_name(self, row) -> str:
        if not is_empty(row['name']):
            # Replace not needed spaces
            result = row['name'].strip()
            result = result.replace(' / ', '/')
            result = result.replace(' , ', ', ')

            # Fix encoding issues
            try:
                result = result.encode('latin-1').decode('utf-8')
            except UnicodeDecodeError:
                pass

            # Filter suffixes
            locator = str(row['locator'])
            if result == locator:
                return ''

            result = self.remove_suffix(result, ' ' + locator, True)
            result = self.remove_suffix(result, ',' + locator, True)
            result = self.remove_suffix(result, ' (' + locator + ')', True)
            if len(locator) == 6:
                result = self.remove_suffix(result, ' ' + locator[:-1], True)
                result = self.remove_suffix(result, ',' + locator[:-1], True)
                result = self.remove_suffix(result, ' (' + locator[:-1] + ')', True)
                result = self.remove_suffix(result, ' ' + locator[:-2], True)
                result = self.remove_suffix(result, ',' + locator[:-2], True)
                result = self.remove_suffix(result, ' (' + locator[:-2] + ')', True)

            result = self.remove_suffix(result, ', DE').strip()
            result = self.remove_suffix(result, ',').strip()

            result = self.remove_suffix(result, ' (C4FM)', True).strip()
            result = self.remove_suffix(result, '/C4FM', True).strip()

            result = self.remove_suffix(result, ' (FM)', True).strip()
            result = self.remove_suffix(result, ' (FM/C4FM)', True).strip()
            result = self.remove_suffix(result, ' (FM & D-Star)', True).strip()
            result = self.remove_suffix(result, ', FM und Multimode', True).strip()

            result = self.remove_suffix(result, ' (NXDN)', True).strip()

            result = self.remove_suffix(result, ' (Multim)', True).strip()
            result = self.remove_suffix(result, ' (Multimode)', True).strip()
            result = self.remove_suffix(result, ' Multimode', True).strip()
            result = self.remove_suffix(result, '/Multimode', True).strip()

            result = self.remove_suffix(result, '(RTTY)', True).strip()

            result = self.remove_suffix(result, ' (P25)', True).strip()
            result = self.remove_suffix(result, ' (Tetra)', True).strip()

            # Convert to title case
            result = titlecase(result, callback=self.callback_name_abbreviations)
        else:
            result = ''
        return result

    def callback_name_abbreviations(self, word: str, all_caps: bool, **kwargs) -> str:
        if not all_caps and word.lower() in ('am', 'an', 'bei', 'der', 'in', 'im', 'mit'):
            return word.lower()

        if word.upper() in ('OT'):
            return word.upper()

    def remove_suffix(self, content: str, suffix: str, ignore_case: bool = False) -> str:
        if content:
            result = content
            if suffix and not ignore_case and result.endswith(suffix):
                result = result[:-len(suffix)]
            elif suffix and ignore_case and result.lower().endswith(suffix.lower()):
                result = result[:-len(suffix)]

            return result
        else:
            return content

    def get_dmr_id(self, row) -> int:
        if row['dmr'] and not is_empty(row['mode_node']):
            try:
                regex_result = re.search(r"[DB]M#(?P<id>\d+)", row['mode_node'])
                if regex_result and len(regex_result.groups()) > 0:
                    result = int(regex_result.group('id'))
                else:
                    result = None
            except:
                result = None
        else:
            result = None

        return result

    def get_ctcss_tx(self, row) -> float64:
        if row['fm'] and not is_empty(row['remarks']):
            try:
                regex_result = re.search(r"(?P<t>\d+([.,]\d)?)(\s?Hz)?", row['remarks'])
                if regex_result and len(regex_result.groups()) > 0:
                    tone = regex_result.group('t').replace(",", ".")

                    result = float64(round_float(float(tone)), 1)
                else:
                    result = None
            except:
                result = None
        else:
            result = None
        return result

    def get_loc_lat(self, row) -> float64:
        if not is_empty(row['locator']):
            try:
                lat, long = locator_to_latlong(row['locator'])
                result = float64(round_float(lat, 5))
            except:
                result = None
        else:
            result = None

        return result

    def get_loc_long(self, row) -> float64:
        if not is_empty(row['locator']):
            try:
                lat, long = locator_to_latlong(row['locator'])
                result = float64(round_float(long, 5))
            except:
                result = None
        else:
            result = None

        return result
