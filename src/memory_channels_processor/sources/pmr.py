import argparse

import pandas as pd
from pandas import DataFrame

from .base_text import TextBaseSource
from memory_channels_processor.processor import (MemoryChannelsProcessor, get_data_source_col_names, get_data_source_cols, SOURCE_TYPE_STATIC)


class PMRChannelSource(TextBaseSource):

    _alias_ = 'pmr-channels'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        data = pd.DataFrame(columns=get_data_source_col_names())

        # Convert data types
        data = data.astype(get_data_source_cols())

        # -----------------------------------------

        step = 1250
        rows = []

        # -- Generate data for PMR band ------------
        freq_pmr = []

        # PMR Analogue (NFM)
        for freq in range(44600625, 44619375 + step, step):
            freq_pmr.append(freq / 100000)

        for freq in freq_pmr:
            channel = round(((freq - 446 - 0.0125) * 80) + 1.5)
            name = f"Ch%02d" % channel

            row = {
                'callsign': None,
                'name': name,
                'band': "pmr",
                'freq_tx': freq,
                'freq_rx': freq,
                'ctcss': False,
                'ctcss_tx': None,
                'ctcss_rx': None,
                'dmr': False,
                'dmr_id': None,
                'dstar': False,
                'dstar_rpt1': None,
                'dstar_rpt2': None,
                'fm': True,
                'landmark': None,
                'state': None,
                'country': None,
                'country_code': None,
                'loc_exact': False,
                'lat': None,
                'long': None,
                'locator': None,
                'sea_level': None,
                'scan_group': None,
                'source_id': 'pmr',
                'source_name': 'PMR Channels',
                'source_provider': '',
                'source_type': SOURCE_TYPE_STATIC,
                'source_license': None,
                'source_license_url': None,
                'source_url': ''
            }
            rows.append(row)

        # -----------------------------------------

        df_extended = pd.DataFrame(rows, columns=get_data_source_col_names())
        data = pd.concat([data, df_extended], ignore_index=True)

        # -----------------------------------------

        return self.process_data(data, processor, args)
