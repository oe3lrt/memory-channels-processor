import argparse

from pandas import DataFrame

from memory_channels_processor.processor import MemoryChannelsProcessor, get_data_source_col_names
from memory_channels_processor.plugin_source import Source


class TextBaseSource(Source):

    _skipload_ = True

    def process_data(self, data: DataFrame, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        # If a 'name_formatted' column exists, take that as 'name' (when enabled)
        if 'name_formatted' in data.columns and args.formatted_names_force_on_import:
            #data.drop(columns=['name'], inplace=True)
            #data.rename(columns={'name_formatted': 'name'}, inplace=True)
            data['name'] = data['name_formatted']

        # Add columns if they don't exist for compatibility
        data['country_code'] = data.get('country_code', None)
        data['locator'] = data.get('locator', None)
        data['sea_level'] = data.get('sea_level', None)
        data['scan_group'] = data.get('scan_group', None)
        data['source_id'] = data.get('source_id', None)
        data['source_name'] = data.get('source_name', None)
        data['source_provider'] = data.get('source_provider', None)
        data['source_type'] = data.get('source_type', None)
        data['source_license'] = data.get('source_license', None)
        data['source_url'] = data.get('source_url', None)

        data.reindex(columns=get_data_source_col_names(), fill_value=None)

        # Return dataset
        return data[get_data_source_col_names()]
