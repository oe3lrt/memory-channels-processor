import argparse
import os
import warnings

import pandas as pd
from pandas import DataFrame

from memory_channels_processor.processor import (MemoryChannelsProcessor, flatten_array, strip_duplicates, is_empty)
from memory_channels_processor.sources.base_text import TextBaseSource


class XmlSource(TextBaseSource):

    _alias_ = 'xml'

    def setup_args(self, parser: argparse.ArgumentParser):
        parser.add_argument('--xml-input-file', '--xml', dest='xml_input_files', action='append', help='The XML input file', metavar="<file>", nargs='*', type=str)

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        xml_input_files_flat = flatten_array(args.xml_input_files)

        if not is_empty(xml_input_files_flat):
            files = []
            for xml_input_file in xml_input_files_flat:
                files.append(os.path.realpath(xml_input_file))

            files = strip_duplicates(files)

            data_frames = []
            for xml_input_file in files:
                processor.print_verbose(f"Reading file '{xml_input_file}'")
                with open(file=xml_input_file, mode='rb') as f:
                    data_frames.append(self.process_data(pd.read_xml(path_or_buffer=f, encoding='utf-8'), processor, args))

            # Append dataframes
            if not is_empty(data_frames):
                with warnings.catch_warnings():
                    # TODO: (Pandas 2.1.0+) Fix deprecated functionality "FutureWarning: The behavior of DataFrame concatenation with empty or all-NA entries is deprecated."
                    warnings.filterwarnings("ignore", category=FutureWarning)
                    data = pd.concat(data_frames, ignore_index=True)

                return data
            else:
                return None
        else:
            print("XmlSource: No file given. Specify a file with option '--xml-input-file'")
            exit(1)
