import argparse
import os
from datetime import timedelta
from io import BytesIO
from pathlib import Path

import pandas as pd
import pycountry
from pandas import DataFrame

from memory_channels_processor.plugin_source import Source
from memory_channels_processor.processor import (MemoryChannelsProcessor, get_data_source_col_names, path_resolve, get_band_by_freq, is_empty, \
                                                 SOURCE_TYPE_DYNAMIC, SOURCE_LICENCE_NC)


class IrcDDBRepeatersSource(Source):

    _alias_ = 'ircddb-repeaters'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:

        ircddb_dtypes = {
            'country-code': 'object',
            'call': 'object',
            'active': 'bool',
            'gateway': 'object',
            'latitude': 'float64',
            'longitude': 'float64',
            'qth1': 'object',
            'qth2': 'object',
            'qrg': 'float64',
            'shift': 'float64',
            'agl': 'float64',
            'range': 'float64',
            'lastact': 'datetime64[s]'
        }

        ircddb_col_map = {
            'latitude': 'lat',
            'longitude': 'long',
            'qrg': 'freq_rx'
        }
        ircddb_col_drop = [
            'active',
            'shift',
            'agl',
            'range',
            'lastact'
        ]

        if not args.offline:
            ircddb_api_url = 'https://status.ircddb.net/rptr-xml.php'
            processor.print_verbose(f"Fetching data from '{ircddb_api_url}'")

            session = processor.requests_session()
            response = session.get(ircddb_api_url, allow_redirects=True)
            ircddb_api_url_final = response.url

            if response.history:
                processor.print_verbose(f"Request was redirected from {ircddb_api_url} to {ircddb_api_url_final}")

            if response.status_code == 200:
                data_raw = BytesIO(response.content)

                ircddb_folder_path = path_resolve(os.path.join(os.getcwd(), os.path.dirname(__file__)))
                ircddb_stylesheet_path = Path(path_resolve(os.path.join(ircddb_folder_path, './ircddb.xslt')))

                if ircddb_stylesheet_path.exists():
                    data = pd.read_xml(data_raw, xpath="//repeaters/repeater", elems_only=True, attrs_only=False, stylesheet=ircddb_stylesheet_path, dtype=ircddb_dtypes, encoding='utf8')
                else:
                    processor.print_verbose(f"XSLT stylesheet '{ircddb_stylesheet_path}' is missing!")
                    return None

            else:
                processor.print_verbose(f"Failed to retrieve data: {response.status_code}")
                return None

        else:
            processor.print_verbose("Loading data from offline data")

            # TODO: This currently doesn't work. Fix it...
            data = pd.read_xml('ircddb.xml', dtype=ircddb_dtypes)


        # Rename columns
        data.rename(columns=ircddb_col_map, inplace=True)


        # Clean up data
        data['call'] = data.apply(self.clean_call, axis=1)
        data['gateway'] = data.apply(self.clean_gateway, axis=1)


        # Generate additional data
        data['callsign'] = data.apply(self.get_callsign, axis=1)

        data['name'] = data.apply(self.get_name, axis=1)

        data['freq_tx'] = data.apply(self.get_freq_tx, axis=1)

        data['band'] = data.apply(self.get_band_tx, axis=1)
        data['band_tx'] = data.apply(self.get_band_tx, axis=1)
        data['band_rx'] = data.apply(self.get_band_rx, axis=1)

        data['ctcss_tx'] = None
        data['ctcss_rx'] = None

        data['fm'] = False
        data['dmr'] = False
        data['dstar'] = True

        data['dmr_id'] = None

        data['dstar_rpt1'] = data.apply(self.get_dstar_rpt1, axis=1)
        data['dstar_rpt2'] = data.apply(self.get_dstar_rpt2, axis=1)

        data['landmark'] = None
        data['state'] = None
        data['country'] = data.apply(self.get_country, axis=1)

        data['loc_exact'] = False
        data['locator'] = None
        data['sea_level'] = None

        data['scan_group'] = None

        data['source_id'] = 'ircddb-repeaters'
        data['source_name'] = 'ircDDB'
        data['source_provider'] = 'ircDDB'
        data['source_type'] = SOURCE_TYPE_DYNAMIC
        data['source_license'] = SOURCE_LICENCE_NC
        data['source_license_url'] = None
        data['source_url'] = 'https://status.ircddb.net/clg.php'

        # Filtering - filter invalid values
        data_query = ['not band.isnull()']

        # Filter for 'active'
        processor.print_verbose("Filter out inactive")
        data_query.append("active == True")

        data['lastact_duration'] = pd.Timestamp('today') - data['lastact']
        data['lastact_more_than'] = data['lastact_duration'] > timedelta(days=30)
        data_query.append('lastact_more_than != True')

        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            processor.print_verbose(f"Filter dataset: {data_query_str}")
            data.query(data_query_str, inplace=True)


        # Drop unused columns
        processor.print_verbose("Drop unused columns")
        data.drop(columns=ircddb_col_drop, inplace=True)


        # Return dataset
        return data[get_data_source_col_names()]

    def get_name(self, row) -> str:
        if not is_empty(row['qth1']):
            result = row['qth1'].strip().replace(" Ort", "")
            if result.startswith("Stadt/"):
                result = result.replace("Stadt/", row['city'] + " ")
            elif result.startswith("Wien/"):
                result = result.replace("Wien/", "")
        else:
            result = ''
        return result

    def get_country(self, row) -> str:
        if not is_empty(row['country_code']):
            country = pycountry.countries.get(alpha_3=str(row['country_code']))
            if not is_empty(country):
                result = country.name
            else:
                result = ''
        else:
            result = ''
        return result

    def get_callsign(self, row) -> str:
        if not is_empty(row['call']):
            result = row['call'][0:7].strip()
        else:
            result = ''
        return result

    def get_dstar_rpt1(self, row) -> str:
        if not is_empty(row['call']):
            result = row['call'][7:].strip()
        else:
            result = ''
        return result

    def get_dstar_rpt2(self, row) -> str:
        if not is_empty(row['gateway']):
            result = row['gateway'][7:].strip()
        else:
            result = ''
        return result

    def clean_call(self, row) -> str:
        if not is_empty(row['call']):
            result = row['call'][0:8]
        else:
            result = ''
        return result

    def clean_gateway(self, row) -> str:
        if not is_empty(row['gateway']):
            result = row['gateway'][0:8]
        else:
            result = ''
        return result

    def get_freq_tx(self, row) -> str:
        return row['freq_rx'] + row['shift']

    def get_band_tx(self, row) -> str:
        return get_band_by_freq(row['freq_tx'])

    def get_band_rx(self, row) -> str:
        return get_band_by_freq(row['freq_rx'])
