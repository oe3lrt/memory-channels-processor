import argparse

import pandas as pd
from pandas import DataFrame

from .base_text import TextBaseSource
from memory_channels_processor.processor import (MemoryChannelsProcessor, get_data_source_col_names, get_data_source_cols, SOURCE_TYPE_STATIC)


class CBChannelSource(TextBaseSource):

    _alias_ = 'cb-channels'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        data = pd.DataFrame(columns=get_data_source_col_names())

        # Convert data types
        data = data.astype(get_data_source_cols())

        # -----------------------------------------

        step = 1000
        rows = []

        # -- Generate data for CB band ------------
        # https://en.wikipedia.org/wiki/Citizens_band_radio
        # https://de.wikipedia.org/wiki/CB-Funk
        # https://www.bmf.gv.at/themen/telekommunikation-post_2/funk-mobilfunk/funk-frequenzen/funk-schnittstellenbeschreibung.html
        # https://www.bmf.gv.at/dam/jcr:88e0be74-0bcb-4f20-bd99-f78555d0305e/FSB-LN%20-%20Diverse%20Funknetze_%20Stand%2002%20November%202019.pdf
        freq_cb = list()

        # CB Channels 1-40 (10 kHz)
        for freq in range(2696500, 2740500 + step, step):
            freq_cb.append(freq / 100000)

        # Remove invalid frequencies
        for freq in [26.995, 27.045, 27.095, 27.145, 27.195]:
            freq_cb.remove(freq)

        # Reorder channels (jump around channels 23-25)
        freq_cb.insert(22, freq_cb.pop(24))

        for idx, freq in enumerate(freq_cb, start=1):
            name = f"C%02dEF" % idx

            if idx == 9:
                name += " EmComm"

            row = {
                'callsign': None,
                'name': name,
                'band': "cb",
                'freq_tx': freq,
                'freq_rx': freq,
                'ctcss': False,
                'ctcss_tx': None,
                'ctcss_rx': None,
                'dmr': False,
                'dmr_id': None,
                'dstar': False,
                'dstar_rpt1': None,
                'dstar_rpt2': None,
                'fm': True,
                'landmark': None,
                'state': None,
                'country': None,
                'country_code': None,
                'loc_exact': False,
                'lat': None,
                'long': None,
                'locator': None,
                'sea_level': None,
                'scan_group': None,
                'source_id': 'cb',
                'source_name': 'CB Channels',
                'source_provider': 'CEPT',
                'source_type': SOURCE_TYPE_STATIC,
                'source_license': None,
                'source_license_url': None,
                'source_url': ''
            }
            rows.append(row)

        # -----------------------------------------

        df_extended = pd.DataFrame(rows, columns=get_data_source_col_names())
        data = pd.concat([data, df_extended], ignore_index=True)

        # -----------------------------------------

        return self.process_data(data, processor, args)
