import argparse
from _csv import QUOTE_MINIMAL
from io import BytesIO

import pandas as pd
import pycountry
from pandas import DataFrame, Timestamp

from memory_channels_processor.plugin_source import Source
from memory_channels_processor.processor import (MemoryChannelsProcessor, get_data_source_col_names, SOURCE_TYPE_DYNAMIC, is_empty, flatten_array, \
                                                 strip_duplicates)


class SotaSummitsSource(Source):

    _alias_ = 'sota-summits'

    # Adapted from https://github.com/manuelkasper/sotlas-api/blob/master/jobs/isocodes.txt using the 'generate-sota-prefix-country-mapping.py' script
    sota_summit_prefix_country_mapping = {
        "4O": "MNE",
        "4X": "ISR",
        "5B": "CYP",
        "8P": "BRB",
        "9A": "HRV",
        "9H": "MLT",
        "9M": "MYS",
        "9V": "SGP",
        "A6": "ARE",
        "BV": "TWN",
        "C3": "AND",
        "CE": "CHL",
        "CT": "PRT",
        "CU": "PRT",
        "CX": "URY",
        "DU": "PHL",
        "D": "DEU",
        "E5": "COK",
        "E7": "BIH",
        "EA": "ESP",
        "EI": "IRL",
        "ER": "MDA",
        "ES": "EST",
        "FG": "GLP",
        "FH": "MYT",
        "FJ": "FRA",
        "FK": "NCL",
        "FM": "MTQ",
        "FO": "PYF",
        "FP": "FRA",
        "FR": "FRA",
        "FT": "FRA",
        "F": "FRA",
        "GD": "IMN",
        "GI": "GBR",
        "GJ": "JEY",
        "GM": "GBR",
        "GU": "GGY",
        "GW": "GBR",
        "G": "GBR",
        "HA": "HUN",
        "HB0": "LIE",
        "HB": "CHE",
        "HI": "DOM",
        "HL": "KOR",
        "HR": "HND",
        "IA": "ITA",
        "I": "ITA",
        "JA": "JPN",
        "JX": "SJM",
        "JW": "NOR",
        "KH0": "MNP",
        "KH2": "GUM",
        "KH8": "ASM",
        "KP4": "PRI",
        "K": "USA",
        "LA": "NOR",
        "LU": "ARG",
        "LX": "LUX",
        "LY": "LTU",
        "LZ": "BGR",
        "OD": "LBN",
        "OE": "AUT",
        "OH": "FIN",
        "OK": "CZE",
        "OM": "SVK",
        "ON": "BEL",
        "OY": "FRO",
        "OZ": "DNK",
        "PA": "NLD",
        "PP": "BRA",
        "PQ": "BRA",
        "PR": "BRA",
        "PS": "BRA",
        "PT": "BRA",
        "PY": "BRA",
        "R": "RUS",
        "S5": "SVN",
        "S7": "SYC",
        "SM": "SWE",
        "SP": "POL",
        "SV": "GRC",
        "TF": "ISL",
        "TI": "CRI",
        "TK": "FRA",
        "UT": "UKR",
        "V5": "NAM",
        "VE": "CAN",
        "VK": "AUS",
        "VO": "CAN",
        "VP8": "FLK",
        "VR": "HKG",
        "VY": "CAN",
        "W": "USA",
        "XE": "MEX",
        "XF": "MEX",
        "YB": "IDN",
        "YL": "LVA",
        "YO": "ROU",
        "YU": "SRB",
        "Z3": "MKD",
        "ZB2": "GIB",
        "ZD": "GBR",
        "ZL": "NZL",
        "ZS": "ZAF"
    }

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        sota_date_format = '%d/%m/%Y'
        sota_summits_dtypes = {
            'SummitCode': 'string',
            'AssociationName': 'string',
            'RegionName': 'string',
            'SummitName': 'string',
            'AltM': 'int64',
            'AltFt': 'int64',
            'GridRef1': 'string',
            'GridRef2': 'string',
            'Longitude': 'float64',
            'Latitude': 'float64',
            'Points': 'int64',
            'BonusPoints': 'int64',
            'ValidFrom': 'string',
            'ValidTo': 'string',
            'ActivationCount': 'int64',
            'ActivationDate': 'string',
            'ActivationCall': 'string'
        }
        sota_summits_parse_dates = ['ValidFrom', 'ValidTo', 'ActivationDate']
        sota_summits_col_map = {
            'SummitCode': 'callsign',
            'RegionName': 'state',
            'SummitName': 'name',
            'AltM': 'sea_level',
            'Longitude': 'long',
            'Latitude': 'lat'
        }

        if not args.offline:
            sota_summits_url = 'https://storage.sota.org.uk/summitslist.csv'
            processor.print_verbose(f"Fetching data from '{sota_summits_url}'")

            session = processor.requests_session()
            response = session.get(sota_summits_url, allow_redirects=True)
            sota_summits_url_final = response.url

            if response.history:
                processor.print_verbose(f"Request was redirected from {sota_summits_url} to {sota_summits_url_final}")

            if response.status_code == 200:
                data = pd.read_csv(filepath_or_buffer=BytesIO(response.content), index_col=None, header='infer', quoting=QUOTE_MINIMAL, encoding='utf-8', skiprows=1, dtype=sota_summits_dtypes, parse_dates=sota_summits_parse_dates, dayfirst=True, date_format=sota_date_format)
            else:
                processor.print_verbose(f"Failed to retrieve data: {response.status_code}")
                return None
        else:
            return None


        # Rename columns
        data.rename(columns=sota_summits_col_map, inplace=True)


        # Generate additional data
        data['band'] = None

        data['freq_rx'] = None
        data['freq_tx'] = None

        data['fm'] = False
        data['dmr'] = False
        data['dstar'] = False

        data['dmr_id'] = None

        data['ctcss_tx'] = None
        data['ctcss_rx'] = None

        data['dstar_rpt1'] = None
        data['dstar_rpt2'] = None

        data['loc_exact'] = True
        data['locator'] = None

        data['landmark'] = None
        data[['country_code', 'country']] = data.apply(self.calc_country, axis=1, result_type='expand')

        data['scan_group'] = None

        data['source_id'] = 'sota-summits'
        data['source_name'] = 'SOTA Summits'
        data['source_provider'] = 'Summits on the Air'
        data['source_type'] = SOURCE_TYPE_DYNAMIC
        data['source_license'] = None
        data['source_license_url'] = None
        data['source_url'] = 'https://www.sotadata.org.uk/de/summits'

        data['TodayDate'] = pd.Timestamp('today')
        data[['ValidFromDate', 'ValidToDate']] = data.apply(self.calc_datetime, args=(sota_date_format,), axis=1, result_type='expand')


        # Filtering
        data_query = []

        # Filter out inactive
        processor.print_verbose("Filter out inactive")
        data_query.append("ValidFromDate <= TodayDate")
        data_query.append("TodayDate <= ValidToDate")

        # Filtering - include only values defined in args
        filter_countries_flat = strip_duplicates(flatten_array(args.filter_countries))
        if not is_empty(filter_countries_flat):
            processor.print_verbose(f"Filter for countries: {filter_countries_flat}")
            data_query.append(f"country_code in {filter_countries_flat}")

        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            processor.print_verbose(f"Filter dataset: {data_query_str}")
            data.query(data_query_str, inplace=True)


        # Return dataset
        return data[get_data_source_col_names()]

    def calc_country(self, row) -> [str, str]:
        country_code = ''
        country_name = ''
        if not is_empty(row['callsign']):
            summit_association = row['callsign'].split("/")[0]
            country_code_matches = [val for key, val in self.sota_summit_prefix_country_mapping.items()
                                    if summit_association.startswith(key)]

            if country_code_matches is not None and len(country_code_matches) > 0:
                if not is_empty(country_code_matches[0]):
                    country_code = country_code_matches[0]
                    country = pycountry.countries.get(alpha_3=country_code)
                    if not is_empty(country):
                        country_name = country.name

        return [country_code, country_name]

    def calc_datetime(self, row, date_format: str) -> [Timestamp, Timestamp]:
        valid_from = None
        valid_to = None

        if not is_empty(date_format):
            try:
                valid_from = pd.to_datetime(row['ValidFrom'], format=date_format)
            except:
                valid_from = None

            try:
                valid_to = pd.to_datetime(row['ValidTo'], format=date_format)
            except:
                valid_to = None

        return [valid_from, valid_to]
