import argparse

from pandas import DataFrame

from .base_jinja2 import Jinja2BaseTarget
from memory_channels_processor.processor import MemoryChannelsProcessor


class OpenGD77Target(Jinja2BaseTarget):

    _alias_ = 'opengd77'

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)

    def prepare_data(self, data: DataFrame, processor: MemoryChannelsProcessor, args: argparse.Namespace, guessed_output_type: str = None) -> DataFrame:
        return data

    def get_template_name(self, args: argparse.Namespace, guessed_output_type: str = None) -> str:
        return 'opengd77_channels.csv.tpl'
