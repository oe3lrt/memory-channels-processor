Zone,CH Num,CH Name,RX Freq,TX Freq,RX TS,TX TS,RX CC,TX CC,Type,Power,Bandwidth,RX Only,Contact Name,RX Group Name,Scan List Name,Alarm,Prompt,PCT,MSG Type,TX Policy,EAS,TX Tone Type,TX Tone,RX Tone Type,RX Tone,APRS CH,Relay Monitor,Relay Mode
{%- for row in data.itertuples(index=True) %}
{%- if row['fm'] %}
{{ row['zone_name'] | default('', True) | replaceumlauts | removediacritic | substring(0,10) }},{{ row['zone_counter'] }},{{ row['name_formatted'] | default('', True) | substring(0,10) }},{{ "%.0f" | format(row['freq_rx'] * 1000 * 1000) }},{{ "%.0f" | format(row['freq_tx'] * 1000 * 1000) }},TS1,TS1,1,1,ANALOG,HIGH,12.5KHz,OFF,OFF,OFF,OFF,OFF,OFF,PATCS,UNCONFIRMED,IMPOLITE,OFF,{{ "CTCSS" if (row['ctcss_tx'] != None and row['ctcss_tx'] > 0) else "OFF" }},{{ row['ctcss_tx'] | default("0", True) }},{{ "CTCSS" if (row['ctcss_rx'] != None and row['ctcss_rx'] > 0) else "OFF" }},{{ row['ctcss_rx'] | default("0", True) }},0,OFF,OFF
{%- elif row['dmr'] %}
{{ row['zone_name'] | default('', True) | replaceumlauts | removediacritic | substring(0,10) }},{{ row['zone_counter'] }},{{ row['name_formatted'] | default('', True) | substring(0,10) }},{{ "%.0f" | format(row['freq_rx'] * 1000 * 1000) }},{{ "%.0f" | format(row['freq_tx'] * 1000 * 1000) }},TS1,TS1,1,1,DIGITAL,HIGH,12.5KHz,OFF,OFF,OFF,OFF,OFF,OFF,PATCS,UNCONFIRMED,IMPOLITE,OFF,OFF,0,OFF,0,0,OFF,OFF
{%- endif -%}
{%- endfor %}

