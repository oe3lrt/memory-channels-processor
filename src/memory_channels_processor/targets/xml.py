import argparse
from typing import AnyStr

from pandas import DataFrame

from memory_channels_processor.processor import MemoryChannelsProcessor, is_empty
from memory_channels_processor.plugin_target import Target


class XmlTarget(Target):

    _alias_ = 'xml'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def store_data(self, processor: MemoryChannelsProcessor, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame):
        if is_empty(output_file_path):
            result = data.to_xml(path_or_buffer=None, index=False, elem_cols=super().get_columns(args), root_name="entries", row_name="entry", encoding='utf-8')
            print(result)
        else:
            processor.print_verbose(f"Writing out to: {output_file_path}")

            with open(file=output_file_path, mode='wb') as f:
                data.to_xml(path_or_buffer=f, index=False, elem_cols=super().get_columns(args), root_name="entries", row_name="entry", encoding='utf-8')

    def check_output_file_extension(self, output_file_extension: AnyStr) -> bool:
        pass
