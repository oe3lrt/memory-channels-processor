import argparse
import math
import os
from datetime import datetime
from typing import AnyStr

from pandas import DataFrame

from memory_channels_processor.processor import get_jinja2_filters, MemoryChannelsProcessor, force_text, \
    is_empty, strip_duplicates, flatten_array, DATA_TYPE_FM, DATA_TYPE_DMR, DATA_TYPE_DSTAR, path_resolve
from memory_channels_processor.plugin_target import Target


class Jinja2BaseTarget(Target):
    from jinja2 import (
        Environment,
    )

    _skipload_ = True

    output_encoding = 'utf-8'

    def __init__(self, output_encoding='utf-8', filename=__file__):
        super().__init__()
        self.output_encoding = output_encoding
        self.filename = filename
        self.template_path = path_resolve(os.path.join(os.getcwd(), os.path.dirname(filename)))

    def setup_args(self, parser: argparse.ArgumentParser):
        #if arg_is_undefined(parser, '--jinja2-extension'):
        #    parser.add_argument('--jinja2-extension', dest='jinja2_extensions', action='append', help='Jinja2 extension name', metavar='<name>', nargs='+', type=str, default=[])
        #if arg_is_undefined(parser, '--jinja2-lenient'):
        #    parser.add_argument('--jinja2-lenient', dest='jinja2_lenient', action='store_true', help='Disable strict mode', default=False)
        pass

    def custom_jinja2_filters(self) -> dict:
        # Return custom filters/functions
        custom_jinja_filters = get_jinja2_filters()
        custom_jinja_filters['format_ctcss'] = self.filter_format_ctcss
        custom_jinja_filters['format_coordinate'] = self.filter_format_coordinate
        custom_jinja_filters['format_callsign_dstar'] = self.filter_format_callsign_dstar
        return custom_jinja_filters

    def prepare_jinja2_env(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> Environment:
        from jinja2 import (
            __version__ as jinja_version,
            Environment,
            FileSystemLoader,
            StrictUndefined,
        )

        extensions = []
        #for ext in args.jinja2_extensions:
        #    # Allow shorthand and assume if it's not a module
        #    # path, it's probably trying to use builtin from jinja2
        #    if "." not in ext:
        #        ext = "jinja2.ext." + ext
        #    extensions.append(ext)

        # Starting with jinja2 3.1, `with_` and `autoescape` are no longer
        # able to be imported, but since they were default, let's stub them back
        # in implicitly for older versions.
        # We also don't track any lower bounds on jinja2 as a dependency, so
        # it's not easily safe to know it's included by default either.
        if tuple(jinja_version.split(".", 2)) < ("3", "1"):
            for ext in "with_", "autoescape":
                ext = "jinja2.ext." + ext
                if ext not in extensions:
                    extensions.append(ext)

        processor.print_verbose(f"Using template folder: {self.template_path}")
        env = Environment(
            loader=FileSystemLoader(self.template_path, encoding='utf-8'),
            extensions=extensions,
            keep_trailing_newline=True,
            autoescape=False,
            #trim_blocks=True,
            #lstrip_blocks=True,
        )

        #if args.jinja2_lenient:
        #    processor.print_verbose("Lenient mode")
        #else:
        env.undefined = StrictUndefined
        processor.print_verbose("Strict mode")

        # Add environ global
        env.globals["environ"] = lambda key: force_text(os.environ.get(key))
        env.globals['now'] = datetime.now

        # Register custom functions/functions
        custom_jinja_functions = self.custom_jinja2_filters()

        # Add as functions
        env.globals.update(custom_jinja_functions)

        # Add as filters
        env.filters.update(custom_jinja_functions)

        return env

    def store_data(self, processor: MemoryChannelsProcessor, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame):
        # Get the Jinja2 Environment
        env = self.prepare_jinja2_env(processor, args)

        # Add environ global
        env.globals["get_context"] = lambda: data

        # Get additional values
        guessed_output_type = self.guess_output_type(args, data)

        # Prepare data
        processor.print_verbose("Prepare data for processing with templates")

        # Flatten dataset to have one "mode" per row
        data = processor.flatten_data(data, args)

        if args.verbose:
            print("============")
            print("Dataset:")
            data.info()
            print(data)
            print("============")

        # Resolve the template name
        template_name = self.get_template_name(args, guessed_output_type)
        if is_empty(template_name):
            print("Unable to get a template for the specified options!")
            exit(1)

        # Additional data processing
        prepared_data = self.prepare_data(data, processor, args)
        if prepared_data is not None:
            data = prepared_data

        processor.print_verbose(f"Using template: {template_name}")
        result = env.get_template(template_name).render(args=args, data=data)

        if is_empty(output_file_path):
            print(result)
        else:
            processor.print_verbose(f"Writing out to: {output_file_path} (Encoding: {self.output_encoding})")
            with open(file=output_file_path, mode='w', encoding=self.output_encoding) as f:
                f.write(result)

    def check_output_file_extension(self, output_file_extension: AnyStr) -> bool:
        pass

    def prepare_data(self, data: DataFrame, processor: MemoryChannelsProcessor, args: argparse.Namespace, guessed_output_type: str = None) -> DataFrame:
        pass

    def get_template_name(self, args: argparse.Namespace, guessed_output_type: str = None) -> str:
        pass

    def guess_output_type(self, args: argparse.Namespace, data: DataFrame) -> str:
        result = None

        filter_types_flat = strip_duplicates(flatten_array(args.filter_types))
        if len(filter_types_flat) == 1:
            result = filter_types_flat[0]
        else:
            if self.is_all_type_fm_only(data):
                result = DATA_TYPE_FM
            elif self.is_all_type_dmr_only(data):
                result = DATA_TYPE_DMR
            elif self.is_all_type_dstar_only(data):
                result = DATA_TYPE_DSTAR
        return result

    def filter_format_ctcss(self, ctcss: any) -> str:
        """
        Format a given ctcss value
        :param ctcss: The ctcss value
        :return: The formatted ctcss value as str
        """
        if is_empty(ctcss):
            return ""
        elif isinstance(ctcss, str):
            return ctcss.replace("Hz", "")
        elif isinstance(ctcss, float) or isinstance(ctcss, int):
            return str(ctcss)

    def filter_format_coordinate(self, coordinate: any, precision: int = None) -> str:
        """
        Format a given coordinate and consider the precision if given
        :param coordinate: The coordinate
        :param precision: The optional precision value
        :return: The formatted coordinate as str
        """
        if is_empty(coordinate):
            return ""
        elif isinstance(coordinate, str):
            return coordinate.strip()
        elif isinstance(coordinate, float):
            if math.isnan(coordinate):
                return ""
            else:
                if precision is not None:
                    return str(round(coordinate, precision))
                else:
                    return str(coordinate)
        elif isinstance(coordinate, int):
            return str(coordinate)

    def filter_format_callsign_dstar(self, callsign: str, port: str) -> str:
        """
        Format a given callsign according to the D-STAR convention with port
        :param callsign: The callsign
        :param port: The port - may be A, B, C, D or G
        :return: The formatted D-STAR callsign with port
        """
        if is_empty(callsign):
            return ""
        elif is_empty(port):
            result = (callsign[:8]) if len(callsign) > 8 else callsign
            return result.upper()
        else:
            result = (callsign[:7]) if len(callsign) > 7 else callsign
            result = result.ljust(7)
            identifier = (port[:1]) if len(callsign) > 1 else port
            return result.upper() + identifier.upper()
