import argparse
from typing import AnyStr

from pandas import DataFrame

from memory_channels_processor.processor import MemoryChannelsProcessor, is_empty
from memory_channels_processor.plugin_target import Target


class ExcelTarget(Target):

    _alias_ = 'xlsx'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def store_data(self, processor: MemoryChannelsProcessor, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame):
        if is_empty(output_file_path):
            print("No output file given")
            exit(1)

        processor.print_verbose(f"Writing out to: {output_file_path}")
        data.to_excel(output_file_path, index=False, header=args.output_header, columns=super().get_columns(args), engine='xlsxwriter', sheet_name='memory_channels_processor-channels')

    def check_output_file_extension(self, output_file_extension: AnyStr) -> bool:
        return str.lower(output_file_extension) == ".xlsx"
