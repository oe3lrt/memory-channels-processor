import argparse

from latloncalc.latlon import Latitude, Longitude, LatLon
from pandas import DataFrame

from .base_jinja2 import Jinja2BaseTarget
from memory_channels_processor.processor import MemoryChannelsProcessor, is_empty, DATA_TYPE_FM, DATA_TYPE_DSTAR, \
    arg_is_undefined


class RTSystemsBaseTarget(Jinja2BaseTarget):

    _skipload_ = True

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)
        if arg_is_undefined(parser, '--rtsystems-type'):
            parser.add_argument('--rtsystems-type', dest='rtsystems_type', action='store', choices=[DATA_TYPE_FM, DATA_TYPE_DSTAR], help='The output type', type=str.lower, default=None)
        if arg_is_undefined(parser, '--rtsystems-group-number'):
            parser.add_argument('--rtsystems-group-number', dest='rtsystems_group_number', action='store', help='The group number', metavar='<number>', type=int, default=0)
        if arg_is_undefined(parser, '--rtsystems-group-name'):
            parser.add_argument('--rtsystems-group-name', dest='rtsystems_group_name', action='store', help='The group name', metavar='<name>', type=str, default="Default Group Name")

    def get_type(self, args: argparse.Namespace, guessed_output_type: str = None):
        if is_empty(args.rtsystems_type) and is_empty(guessed_output_type):
            result = DATA_TYPE_FM
        elif not is_empty(args.rtsystems_type):
            result = args.rtsystems_type
        else:
            result = guessed_output_type
        return result

    def prepare_data(self, data: DataFrame, processor: MemoryChannelsProcessor, args: argparse.Namespace, guessed_output_type: str = None) -> DataFrame:
        # Set values
        data.loc[data['offset'] > 0, 'offset_direction'] = "DUP-"
        data.loc[data['offset'] == 0, 'offset_direction'] = "Simplex"
        data.loc[data['offset'] < 0, 'offset_direction'] = "DUP+"

        data['rtsystems_group_number'] = args.rtsystems_group_number
        data['rtsystems_group_name'] = args.rtsystems_group_name

        # Additional params
        output_type = self.get_type(args, guessed_output_type)

        # Calculate values
        data['tone'] = data.apply(self.get_tone, args=(output_type,), axis=1)
        data['freq_offset'] = data.apply(self.get_freq_offset, axis=1)
        data['comment'] = data.apply(self.get_comment, axis=1)

        # Calculate - additional coordinate based values
        data['latitude'] = data.apply(self.get_latitude, args=('d%°%m%\'%S%"%H',), axis=1)
        data['longitude'] = data.apply(self.get_longitude, args=('d%°%m%\'%S%"%H',), axis=1)

        return data

    def get_tone(self, row, output_type: str) -> str:
        result = ""
        if output_type == DATA_TYPE_FM:
            if (not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0) and (not is_empty(row['ctcss_rx']) and row['ctcss_rx'] > 0):
                result = "Split Tone"
            elif not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0:
                result = "Tone"
            else:
                result = "None"
        elif output_type == DATA_TYPE_DSTAR:
            if not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0:
                result = "Tone"
            else:
                result = "None"

        return result

    def get_freq_offset(self, row) -> str:
        offset = float(row['offset'])
        if offset == 0:
            result = ' '
        elif offset < 1:
            result = f"%.0f kHz" % (offset * 1000)
        elif offset > 1:
            result = f"%.2f MHz" % offset
        else:
            result = ' '

        return result

    def get_latitude(self, row, output_format: str) -> str:
        if row['lat'] is not None and row['long'] is not None and not is_empty(output_format):
            try:
                latlon = LatLon(Latitude(row['lat']), Longitude(row['long']))
                result = latlon.to_string(output_format)[0]
            except:
                result = None
        else:
            result = None

        return result

    def get_longitude(self, row, output_format: str) -> str:
        if row['lat'] is not None and row['long'] is not None and not is_empty(output_format):
            try:
                latlon = LatLon(Latitude(row['lat']), Longitude(row['long']))
                result = latlon.to_string(output_format)[1]
            except:
                result = None
        else:
            result = None

        return result

    def get_comment(self, row) -> str:
        result = ""
        if row['name_formatted'] is not row['callsign']:
            result = row['callsign']

        return result


class RTSystemsWCS705Target(RTSystemsBaseTarget):

    _alias_ = 'rtsystems-wcs-705'

    def get_template_name(self, args: argparse.Namespace, guessed_output_type: str = None) -> str:
        output_type = self.get_type(args, guessed_output_type)

        template_name = None
        if output_type == DATA_TYPE_FM:
            template_name = "rtsystems_wcs-705_fm.csv.tpl"
        elif output_type == DATA_TYPE_DSTAR:
            template_name = "rtsystems_wcs-705_dstar.csv.tpl"

        return template_name
