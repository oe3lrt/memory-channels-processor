"Sequence Number","band","Country","State","Region","Location","Output Frequency","Input Frequency","Call Sign","Repeater Notes","CTCSS Tones","Sponsor","Distance","Bearing"
{%- set count = namespace(value=-1) %}
{%- for row in data.itertuples(index=False) %}
{%- if row['fm'] %}
{%- set count.value = count.value + 1 %}
"{{ '%02d' | format(count.value) }}","{{ row['band_formatted'] | default('', True) }}","{{ row['country'] | default('', True) | replace_umlauts | remove_diacritic }}","{{ row['state'] | default('', True) | replace_umlauts | remove_diacritic }}","","{{ row['location'] | default('', True) | replace_umlauts | remove_diacritic }}","{{ row['freq_rx'] | replace(',', '.') }}","{{ row['freq_tx'] | replace(',', '.') }}","{{ row['callsign'] | default('', True) }}","{{ row['name_formatted'] | replace_umlauts | remove_diacritic }}","{{ row['ctcss_tx'] | default('88.5', True) | format_ctcss | replace(',', '.') }}","{{ row['name_formatted'] | replace_umlauts | remove_diacritic }}","{{ row['distance'] | default('', True) }}","{{ row['heading'] | default('', True) }}"
{%- endif -%}
{%- endfor %}
