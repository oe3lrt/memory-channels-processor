import argparse

from latloncalc.latlon import LatLon, Latitude, Longitude
from pandas import DataFrame

from .base_jinja2 import Jinja2BaseTarget
from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty, DATA_TYPE_FM, DATA_TYPE_DSTAR,
                                                 arg_is_undefined)


class KenwoodBaseTarget(Jinja2BaseTarget):

    _skipload_ = True

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)
        if arg_is_undefined(parser, '--kenwood-type'):
            parser.add_argument('--kenwood-type', dest='kenwood_type', action='store', choices=[DATA_TYPE_FM, DATA_TYPE_DSTAR], help='The output type', type=str.lower, default=None)
        if arg_is_undefined(parser, '--kenwood-world-number'):
            parser.add_argument('--kenwood-world-number', dest='kenwood_world_number', action='store', help='The world number (1-6)', metavar='<number>', type=int, default=4)
        if arg_is_undefined(parser, '--kenwood-world-name'):
            parser.add_argument('--kenwood-world-name', dest='kenwood_world_name', action='store', help='The world name', metavar='<name>', type=str, default="Europe")
        if arg_is_undefined(parser, '--kenwood-country-number'):
            parser.add_argument('--kenwood-country-number', dest='kenwood_country_number', action='store', help='The country number (1-150)', metavar='<number>', type=int, default=9)
        if arg_is_undefined(parser, '--kenwood-country-name'):
            parser.add_argument('--kenwood-country-name', dest='kenwood_country_name', action='store', help='The country name', metavar='<name>', type=str, default="Austria")
        if arg_is_undefined(parser, '--kenwood-group-number'):
            parser.add_argument('--kenwood-group-number', dest='kenwood_group_number', action='store', help='The group number (1-300)', metavar='<number>', type=int, default=75)
        if arg_is_undefined(parser, '--kenwood-group-name'):
            parser.add_argument('--kenwood-group-name', dest='kenwood_group_name', action='store', help='The group name', metavar='<name>', type=str, default="Default")

    def get_type(self, args: argparse.Namespace, guessed_output_type: str = None):
        if is_empty(args.kenwood_type) and is_empty(guessed_output_type):
            result = DATA_TYPE_FM
        elif not is_empty(args.kenwood_type):
            result = args.kenwood_type
        else:
            result = guessed_output_type
        return result

    def prepare_data(self, data: DataFrame, processor: MemoryChannelsProcessor, args: argparse.Namespace, guessed_output_type: str = None) -> DataFrame:
        # Additional params
        output_type = self.get_type(args, guessed_output_type)

        # Calculate - additional coordinate based values
        data['lat_dd'] = data.apply(self.get_lat, args=('d%',), axis=1)
        data['lat_mm'] = data.apply(self.get_lat, args=('%M%',), axis=1)
        data['lat_direction'] = data.apply(self.get_lat, args=('%H',), axis=1)
        data['long_dd'] = data.apply(self.get_lon, args=('d%',), axis=1)
        data['long_mm'] = data.apply(self.get_lon, args=('%M%',), axis=1)
        data['long_direction'] = data.apply(self.get_lon, args=('%H',), axis=1)

        data['band_formatted'] = data.apply(self.get_band_formatted, args=(output_type,), axis=1)

        return data

    def get_lat(self, row, output_format: str) -> str:
        if row['lat'] is not None and row['long'] is not None and not is_empty(output_format):
            try:
                latlon = LatLon(Latitude(row['lat']), Longitude(row['long']))
                result = latlon.to_string(output_format)[0]
            except:
                result = None
        else:
            result = None

        return result

    def get_lon(self, row, output_format: str) -> str:
        if row['lat'] is not None and row['long'] is not None and not is_empty(output_format):
            try:
                latlon = LatLon(Latitude(row['lat']), Longitude(row['long']))
                result = latlon.to_string(output_format)[1]
            except:
                result = None
        else:
            result = None

        return result

    def get_band_formatted(self, row, output_type: str) -> str:
        result = ""
        if output_type == DATA_TYPE_FM:
            if not is_empty(row['band']):
                if row['band'] == "70cm":
                    result = "430-440 MHz (70 cm)"
                elif row['band'] == "2m":
                    result = "144-148 MHz (2 m)"

        return result
    

class KenwoodTHD74Target(KenwoodBaseTarget):

    _alias_ = 'kenwood-th-d74'

    def get_template_name(self, args: argparse.Namespace, guessed_output_type: str = None) -> str:
        output_type = self.get_type(args, guessed_output_type)

        template_name = None
        if output_type == DATA_TYPE_FM:
            # Initialize Jinja for UTF-8 encoded output
            self.output_encoding = 'utf-8'
            template_name = "kenwood_th_d74_fm.csv.tpl"
        elif output_type == DATA_TYPE_DSTAR:
            # Initialize Jinja for UTF-16 encoded output
            self.output_encoding = 'utf-16'
            template_name = "kenwood_th_d74_dstar.tsv.tpl"

        return template_name
