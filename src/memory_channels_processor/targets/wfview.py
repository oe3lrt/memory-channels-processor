import argparse

from pandas import DataFrame

from .base_jinja2 import Jinja2BaseTarget
from memory_channels_processor.processor import MemoryChannelsProcessor, is_empty, DATA_TYPE_FM, DATA_TYPE_DSTAR


class WfviewTarget(Jinja2BaseTarget):

    _alias_ = 'wfview'

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)

    def get_type(self, args: argparse.Namespace, guessed_output_type: str = None):
        if is_empty(args.rtsystems_type) and is_empty(guessed_output_type):
            result = DATA_TYPE_FM
        elif not is_empty(args.rtsystems_type):
            result = args.rtsystems_type
        else:
            result = guessed_output_type
        return result

    def prepare_data(self, data: DataFrame, processor: MemoryChannelsProcessor, args: argparse.Namespace, guessed_output_type: str = None) -> DataFrame:
        # Set values
        data.loc[data['offset'] > 0, 'duplex'] = "DUP-"
        data.loc[data['offset'] == 0, 'duplex'] = "OFF"
        data.loc[data['offset'] < 0, 'duplex'] = "DUP+"

        # Additional params
        output_type = self.get_type(args, guessed_output_type)

        # Calculate values
        data['tone'] = data.apply(self.get_tone, args=(output_type,), axis=1)
        data['freq_offset'] = data.apply(self.get_freq_offset, axis=1)
        data['scan'] = data.apply(self.get_scan, axis=1)

        return data

    def get_tone(self, row, output_type: str) -> str:
        result = ""
        if output_type == DATA_TYPE_FM:
            if (not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0) and (not is_empty(row['ctcss_rx']) and row['ctcss_rx'] > 0):
                result = "TSQL"
            elif not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0:
                result = "TONE"
            else:
                result = "OFF"
        elif output_type == DATA_TYPE_DSTAR:
            if not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0:
                result = "TONE"
            else:
                result = "OFF"

        return result

    def get_freq_offset(self, row) -> str:
        offset = float(row['offset'])
        if offset == 0:
            result = '0.000'
        elif offset < 1:
            result = f"%.3f" % offset
        elif offset > 1:
            result = f"%.3f" % offset
        else:
            result = ''

        return result

    def get_scan(self, row) -> str:
        result = "OFF"
        if not is_empty(row['scan_group']) and 1 <= int(row['scan_group']) <= 3:
            result = "*" + str(int(row['scan_group']))

        return result

    def get_template_name(self, args: argparse.Namespace, guessed_output_type: str = None) -> str:
        return 'wfview.csv.tpl'
