Channel Number,Bank,Receive Frequency,Transmit Frequency,Offset Frequency,Offset Direction,Repeater Use,Operating Mode,Name,Sub Name,Tone Mode,CTCSS,Rpt-1 CallSign,Rpt-2 CallSign,LatLng,Latitude,Longitude,UTC Offset,Comment
{%- set count = namespace(value=-1) %}
{%- for row in data.itertuples(index=False) %}
{%- if row['fm'] %}
{%- set count.value = count.value + 1 %}
{%- if row['band'] != "radio" %}
{{ '%01d' | format(count.value) }},{{ '%01d' | format(args.rtsystems_group_number | default(0, True)) }}: {{ args.rtsystems_group_name | default('', True) | substring(0,16) }},{{ "%.6f" | format(row['freq_rx']) | replace(',', '.') }},{{ "%.6f" | format(row['freq_tx']) | replace(',', '.') }},{{ row['freq_offset'] | default('', True) }},{{ row['offset_direction'] | default('', True) }},On,FM,{{ row['name_formatted'] | replace_umlauts | remove_diacritic | substring(0,16) }},{{ row['callsign'] | default('', True) }},{{ row['tone'] }},{{ row['ctcss_tx'] | default('88,5', True) | format_ctcss | replace(',', '.') }},{{ row['callsign'] | default('', True) }},,{{ "Exact" if row['loc_exact'] else "Approximate" | default('None', True) }},{{ row['latitude'] | default('', True) }},{{ row['longitude'] | default('', True) }},+1:00,
{%- endif -%}
{%- elif row['dstar'] %}
{%- set count.value = count.value + 1 %}
{{ '%01d' | format(count.value) }},{{ '%01d' | format(args.rtsystems_group_number | default(0, True)) }}: {{ args.rtsystems_group_name | default('', True) | substring(0,16) }},{{ "%.6f" | format(row['freq_rx']) | replace(',', '.') }},{{ "%.6f" | format(row['freq_tx']) | replace(',', '.') }},{{ row['freq_offset'] | default('', True) }},{{ row['offset_direction'] | default('', True) }},On,DV,{{ row['name_formatted'] | replace_umlauts | remove_diacritic | substring(0,16) }},{{ row['callsign'] | default('', True) }},{{ row['tone'] }},{{ row['ctcss_tx'] | default('88,5', True) | format_ctcss | replace(',', '.') }},{{ row['callsign'] | format_callsign_dstar(row['dstar_rpt1']) | default('', True) }},{{ row['callsign'] | format_callsign_dstar(row['dstar_rpt2']) | default('', True) }},{{ "Exact" if row['loc_exact'] else "Approximate" | default('None', True) }},{{ row['latitude'] | default('', True) }},{{ row['longitude'] | default('', True) }},+1:00,
{%- endif -%}
{%- endfor %}
