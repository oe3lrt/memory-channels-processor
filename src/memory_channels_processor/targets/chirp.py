import argparse

from pandas import DataFrame

from .base_jinja2 import Jinja2BaseTarget
from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty)


class ChirpTarget(Jinja2BaseTarget):

    _alias_ = 'chirp'

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)

    def prepare_data(self, data: DataFrame, processor: MemoryChannelsProcessor, args: argparse.Namespace, guessed_output_type: str = None) -> DataFrame:
        # Set values
        data.loc[data['offset'] > 0, 'dup'] = "-"
        data.loc[data['offset'] < 0, 'dup'] = "+"

        # Calculate
        data['tone'] = data.apply(self.get_tone, args=(args,), axis=1)
        data['chirp_comment'] = data.apply(self.get_comment, args=(args,), axis=1)

        return data

    def get_template_name(self, args: argparse.Namespace, guessed_output_type: str = None) -> str:
        return 'chirp_next.csv.tpl'

    def get_tone(self, row, args: argparse.Namespace) -> str:
        result = ""
        if (not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0) and (not is_empty(row['ctcss_rx']) and row['ctcss_rx'] > 0):
            result = "TSQL"
        elif not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0:
            result = "Tone"

        return result

    def get_comment(self, row, args: argparse.Namespace) -> str:
        return row['name'] + ' ' + row['band']
