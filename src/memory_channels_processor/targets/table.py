import argparse
import contextlib
from typing import AnyStr

from pandas import DataFrame

from rich.errors import NotRenderableError

from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty)
from memory_channels_processor.targets.base_table import TableBaseTarget


class TableTarget(TableBaseTarget):

    _alias_ = 'table'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def store_data(self, processor: MemoryChannelsProcessor, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame):
        column_keys = super().get_columns(args)
        if is_empty(column_keys):
            column_keys = self.get_default_column_keys()

            # Add extra column
            if args.consistency_check:
                column_keys.append('issues')

        table_data = self.format_table(data[column_keys])
        self.print_rich_display_dataframe(processor, table_data, output_file_path)

    def print_rich_display_dataframe(self, processor: MemoryChannelsProcessor, data: DataFrame, output_file_path: AnyStr = None, title: str = None) -> None:
        """Display dataframe as table using rich library.
        Args:
            processor (MemoryChannelsProcessor): Instance of the MemoryChannelsProcessor
            data (pd.DataFrame): dataframe to display
            output_file_path (AnyStr, optional): Path to output file
            title (str, optional): title of the table. Defaults to "Dataframe"
        Raises:
            NotRenderableError: if dataframe cannot be rendered
        Returns:
            None
        """
        from rich import print
        from rich.console import Console
        from rich.table import Table

        table = Table(title=title, show_footer=False)
        for col in data.columns:
            table.add_column(col)
        for index, row in data.iterrows():
            with contextlib.suppress(NotRenderableError):
                if 'issues' in row:
                    row_style = 'bright_red' if len(row['issues']) > 0 else 'bright_green'
                else:
                    row_style = None

                row_printable = [self.get_cell_mapped_value(el) for el in row.tolist()]
                table.add_row(*row_printable, style=row_style)

        if is_empty(output_file_path):
            console = Console()
            console.print(table)
        else:
            processor.print_verbose(f"Writing out to: {output_file_path}")

            with open(file=output_file_path, mode='w', encoding="utf-8") as f:
                console = Console(file=f)
                console.print(table)


    def check_output_file_extension(self, output_file_extension: AnyStr) -> bool:
        pass
