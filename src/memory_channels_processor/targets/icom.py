import argparse
import string

from pandas import DataFrame

from .base_jinja2 import Jinja2BaseTarget
from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty, DATA_TYPE_FM, DATA_TYPE_DSTAR,
                                                 arg_is_undefined)

ICOM_TYPE_FM_RADIO = "fm-radio"
ICOM_TYPE_GPS = "gps"

class IcomBaseTarget(Jinja2BaseTarget):

    _skipload_ = True

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)
        if arg_is_undefined(parser, '--icom-type'):
            parser.add_argument('--icom-type', dest='icom_type', action='store', choices=[DATA_TYPE_FM, DATA_TYPE_DSTAR, ICOM_TYPE_FM_RADIO, ICOM_TYPE_GPS], help='The output type', type=str.lower, default=None)
        if arg_is_undefined(parser, '--icom-group-number'):
            parser.add_argument('--icom-group-number', dest='icom_group_number', action='store', help='The group number', metavar='<number>', type=int, default=0)
        if arg_is_undefined(parser, '--icom-group-name'):
            parser.add_argument('--icom-group-name', dest='icom_group_name', action='store', help='The group name', metavar='<name>', type=str, default="Default Group Name")

    def get_type(self, args: argparse.Namespace, guessed_output_type: str = None):
        if is_empty(args.icom_type) and is_empty(guessed_output_type):
            result = DATA_TYPE_FM
        elif not is_empty(args.icom_type):
            result = args.icom_type
        else:
            result = guessed_output_type
        return result

    def prepare_data(self, data: DataFrame, processor: MemoryChannelsProcessor, args: argparse.Namespace, guessed_output_type: str = None) -> DataFrame:
        # Set values
        data.loc[data['offset'] > 0, 'dup'] = "DUP-"
        data.loc[data['offset'] < 0, 'dup'] = "DUP+"

        data['icom_group_number'] = args.icom_group_number
        data['icom_group_number_alphabetical'] = self.get_group_number_alphabetical(args.icom_group_number)
        data['icom_group_name'] = args.icom_group_name

        # Additional params
        output_type = self.get_type(args, guessed_output_type)

        # Calculate - Tone
        data['tone'] = data.apply(self.get_tone, args=(output_type,), axis=1)

        # Calculate - Mode
        data['mode'] = data.apply(self.get_mode, args=(output_type,), axis=1)

        # Calculate - Memory Skip
        data['sel'] = data.apply(self.get_scan_sel, axis=1)
        if output_type == DATA_TYPE_FM:
            # For template "icom_fm.csv.tpl"
            if not (len(data['sel'].unique()) == 1 and data['sel'].unique()[0] == "OFF"):
                data['skip'] = data.apply(self.get_scan_skip, axis=1)
            else:
                data['skip'] = "OFF"
        elif output_type == DATA_TYPE_DSTAR:
            # For template "icom_dstar.csv.tpl"
            if not (len(data['sel'].unique()) == 1 and data['sel'].unique()[0] == "OFF"):
                data['rpt_use'] = data.apply(self.get_rpt_use, axis=1)
            else:
                data['rpt_use'] = "YES"

        return data

    def get_tone(self, row, output_type: str) -> str:
        result = ""
        if output_type == DATA_TYPE_FM:
            if (not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0) and (not is_empty(row['ctcss_rx']) and row['ctcss_rx'] > 0):
                result = "TONE(T)/TSQL(R)"
            elif not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0:
                result = "TONE"
            else:
                result = "OFF"
        elif output_type == DATA_TYPE_DSTAR:
            if not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0:
                result = "TONE"
            else:
                result = "OFF"

        return result

    def get_mode(self, row, output_type: str) -> str:
        result = ""
        if output_type == DATA_TYPE_FM:
            if row['fm']:
                if row['band'] == 'radio':
                    result = 'WFM'
                else:
                    result = 'FM'
            elif row['dstar']:
                result = 'DV'

        elif output_type == DATA_TYPE_DSTAR:
            if row['fm']:
                result = 'FM'
            elif row['dstar']:
                result = 'DV'

        return result

    def get_scan_sel(self, row) -> str:
        result = "OFF"
        if not is_empty(row['scan_group']) and 1 <= int(row['scan_group']) <= 3:
            result = "SEL" + str(int(row['scan_group']))

        return result

    def get_scan_skip(self, row) -> str:
        result = "OFF"
        if not is_empty(row['sel']) and (row['sel'] == "OFF"):
            result = "Skip"

        return result

    def get_rpt_use(self, row) -> str:
        result = "YES"
        if not is_empty(row['sel']) and (row['sel'] == "OFF"):
            result = "NO"

        return result

    def get_group_number_alphabetical(self, group_number: int) -> str:
        result = ""
        if group_number == 0:
            result = "A"
        elif group_number > 0:
            alphabet = string.ascii_uppercase
            index = group_number % len(alphabet)
            result = alphabet[index - 1]

        return result


class IcomIC9700IC705ID52Target(IcomBaseTarget):

    _alias_ = 'icom'

    def get_template_name(self, args: argparse.Namespace, guessed_output_type: str = None) -> str:
        output_type = self.get_type(args, guessed_output_type)

        template_name = None
        if output_type == DATA_TYPE_FM:
            template_name = "icom_fm.csv.tpl"
        elif output_type == DATA_TYPE_DSTAR:
            template_name = "icom_dstar.csv.tpl"
        elif output_type == ICOM_TYPE_FM_RADIO:
            template_name = "icom_fm_radio.csv.tpl"
        elif output_type == ICOM_TYPE_GPS:
            template_name = "icom_gps.csv.tpl"

        return template_name
