Location,Name,Frequency,Duplex,Offset,Tone,rToneFreq,cToneFreq,DtcsCode,DtcsPolarity,RxDtcsCode,CrossMode,Mode,TStep,Skip,Comment,URCALL,RPT1CALL,RPT2CALL,DVCODE
{%- set count = namespace(value=-1) %}
{%- for row in data.itertuples(index=True) %}
{%- if row['fm'] %}
{%- set count.value = count.value + 1 %}
{{ '%02d' | format(count.value) }},{{ row['name_formatted'] | default('', True) }},{{ row['freq_rx'] }},{{ row['dup'] | default('', True) }},{{ row['offset'] | default('', True) }},{{ row['tone'] }},{{ row['ctcss_rx'] | default('88,5', True) | format_ctcss | replace(',', '.') }},{{ row['ctcss_tx'] | default('88,5', True) | format_ctcss | replace(',', '.') }},023,NN,023,Tone->Tone,FM,5,,{{ row['chirp_comment'] }},,,,
{%- elif row['dstar'] %}
{%- set count.value = count.value + 1 %}
{{ '%02d' | format(count.value) }},{{ row['name_formatted'] | default('', True) }},{{ row['freq_rx'] }},{{ row['dup'] | default('', True) }},{{ row['offset'] | default('', True) }},{{ row['tone'] }},{{ row['ctcss_rx'] | default('88,5', True) | format_ctcss | replace(',', '.') }},{{ row['ctcss_tx'] | default('88,5', True) | format_ctcss | replace(',', '.') }},023,NN,023,Tone->Tone,DV,5,,{{ row['chirp_comment'] }},,{{ row['callsign'] | format_callsign_dstar(row['dstar_rpt1']) | default('', True) }},{{ row['callsign'] | format_callsign_dstar(row['dstar_rpt2']) | default('', True) }},
{%- endif -%}
{%- endfor %}