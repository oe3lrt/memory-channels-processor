import argparse
from _csv import QUOTE_MINIMAL
from typing import AnyStr

from pandas import DataFrame

from memory_channels_processor.processor import (MemoryChannelsProcessor, is_empty)
from memory_channels_processor.plugin_target import Target


class CsvTarget(Target):

    _alias_ = 'csv'

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def store_data(self, processor: MemoryChannelsProcessor, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame):
        if is_empty(output_file_path):
            result = data.to_csv(path_or_buf=None, index=False, header=args.output_header, columns=super().get_columns(args), quoting=QUOTE_MINIMAL, encoding='utf-8')
            print(result)
        else:
            processor.print_verbose(f"Writing out to: {output_file_path}")
            with open(file=output_file_path, mode='wb') as f:
                data.to_csv(path_or_buf=f, index=False, header=args.output_header, columns=super().get_columns(args), quoting=QUOTE_MINIMAL, encoding='utf-8', lineterminator='\r\n')

    def check_output_file_extension(self, output_file_extension: AnyStr) -> bool:
        pass


