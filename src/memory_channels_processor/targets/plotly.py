import argparse
import itertools
from typing import AnyStr

import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from pandas import DataFrame

from memory_channels_processor.processor import (MemoryChannelsProcessor, arg_is_undefined, is_empty, get_bands, get_types, get_type_mappings,
                                                 DATA_TYPE_FM, \
                                                 DATA_TYPE_DSTAR, DATA_TYPE_DMR, get_band_by_freq, get_row_count)
from memory_channels_processor.plugin_target import Target


class PlotlyTarget(Target):

    _alias_ = 'map'

    def setup_args(self, parser: argparse.ArgumentParser):
        if arg_is_undefined(parser, '--map-output-type'):
            parser.add_argument('--map-output-type', dest='map_output_type', action='store', choices=['file', 'inline'], help=argparse.SUPPRESS, type=str.lower, default='file')

    def store_data(self, processor: MemoryChannelsProcessor, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame):
        # Filter out items without coordinates
        data.query("lat.notnull() and long.notnull()", inplace=True)

        # ================
        # Prepare data
        # ================
        processor.print_verbose("Prepare data for processing")

        # Flatten dataset to have one "mode" per row
        df = processor.flatten_data(data, args)

        if df.empty:
            print("No data to output")
            exit(1)

        df['tooltip'] = df.apply(self.calc_tooltip, axis=1)
        df['color_key'] = df.apply(self.calc_color_key, axis=1)

        if args.verbose:
            print("============")
            print("Dataset:")
            df.info()
            print(df)
            print("============")

        # ================
        # Prepare colors
        # ================
        color_combinations = itertools.product(get_bands() + ['no-band'], get_types() + ['no-type'])
        color_index = 5
        colors = dict()
        for band, data_type in color_combinations:
            item_key = str(f"{band}_{data_type}")
            color = px.colors.qualitative.Dark24[color_index % (len(px.colors.qualitative.Dark24) - 1)]
            colors[item_key] = color
            color_index += 1

        # ================
        # Prepare attribution
        # ================
        attributions = self.get_attributions(df)
        attributions_text = "Data providers: " + ", ".join(attributions)

        # ================
        # Prepare map
        # ================

        # Calculate zoom and center
        zoom, center = self.get_zoom_center(df)

        # Create figure
        fig = go.Figure()

        # Config options (see https://plotly.com/javascript/configuration-options/)
        config = {
            'staticPlot': False,
            'scrollZoom': True,
            'editable': False,
            'responsive': True,
            'modeBarButtonsToRemove': [
                'editInChartStudio',
                'select2d',
                'lasso2d'
            ],
            'displaylogo': True,
            'watermark': False,
        }

        processor.print_verbose("------------")

        # Group by bands
        layers = dict()
        for item_band in df["band"].unique().tolist():
            processor.print_verbose(f"Processing band '{item_band}'")
            if item_band is not None:
                df_band = df[df.band == item_band]
                band_name = item_band
            else:
                df_band = df[df.band.isnull()]
                band_name = 'no-band'

            if args.verbose:
                print("============")
                print("Dataset:")
                df_band.info()
                print(df_band)
                print("============")

            processor.print_verbose(f"Items: '{get_row_count(df_band)}' (band)")

            if not df_band.empty:
                for data_type in get_types() + [None]:
                    processor.print_verbose(f"Processing type '{data_type}'")

                    if data_type is not None:
                        type_query_str = '(' + get_type_mappings()[data_type] + ')'
                        band_type_name = data_type
                    else:
                        type_query_str = '(not ((' + ') or ('.join(['' + item + '' for item in get_type_mappings().values()]) + ')))'
                        band_type_name = 'no-type'

                    processor.print_verbose(f"Filter dataset: {type_query_str}")
                    df_band_type = df_band.query(type_query_str, inplace=False)

                    if args.verbose:
                        print("============")
                        print("Dataset:")
                        df_band_type.info()
                        print(df_band_type)
                        print("============")

                    processor.print_verbose(f"Items: '{get_row_count(df_band_type)}' (band + type)")

                    if not df_band_type.empty:
                        item_name = str(f"{band_name}_{band_type_name}")
                        layer_name = str(f"{band_name} ({band_type_name})")

                        processor.print_verbose(f"Item name: '{item_name}'")
                        processor.print_verbose(f"Layer name: '{layer_name}'")

                        fig_tmp = px.scatter_mapbox(df_band_type,
                                                    lat="lat",
                                                    lon="long",
                                                    text="name_formatted",
                                                    color="color_key",
                                                    color_discrete_map=colors,
                                                    custom_data='tooltip',

                                                    )

                        fig_tmp.update_traces(
                            name=layer_name,
                            marker=go.scattermapbox.Marker(
                                size=12,
                                color=colors[item_name],
                                opacity=0.8
                            ),
                            textposition='bottom center',
                            textfont_size=14,
                            hovertemplate="%{customdata}<br>" +
                                          "<extra></extra>",

                            # cluster=dict(enabled=True)
                        )

                        layer = {
                            "type": "line",
                            "color": colors[item_name]
                        }
                        layers[layer_name] = layer

                        fig.add_trace(fig_tmp.data[0])

                    processor.print_verbose("------------")

        fig.update_layout(mapbox=dict(style="open-street-map",
                                      center=center,
                                      zoom=zoom
                                      ),
                          margin={"r": 0, "t": 0, "l": 0, "b": 0},
                          mapbox_layers=list(layers.values()),
                          annotations=[
                              go.layout.Annotation(
                                  text=attributions_text,
                                  showarrow=False,
                                  xref='paper',
                                  yref='paper',
                                  x=0,
                                  y=0
                              )
                          ]
                          )

        fig.update_layout(showlegend=True,
                          legend=dict(
                              xanchor="left",
                              x=0.001,
                              yanchor="top",
                              y=0.999
                          ))

        if is_empty(output_file_path):
            processor.print_verbose("Directly show interactive figure")
            fig.show(config=config)
        else:
            if args.map_output_type == 'file':
                # Write HTML file out
                processor.print_verbose(f"Writing out to: {output_file_path}")
                fig.write_html(output_file_path, config=config, include_plotlyjs=True)
            elif args.map_output_type == 'inline':
                # Write HTML snippet out
                processor.print_verbose(f"Writing out to: {output_file_path}")
                fig.write_html(output_file_path, config=config, include_plotlyjs=False, full_html=False)

    def check_output_file_extension(self, output_file_extension: AnyStr) -> bool:
        pass

    def get_zoom_center(self, data: DataFrame, width_to_height: float=2.0) -> (float, dict):
        """Finds optimal zoom and centering for a plotly mapbox.

        Parameters
        --------
        data: DataFrame, ddd
        width_to_height: float, expected ratio of final graph's with to height,
            used to select the constrained axis.

        Returns
        --------
        zoom: float, from 1 to 20
        center: dict, gps position with 'lon' and 'lat' keys
        """

        if data is None or data.empty:
            return 8, None

        minlon, maxlon = data['long'].min(), data['long'].max()
        minlat, maxlat = data['lat'].min(), data['lat'].max()
        center = {
            'lon': round((maxlon + minlon) / 2, 6),
            'lat': round((maxlat + minlat) / 2, 6)
        }

        # longitudinal range by zoom level (20 to 1)
        # in degrees, if centered at equator
        lon_zoom_range = np.array([
            0.0007, 0.0014, 0.003, 0.006, 0.012, 0.024, 0.048, 0.096,
            0.192, 0.3712, 0.768, 1.536, 3.072, 6.144, 11.8784, 23.7568,
            47.5136, 98.304, 190.0544, 360.0
        ])

        height = (maxlat - minlat)
        width = (maxlon - minlon)

        lon_zoom = float(np.interp(width, lon_zoom_range, range(20, 0, -1)))
        lat_zoom = float(np.interp(height, lon_zoom_range, range(20, 0, -1)))

        zoom = round(min(lon_zoom, lat_zoom), 2)

        return zoom, center

    def calc_tooltip(self, row):
        result = "<b><i>" + str(row['name_formatted']) + "</i>"
        if not is_empty(row['callsign']) and row['callsign'] != row['name_formatted']:
            result += " (" + str(row['callsign']) + ")"
        result += "</b><br><br>"

        if row[get_type_mappings()[DATA_TYPE_FM]]:
            result += "<b>Type:</b> " + "FM" + "<br>"
        elif row[get_type_mappings()[DATA_TYPE_DSTAR]]:
            result += "<b>Type:</b> " + "D-STAR" + "<br>"
        elif row[get_type_mappings()[DATA_TYPE_DMR]]:
            result += "<b>Type:</b> " + "DMR" + "<br>"

        if row['simplex']:
            result += "<b>Freq:</b> " + str(row['freq_rx']) + "<br>"
        else:
            result += "<b>Freq:</b> " + str(row['freq_rx']) + " (RX) / " + str(row['freq_tx']) + " (TX)<br>"

        if is_empty(row['ctcss_rx']) and is_empty(row['ctcss_tx']):
            result += "<b>CTCSS:</b> -<br>"
        else:
            result += "<b>CTCSS:</b> " + (str(row['ctcss_rx']) if (not is_empty(row['ctcss_rx'])) else "-") + " (RX) / " + (str(row['ctcss_tx']) if (not is_empty(row['ctcss_tx'])) else "-") + " (TX)<br>"

        result += "<b>Lat/Lon:</b> " + str(row['lat']) + " / " + str(row['long']) + "<br>"
        result += "<b>Locator:</b> " + row['locator'] + "<br>"
        result += "<b>Altitude:</b> " + (str(row['sea_level']) + "m" if (not is_empty(row['sea_level'])) else "-") + "<br>"
        result += "<b>Country:</b> " + str(row['country'])
        if not is_empty(row['country_code']):
            result += " (" + str(row['country_code']) + ")"

        return result

    def calc_color_key(self, row):
        data_type = ""
        if row[get_type_mappings()[DATA_TYPE_FM]]:
            data_type = DATA_TYPE_FM
        elif row[get_type_mappings()[DATA_TYPE_DSTAR]]:
            data_type = DATA_TYPE_DSTAR
        elif row[get_type_mappings()[DATA_TYPE_DMR]]:
            data_type = DATA_TYPE_DMR

        return str(f"{get_band_by_freq(row['freq_rx'])}_{data_type}")

    def get_attributions(self, data: DataFrame) -> [str]:
        attributions = list()
        if data is not None and not data.empty and 'source_id' in data.keys():
            for source_id in data['source_id'].unique():
                df = data.loc[data['source_id'] == source_id]
                if df is not None and not df.empty:
                    row = df.iloc[0]

                    attribution = ""
                    if not is_empty(row['source_provider']) and not is_empty(row['source_url']):
                        attribution = f'<a href="%s">%s</a>' % (str(row['source_url']).strip(), str(row['source_provider']).strip())
                    elif not is_empty(row['source_provider']):
                        attribution = f'%s' % str(row['source_url']).strip()

                    if not is_empty(attribution):
                        attributions.append(attribution)

        return attributions
