Group;Group Name;Mode;Name;Frequency;SKIP
{%- for row in data.itertuples(index=False) %}
{%- if row['fm'] %}
{{ row['icom_group_number_alphabetical'] | default('', True) | substring(0,1) }};{{ args.icom_group_name | default('', True) | substring(0,16) }};WFM;{{ row['name_formatted'] | replace_umlauts | remove_diacritic | substring(0,16) }};{{ "%.6f" | format(row['freq_rx']) | replace('.', ',') }};OFF
{%- endif -%}
{%- endfor %}