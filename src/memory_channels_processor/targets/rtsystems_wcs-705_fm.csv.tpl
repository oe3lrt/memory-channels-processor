Channel Number,Bank,Bank CH #,Receive Frequency,Transmit Frequency,Offset Frequency,Offset Direction,Operating Mode,Data Mode,Filter,Name,Tone Mode,CTCSS,Rx CTCSS,DCS,DCS Polarity,Scan Sel,Digital Squelch,Digital Code,Your Callsign,Rpt-1 CallSign,Rpt-2 CallSign,Comment
{%- set count = namespace(value=-1) %}
{%- for row in data.itertuples(index=False) %}
{%- if row['fm'] %}
{%- set count.value = count.value + 1 %}
{%- if row['band'] == "radio" %}
{{ '%01d' | format(count.value) }},{{ '%02d' | format(args.rtsystems_group_number | default(0, True)) }}: {{ args.rtsystems_group_name | default('', True) | substring(0,16) }},{{ '%02d' | format(count.value) }},{{ "%.6f" | format(row['freq_rx']) | replace(',', '.') }},,,,WFM,Off,1,{{ row['name_formatted'] | replace_umlauts | remove_diacritic | substring(0,16) }},{{ row['tone'] }},,,,,All,Off,0,,,,
{% else %}
{{ '%01d' | format(count.value) }},{{ '%02d' | format(args.rtsystems_group_number | default(0, True)) }}: {{ args.rtsystems_group_name | default('', True) | substring(0,16) }},{{ '%02d' | format(count.value) }},{{ "%.6f" | format(row['freq_rx']) | replace(',', '.') }},{{ "%.6f" | format(row['freq_tx']) | replace(',', '.') }},{{ row['freq_offset'] | default('', True) }},{{ row['offset_direction'] | default('', True) }},FM,Off,1,{{ row['name_formatted'] | replace_umlauts | remove_diacritic | substring(0,16) }},{{ row['tone'] }},{{ row['ctcss_tx'] | default('88,5', True) | format_ctcss | replace(',', '.') }},{{ row['ctcss_rx'] | default('88,5', True) | format_ctcss | replace(',', '.') }},023,Both N,All,Off,0,,,,{{ row['comment'] | default('', True) }}
{%- endif %}
{%- endif -%}
{%- endfor %}
