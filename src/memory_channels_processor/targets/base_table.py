import argparse
from typing import AnyStr

from pandas import DataFrame

from memory_channels_processor.processor import MemoryChannelsProcessor, is_empty
from memory_channels_processor.plugin_target import Target


class TableBaseTarget(Target):

    _skipload_ = True

    def __init__(self, filename=__file__):
        super().__init__()
        self.filename = filename

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def store_data(self, processor: MemoryChannelsProcessor, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame):
        pass

    def check_output_file_extension(self, output_file_extension: AnyStr) -> bool:
        pass

    def get_default_column_keys(self) -> list:
        result = [
            'callsign',
            'name',
            'band',
            'freq_tx',
            'freq_rx',
            'offset',
            'dup',
            'ctcss_tx',
            'ctcss_rx',
            'dmr',
            'dmr_id',
            'dstar',
            'fm',
            'simplex',
            'multimode',
            'landmark',
            'state',
            'country',
            'locator',
            'sea_level'
        ]
        return result

    def format_table(self, data: DataFrame) -> DataFrame:
        if data is None or data.empty:
            return None

        table_data = data.copy()

        table_data['freq_tx'] = table_data.apply(self.get_formatted_freq, args=('freq_tx',), axis=1)
        table_data['freq_rx'] = table_data.apply(self.get_formatted_freq, args=('freq_rx',), axis=1)

        return table_data

    def get_formatted_freq(self, row, col_name: str) -> str:
        if col_name not in row or is_empty(row[col_name]):
            result = ''
        else:
            result = "{:.3f}".format(row[col_name])

        return result

    def get_cell_mapped_value(self, value) -> str:
        if is_empty(value):
            result = ''
        elif isinstance(value, str):
            result = value
        elif isinstance(value, bool):
            result = '✔' if value else ''
        elif isinstance(value, int):
            result = str(value)
        elif isinstance(value, float):
            result = str(value)
        elif isinstance(value, list):
            result = ',\n'.join(value)
        else:
            result = str(value)

        return result
