Group No;Group Name;CH No;Name;SEL;Frequency;Dup;Offset;TS;Mode;SKIP;TONE;Repeater Tone;TSQL Frequency;DTCS Code;DTCS Polarity;DV SQL;DV CSQL Code;Your Call Sign;RPT1 Call Sign;RPT2 Call Sign
{%- set count = namespace(value=-1) %}
{%- for row in data.itertuples(index=False) %}
{%- if row['fm'] %}
{%- set count.value = count.value + 1 %}
{{ '%02d' | format(args.icom_group_number | default(0, True)) }};{{ args.icom_group_name | default('', True) | substring(0,16) }};{{ '%02d' | format(count.value) }};{{ row['name_formatted'] | replace_umlauts | remove_diacritic | substring(0,16) }};{{ row['sel'] | default('', True) }};{{ "%.6f" | format(row['freq_rx']) | replace('.', ',') }};{{ row['dup'] | default('', True) }};{{ "%.6f" | format(row['offset']) | default('', True) | replace('.', ',') }};12,5kHz;{{ row['mode'] }};{{ row['skip'] | default('OFF', True) }};{{ row['tone'] }};{{ row['ctcss_tx'] | default('88,5', True) | format_ctcss | replace('.', ',') }}Hz;{{ row['ctcss_rx'] | default('88,5', True) | format_ctcss | replace('.', ',') }}Hz;23;BOTH N;;;;{{ row['callsign'] | default('', True) }};
{%- elif row['dstar'] %}
{%- set count.value = count.value + 1 %}
{{ '%02d' | format(args.icom_group_number | default(0, True)) }};{{ args.icom_group_name | default('', True) | substring(0,16) }};{{ '%02d' | format(count.value) }};{{ row['name_formatted'] | replace_umlauts | remove_diacritic | substring(0,16) }};{{ row['sel'] | default('', True) }};{{ "%.6f" | format(row['freq_rx']) | replace('.', ',') }};{{ row['dup'] | default('', True) }};{{ "%.6f" | format(row['offset']) | default('', True) | replace('.', ',') }};12,5kHz;{{ row['mode'] }};{{ row['skip'] | default('OFF', True) }};;;;23;BOTH N;;;;{{ row['callsign'] | format_callsign_dstar(row['dstar_rpt1']) | default('', True) }};{{ row['callsign'] | format_callsign_dstar(row['dstar_rpt2']) | default('', True) }}
{%- endif -%}
{%- endfor %}