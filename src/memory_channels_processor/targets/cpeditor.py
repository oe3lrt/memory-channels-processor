import argparse

from pandas import DataFrame

from .base_jinja2 import Jinja2BaseTarget
from memory_channels_processor.processor import MemoryChannelsProcessor


class CPEditorTarget(Jinja2BaseTarget):

    _alias_ = 'cpeditor'

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)

    def prepare_data(self, data: DataFrame, processor: MemoryChannelsProcessor, args: argparse.Namespace, guessed_output_type: str = None) -> DataFrame:
        # Calculate
        data['zone_name'] = data.apply(self.get_zone_name, axis=1)
        data["zone_counter"] = data.groupby('zone_name').cumcount() + 1

        # Sort
        data_sorting_cpeditor = ['zone_name'] + args.data_sorting
        data = processor.sort_data(data, data_sorting_cpeditor, args.data_sorting_ascending)

        return data

    def get_zone_name(self, row) -> str:
        if row['fm']:
            result = "FM"
        elif row['dmr']:
            result = "DMR"
        else:
            result = ""

        return result

    def get_template_name(self, args: argparse.Namespace, guessed_output_type: str = None) -> str:
        return 'cpeditor_channels.csv.tpl'
