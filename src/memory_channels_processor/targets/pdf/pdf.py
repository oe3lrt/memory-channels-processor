import argparse
import os
from typing import AnyStr

from pandas import DataFrame

from fpdf import FPDF
from fpdf.fonts import FontFace, TextStyle
from fpdf.enums import TableCellFillMode

import memory_channels_processor
from memory_channels_processor.processor import (MemoryChannelsProcessor, arg_is_undefined, get_data_all_cols, is_empty, path_resolve)
from memory_channels_processor.targets.base_table import TableBaseTarget


class PdfTarget(TableBaseTarget):

    _alias_ = 'pdf'

    def setup_args(self, parser: argparse.ArgumentParser):
        if arg_is_undefined(parser, '--pdf-title'):
            parser.add_argument('--pdf-title', dest='pdf_title', action='store', help='The title printed in PDF outputs', metavar='<title>', type=str, default=None)

    def store_data(self, processor: MemoryChannelsProcessor, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame):
        if is_empty(output_file_path):
            print("Unable to write pdf file! Please provide a path where the pdf file should be stored.")
            exit(1)
        else:
            processor.print_verbose(f"Writing out to: {output_file_path}")

        column_keys = super().get_columns(args)
        if is_empty(column_keys):
            column_keys = self.get_default_column_keys()

            # Add extra column
            if args.consistency_check:
                column_keys.append('issues')

        table_data = self.format_table(data[column_keys])
        self.write_pdf_dataframe(output_file_path, args, table_data, data, column_keys)

    def check_output_file_extension(self, output_file_extension: AnyStr) -> bool:
        pass

    def write_pdf_dataframe(self, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame, data_source: DataFrame, column_keys: list) -> None:
        pdf = PDF()

        #pdf.set_font("Helvetica", size=10)
        pdf.set_font("dejavu-sans", size=8)

        # Add title, table and legend to the PDF
        title = None
        if not is_empty(args.pdf_title):
            title = args.pdf_title
        if args.output_header:
            self.add_title(pdf, title)
        self.add_table(pdf, data, column_keys)
        self.add_legend(pdf, data_source)

        # Generates the PDF and saves it to a file.
        pdf.output(output_file_path)

    def add_title(self, pdf: FPDF, title: str):
        """Adds the title to the PDF."""

        if not title:
            return

        pdf.set_font("dejavu-sans", style="B", size=12)
        pdf.cell(0, 10, title, ln=True, align="L")
        pdf.ln(5)  # Add a line break

    def add_table(self, pdf: FPDF, data: DataFrame, column_keys: list):
        """Adds the table from the DataFrame to the PDF."""

        if data is None or data.empty:
            return

        # Preparations
        # Columns
        column_weights = self.get_column_weigths()
        column_weights_sel = tuple(column_weights[key] for key in column_keys if key in column_weights)
        column_alignments = self.get_column_alignments()
        column_alignments_sel = tuple(column_alignments[key] for key in column_keys if key in column_alignments)
        column_names = self.get_column_printable_names()

        # Colors
        black = (0, 0, 0)
        white = (255, 255, 255)
        blue = (0, 0, 255)
        blue_light = (71, 145, 235)
        blue_lighter = (224, 235, 255)
        grey = (128, 128, 128)

        # Styles
        headings_style = FontFace(emphasis="B", color=white, fill_color=blue_light)
        table_line_color = blue_light
        table_row_fill_color = blue_lighter

        # Create the table
        pdf.set_font("dejavu-sans", style="", size=8)
        pdf.set_draw_color(table_line_color[0], table_line_color[1], table_line_color[2])
        pdf.set_line_width(0.3)
        with pdf.table(
            borders_layout="NO_HORIZONTAL_LINES",
            cell_fill_color=(table_row_fill_color[0], table_row_fill_color[1], table_row_fill_color[2]),
            cell_fill_mode=TableCellFillMode.ROWS,
            col_widths=column_weights_sel,
            headings_style=headings_style,
            line_height=5,
            text_align=column_alignments_sel,
            #width=160,
        ) as table:
            # Add headers (column names)
            header = data.columns.tolist()
            row = table.row()
            for column in header:
                row.cell(column_names[column])

            # Add rows (data)
            for index, data_row in data.iterrows():
                row = table.row()
                for item in data_row:
                    row.cell(self.get_cell_mapped_value(item))

    def add_legend(self, pdf: FPDF, data: DataFrame):
        """Adds a legend to the PDF."""

        html_tag_styles = {
            #"h1": FontFace(color="#948b8b", size_pt=32),
            #"h2": FontFace(color="#948b8b", size_pt=24),
            "a": FontFace(color="#4791eb"),
            "li": TextStyle(b_margin=2, l_margin=4),
        }

        attributions_html = self.get_attributions_html(data)
        html = (f"<p>Data was processed by <a href=\"{memory_channels_processor.__git_url__}\">memory-channels-processor</a> ({memory_channels_processor.__version__})</p>"
                "<p><b>Data providers:</b><br>"
                "<ul><li>" + "</li><li>".join(attributions_html) + "</li></ul>"
                "</p>"
                )

        pdf.ln(5)  # Add a line break
        pdf.set_font("dejavu-sans", style="B", size=8)
        pdf.write_html(html, tag_styles=html_tag_styles, font_family="dejavu-sans")
        pdf.ln(5)  # Add a line break

    def get_attributions_html(self, data: DataFrame) -> []:
        attributions = list()
        if data is not None and not data.empty and 'source_id' in data.keys():
            for source_id in data['source_id'].unique():
                df = data.loc[data['source_id'] == source_id]
                if df is not None and not df.empty:
                    row = df.iloc[0]

                    attribution = ""
                    if not is_empty(row['source_provider']) and not is_empty(row['source_url']):
                        attribution = f'<a href="%s">%s</a>: %s' % (str(row['source_url']).strip(), str(row['source_provider']).strip(), str(row['source_id']).strip())
                    elif not is_empty(row['source_provider']):
                        attribution = f'%s: %s' % (str(row['source_url']).strip(), str(row['source_id']).strip())

                    if not is_empty(attribution):
                        attributions.append(attribution)

        return attributions

    def get_column_printable_names(self) -> dict:
        result = dict(map(lambda item: (item[0], item[0]), get_data_all_cols().items()))

        result['callsign'] = 'Call'
        result['name'] = 'Name'
        result['band'] = 'Band'
        result['freq_tx'] = 'TX'
        result['freq_rx'] = 'RX'
        result['ctcss_tx'] = 'CTCSS TX'
        result['ctcss_rx'] = 'CTCSS RX'
        result['dmr'] = 'DMR'
        result['dmr_id'] = 'DMR ID'
        result['dstar'] = 'D*'
        result['dstar_rpt1'] = 'Rpt 1'
        result['dstar_rpt2'] = 'Rpt 2'
        result['fm'] = 'FM'
        result['landmark'] = 'Landmark'
        result['state'] = 'State'
        result['country'] = 'Country'
        result['country_code'] = 'Country Code'
        result['loc_exact'] = 'Exct.'
        result['lat'] = 'Lat.'
        result['long'] = 'Long.'
        result['locator'] = 'Locator'
        result['sea_level'] = 'Sea lev.'
        result['scan_group'] = 'Scan'
        result['source_id'] = 'Source'
        result['source_name'] = 'Source Name'
        result['source_provider'] = 'Provider'
        result['source_type'] = 'Type'
        result['source_license'] = 'License'
        result['source_url'] = 'Url'

        result['offset'] = 'Off.'
        result['dup'] = 'Dup'
        result['ctcss'] = 'CTCSS'
        result['simplex'] = 'Simplex'
        result['split'] = 'Splt'
        result['multimode'] = 'Multi'
        result['name_formatted'] = 'Name'
        result['distance'] = 'Dist.'
        result['heading'] = 'Head.'

        result['issues'] = 'Issues'

        return result

    def get_column_alignments(self) -> dict:
        result = dict.fromkeys(get_data_all_cols(), "L")

        result['ctcss_tx'] = 'R'
        result['ctcss_rx'] = 'R'
        result['dmr'] = 'C'
        result['dstar'] = 'C'
        result['fm'] = 'C'

        result['offset'] = 'C'
        result['dup'] = 'C'
        result['ctcss'] = 'R'
        result['simplex'] = 'C'
        result['split'] = 'C'
        result['multimode'] = 'C'

        return result

    def get_column_weigths(self) -> dict:
        result = dict.fromkeys(get_data_all_cols(), 2)

        result['callsign'] = 5
        result['name'] = 8
        result['band'] = 3
        result['freq_tx'] = 4
        result['freq_rx'] = 4
        result['ctcss_tx'] = 3
        result['ctcss_rx'] = 3
        result['dmr_id'] = 4
        result['landmark'] = 8
        result['state'] = 8
        result['country'] = 8
        result['lat'] = 6
        result['long'] = 6
        result['locator'] = 4
        result['sea_level'] = 3

        result['name_formatted'] = 8

        result['issues'] = 7

        return result

    def get_column_widths(self) -> dict:
        result = dict(map(lambda item: (item[0], 22 + (item[1] * 8)), self.get_column_weigths().items()))
        return result


class PDF(FPDF):
    def __init__(self, filename=__file__):
        super().__init__()

        self.filename = filename

        self.add_page(format="A4", orientation="landscape")

        folder_path = path_resolve(os.path.join(os.getcwd(), os.path.dirname(__file__)))

        # Different styles of the same font family.
        self.add_font("dejavu-sans", style="", fname=f"{folder_path}/DejaVuSans.ttf")
        self.add_font("dejavu-sans", style="B", fname=f"{folder_path}/DejaVuSans-Bold.ttf")
        self.add_font("dejavu-sans", style="I", fname=f"{folder_path}/DejaVuSans-Oblique.ttf")
        self.add_font("dejavu-sans", style="BI", fname=f"{folder_path}/DejaVuSans-BoldOblique.ttf")
        # Different type of the same font design.
        self.add_font("dejavu-sans-narrow", style="", fname=f"{folder_path}/DejaVuSansCondensed.ttf")
        self.add_font("dejavu-sans-narrow", style="B", fname=f"{folder_path}/DejaVuSansCondensed-Bold.ttf")
        self.add_font("dejavu-sans-narrow", style="I", fname=f"{folder_path}/DejaVuSansCondensed-Oblique.ttf")
        self.add_font("dejavu-sans-narrow", style="BI", fname=f"{folder_path}/DejaVuSansCondensed-BoldOblique.ttf")

    #def header(self):
    #    # Rendering logo:
    #    #self.image("../docs/fpdf2-logo.png", 10, 8, 33)
    #    # Setting font: helvetica bold 15
    #    self.set_font("dejavu-sans", style="B", size=12)
    #    # Moving cursor to the right:
    #    self.cell(80)
    #    # Printing title:
    #    self.cell(30, 10, "Title", border=1, align="C")
    #    # Performing a line break:
    #    self.ln(20)

    def footer(self):
        # Position cursor at 1.5 cm from bottom:
        self.set_y(-15)
        # Setting font: helvetica italic 8
        self.set_font("dejavu-sans-narrow", style="", size=8)
        # Printing page number:
        self.cell(0, 10, f"Page {self.page_no()}/{{nb}}", align="C")
