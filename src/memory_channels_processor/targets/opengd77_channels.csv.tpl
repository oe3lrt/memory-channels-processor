Channel Number;Channel Name;Channel Type;Rx Frequency;Tx Frequency;Bandwidth (kHz);Colour Code;Timeslot;Contact;TG List;DMR ID;TS1_TA_Tx;TS2_TA_Tx ID;RX Tone;TX Tone;Squelch;Power;Rx Only;Zone Skip;All Skip;TOT;VOX;No Beep;No Eco;APRS;Latitude;Longitude
{%- set count = namespace(value=0) %}
{%- for row in data.itertuples(index=True) %}
{%- if row['fm'] %}
{%- set count.value = count.value + 1 %}
{{ count.value }};{{ row['name_formatted'] | default('', True) }};Analogue;{{ row['freq_rx'] | replace('.', ',') }};{{ row['freq_tx'] | replace('.', ',') }};12,5;;;;;;;;{{ row['ctcss_rx'] | default('88,5', True) | format_ctcss | replace('.', ',') }};{{ row['ctcss_tx'] | default('88,5', True) | format_ctcss | replace('.', ',') }};Disabled;Master;No;No;No;0;Off;No;No;None;{{ row['lat'] | float | format_coordinate(5) | replace('.', ',') }};{{ row['long'] | format_coordinate(5) | replace('.', ',') }}
{%- elif row['dmr'] %}
{%- set count.value = count.value + 1 %}
{{ count.value }};{{ row['name_formatted'] | default('', True) }};Digital;{{ row['freq_rx'] | replace('.', ',') }};{{ row['freq_tx'] | replace('.', ',') }};;1;2;None;None;None;Off;Off;;;;Master;No;No;No;0;Off;No;No;None;{{ row['lat'] | float | format_coordinate(5) | replace('.', ',') }};{{ row['long'] | format_coordinate(5) | replace('.', ',') }}
{%- endif -%}
{%- endfor %}