import argparse
from typing import AnyStr

import pluginlib
from pandas import DataFrame

from memory_channels_processor.processor import PLUGIN_TYPE_TARGET, MemoryChannelsProcessor, is_empty, \
    strip_duplicates
from memory_channels_processor.plugin import Plugin


@pluginlib.Parent(PLUGIN_TYPE_TARGET)
class Target(Plugin):

    def __init__(self):
        super(Plugin, self).__init__()

    @pluginlib.abstractmethod
    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    @pluginlib.abstractmethod
    def store_data(self, processor: MemoryChannelsProcessor, output_file_path: AnyStr, args: argparse.Namespace, data: DataFrame):
        pass

    @pluginlib.abstractmethod
    def check_output_file_extension(self, output_file_extension: AnyStr) -> bool:
        return True

    def is_column_all_true(self, data: DataFrame, col: str) -> bool:
        return data[col].isin([True]).all()

    def is_column_any_true(self, data: DataFrame, col: str) -> bool:
        return data[col].isin([True]).any()

    def is_column_all_false(self, data: DataFrame, col: str) -> bool:
        return data[col].isin([False]).all()

    def is_column_any_false(self, data: DataFrame, col: str) -> bool:
        return data[col].isin([False]).any()

    def is_all_type_fm_only(self, data: DataFrame) -> bool:
        return self.is_column_all_true(data, 'fm') and self.is_column_all_false(data, 'dmr') and self.is_column_all_false(data, 'dstar')

    def is_all_type_dmr_only(self, data: DataFrame) -> bool:
        return self.is_column_all_false(data, 'fm') and self.is_column_all_true(data, 'dmr') and self.is_column_all_false(data, 'dstar')

    def is_all_type_dstar_only(self, data: DataFrame) -> bool:
        return self.is_column_all_false(data, 'fm') and self.is_column_all_false(data, 'dmr') and self.is_column_all_true(data, 'dstar')

    def get_columns(self, args: argparse.Namespace):
        if args is None or is_empty(args.output_columns):
            return None
        else:
            return strip_duplicates([c.strip() for c in args.output_columns.split(',') if not is_empty(c)])
