import argparse

import pluginlib
from pandas import DataFrame

from memory_channels_processor.processor import PLUGIN_TYPE_SOURCE, MemoryChannelsProcessor
from memory_channels_processor.plugin import Plugin


@pluginlib.Parent(PLUGIN_TYPE_SOURCE)
class Source(Plugin):

    def __init__(self):
        super(Plugin, self).__init__()

    @pluginlib.abstractmethod
    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    @pluginlib.abstractmethod
    def get_data(self, processor: MemoryChannelsProcessor, args: argparse.Namespace) -> DataFrame:
        pass
