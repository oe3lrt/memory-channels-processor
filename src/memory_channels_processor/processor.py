"""
Memory Channels Processor
=================================

"""

# (c) 2023-2025, OE3LRT <lukas@oe3lrt.at>

######################################################################

import argparse
import os
import re
import math

import sys
from sys import exit

import pluginlib

import warnings
from _csv import QUOTE_MINIMAL
from builtins import str
from typing import AnyStr, Any, Iterable

import numpy as np
import pandas as pd
from pandas import DataFrame
from pluginlib import PluginLoader
from pyhamtools.locator import calculate_distance, calculate_heading, latlong_to_locator, locator_to_latlong
from requests.sessions import Session
from requests_cache import CachedSession

from . import PY3_9_OR_LATER, __user_agent__

DATA_TYPE_FM = "fm"
DATA_TYPE_DSTAR = "d-star"
DATA_TYPE_DMR = "dmr"

ASCIIDOC_DATA_SOURCE_COLUMNS = "data_source_columns"
ASCIIDOC_DATA_TARGET_COLUMNS = "data_target_columns"
ASCIIDOC_NAME_FORMATS = "name_formats"
ASCIIDOC_JINJA2_FILTERS = "jinja2_filters"
ASCIIDOC_ARGUMENTS_ICOM = "arguments_icom"
ASCIIDOC_ARGUMENTS_KENWOOD = "arguments_kenwood"
ASCIIDOC_ARGUMENTS_RTSYSTEMS = "arguments_rtsystems"

NAME_FORMAT_CALLSIGN = "callsign"
NAME_FORMAT_CALLSIGN_TEMPLATE = "{{ callsign | default('', True) }}"

NAME_FORMAT_CALLSIGN_5_CHAR = "5-char"
NAME_FORMAT_CALLSIGN_5_CHAR_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) | ljust(4) }}{{ mode_short }}"

NAME_FORMAT_CALLSIGN_6_CHAR = "6-char"
NAME_FORMAT_CALLSIGN_6_CHAR_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) | ljust(4) }}-{{ mode_short }}"

NAME_FORMAT_CALLSIGN_7_CHAR = "7-char"
NAME_FORMAT_CALLSIGN_7_CHAR_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,5) | ljust(5) }}-{{ mode_short }}"

NAME_FORMAT_CALLSIGN_NAME = "callsign-name"
NAME_FORMAT_CALLSIGN_NAME_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) | ljust(4) }}{{ '-' if name is defined and name | length }}{{ name | default('', True) | replaceumlauts }}"

NAME_FORMAT_CALLSIGN_MODE_NAME = "callsign-mode-name"
NAME_FORMAT_CALLSIGN_MODE_NAME_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) | ljust(4) }}-{{ mode_short }}{{ '-' if name is defined and name | length }}{{ name | default('', True) | replaceumlauts }}"

NAME_FORMAT_NAME = "name"
NAME_FORMAT_NAME_TEMPLATE = "{{ name | default('', True) }}"

NAME_FORMAT_NAME_5_CHAR = "name-5-char"
NAME_FORMAT_NAME_5_CHAR_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,4) | ljust(4) }}{{ mode_short }}"

NAME_FORMAT_NAME_6_CHAR = "name-6-char"
NAME_FORMAT_NAME_6_CHAR_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,4) | ljust(4) }}-{{ mode_short }}"

NAME_FORMAT_NAME_7_CHAR = "name-7-char"
NAME_FORMAT_NAME_7_CHAR_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,5) | ljust(4) }}-{{ mode_short }}"

NAME_FORMAT_NAME_CALLSIGN = "name-callsign"
NAME_FORMAT_NAME_CALLSIGN_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,7) | ljust(7) }}{{ '-' if callsign is defined and callsign | length }}{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) }}"

NAME_FORMAT_NAME_MODE_CALLSIGN = "name-mode-callsign"
NAME_FORMAT_NAME_MODE_CALLSIGN_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,5) | ljust(5) }}-{{ mode_short }}{{ '-' if callsign is defined and callsign | length }}{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) }}"

NAME_FORMAT_CUSTOM = "custom"

SOURCE_TYPE_DYNAMIC = "dynamic"
SOURCE_TYPE_STATIC = "static"

SOURCE_LICENCE_BY = "Attribution"
SOURCE_LICENCE_NC = "Non Commercial"
SOURCE_LICENCE_CC_BY_40 = "CC BY 4.0"
SOURCE_LICENCE_PUBLIC_DOMAIN = "Public Domain"

SOURCE_LICENCE_CC_BY_40_URL = "https://creativecommons.org/licenses/by/4.0/"

#PLUGIN_LIB = "memory_channels_processor"
PLUGIN_MODULES = ["memory_channels_processor.sources", "memory_channels_processor.targets"]
PLUGIN_ENTRY_POINT = "memory_channels_processor_plugin"
PLUGIN_TYPE_SOURCE = "source"
PLUGIN_TYPE_TARGET = "target"


EQUAL = 'eq'
EQUAL_OP = '=='
NOT_EQUAL = 'neq'
NOT_EQUAL_OP = '!='
LIKE = 'lk'
LIKE_OP = '~='
GREATER = 'gt'
GREATER_OP = '>'
GREATEREQUAL = 'gt_eq'
GREATEREQUAL_OP = '>='
LOWER = 'lt'
LOWER_OP = '<'
LOWEREQUAL = 'lt_eq'
LOWEREQUAL_OP = '<='


class ArgParseFilterCustom(argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None):
        # Force attribute to type dict
        if not isinstance(getattr(namespace, self.dest), dict):
            setattr(namespace, self.dest, dict())

        content = getattr(namespace, self.dest)
        # Prepare dict
        if EQUAL not in content.keys():
            content[EQUAL] = dict()
        if NOT_EQUAL not in content.keys():
            content[NOT_EQUAL] = dict()
        if LIKE not in content.keys():
            content[LIKE] = dict()
        if GREATER not in content.keys():
            content[GREATER] = dict()
        if GREATEREQUAL not in content.keys():
            content[GREATEREQUAL] = dict()
        if LOWER not in content.keys():
            content[LOWER] = dict()
        if LOWEREQUAL not in content.keys():
            content[LOWEREQUAL] = dict()

        # Parse actual arguments
        for value in values:
            if EQUAL_OP in value:
                key, value = value.split(EQUAL_OP)
                content[EQUAL][key.strip()] = value.strip()
            elif NOT_EQUAL_OP in value:
                key, value = value.split(NOT_EQUAL_OP)
                content[NOT_EQUAL][key.strip()] = value.strip()
            elif LIKE_OP in value:
                key, value = value.split(LIKE_OP)
                content[LIKE][key.strip()] = value.strip()
            elif GREATEREQUAL_OP in value:
                key, value = value.split(GREATEREQUAL_OP)
                content[GREATEREQUAL][key.strip()] = value.strip()
            elif LOWEREQUAL_OP in value:
                key, value = value.split(LOWEREQUAL_OP)
                content[LOWEREQUAL][key.strip()] = value.strip()

            elif GREATER_OP in value:
                key, value = value.split(GREATER_OP)
                content[GREATER][key.strip()] = value.strip()
            elif LOWER_OP in value:
                key, value = value.split(LOWER_OP)
                content[LOWER][key.strip()] = value.strip()

            # For backward compatibility
            elif "=" in value:
                key, value = value.split('=')
                content[LIKE][key.strip()] = value.strip()


class MemoryChannelsProcessor(object):
    def __init__(self, args=None):

        """ Main execution path """

        self.root_path = path_resolve(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        self.filename = os.path.basename(__file__)

        # Load plugins
        ### REMOVE THIS ###
        import logging
        logger = logging.getLogger('pluginlib')
        logger.setLevel(logging.DEBUG)
        ### REMOVE THIS ###

        #self.plugin_loader = PluginLoader(library=PLUGIN_LIB, type_filter=[PLUGIN_TYPE_SOURCE, PLUGIN_TYPE_TARGET], entry_point=PLUGIN_ENTRY_POINT)
        self.plugin_loader = PluginLoader(modules=PLUGIN_MODULES, type_filter=[PLUGIN_TYPE_SOURCE, PLUGIN_TYPE_TARGET], entry_point=PLUGIN_ENTRY_POINT)

        try:
            self.plugins_source = self.plugin_loader.plugins[PLUGIN_TYPE_SOURCE]
            self.plugins_target = self.plugin_loader.plugins[PLUGIN_TYPE_TARGET]
        except pluginlib.PluginImportError as e:
            if e.friendly:
                exit(e.friendly)
            else:
                raise

        # Parse CLI arguments
        parser = self.args_parser_prepare()
        self.args = parser.parse_args(args)

        # Set defaults if needed - Workaround for "argparse: append action with default list adds to list instead of overriding" (see https://bugs.python.org/issue16399)
        if is_empty(self.args.filter_types):
            self.args.filter_types = []
        self.args.filter_types = flatten_array(self.args.filter_types)

        if is_empty(self.args.data_sorting):
            self.args.data_sorting = ['freq_rx', 'name', 'callsign']
        self.args.data_sorting = flatten_array(self.args.data_sorting)

        # Select the data sources
        self.data_sources_flat = strip_duplicates(flatten_array(self.args.data_sources))

        self.col_duplicate_check = (
            'freq_tx',
            'freq_rx',
            'callsign'
        )

        self.data_merge_keys_flat = strip_duplicates(flatten_array(self.args.data_merge_keys))

        # Set logging
        if self.args.verbose:
            import logging
            logging.basicConfig()
            logging.basicConfig(format="%(asctime)s %(name)s [%(levelname)s] %(message)s", datefmt="%H:%M:%S", level=logging.DEBUG)
            logger = logging.getLogger()

            logger = logging.getLogger('pluginlib')
            logger.setLevel(logging.DEBUG)

            logger = logging.getLogger('requests_cache')
            logger.setLevel(logging.DEBUG)
            logger.propagate = True

            logger = logging.getLogger('titlecase')
            logger.setLevel(logging.INFO)
            logger.propagate = True

            from http.client import HTTPConnection
            HTTPConnection.debuglevel = 1
            logger = logging.getLogger("urllib3")
            logger.setLevel(logging.DEBUG)
            logger.propagate = True

            logger = logging.getLogger('fontTools.subset')
            logger.setLevel(logging.WARN)
            logger.propagate = True

            logger = logging.getLogger('fontTools.ttLib.ttFont')
            logger.setLevel(logging.WARN)
            logger.propagate = True

            #logger = logging.getLogger('country_converter')
            #logger.setLevel(logging.DEBUG)
            #logger.propagate = True
        else:
            import logging
            logging.basicConfig()

            logger = logging.getLogger('fpdf.output')
            logger.setLevel(logging.ERROR)
            logger.propagate = True

            #logger = logging.getLogger('country_converter')
            #logger.setLevel(logging.CRITICAL)
            #logger.propagate = True


    def process(self):
        # ================
        # Process certain flags/args
        # ================

        # Print out version and exit
        if self.args.version:
            from memory_channels_processor import __version__
            print(__version__)
            exit(0)

        # Print out AsciiDoc and exit
        if not is_empty(self.args.asciidoc):
            self.print_asciidoc(self.args.asciidoc)
            exit(0)

        # Print data sources and exit
        if self.args.list_sources:
            for item in self.plugins_source.keys():
                print(item)
            exit(0)

        # Print data targets and exit
        if self.args.list_targets:
            for item in self.plugins_target.keys():
                print(item)
            exit(0)


        # ================
        # Check for data sources
        # ================
        if is_empty(self.data_sources_flat):
            print("Please specify at least one data source!")
            exit(1)

        # ================
        # Print debug information
        # ================
        self.print_verbose("============")
        self.print_verbose(f"Commandline: {' '.join(sys.argv[1:])}")

        # ================
        # Collect data from data sources
        # ================
        self.print_verbose("============")
        self.print_verbose(f"Initializing data sources: {self.data_sources_flat}")

        data_frames = []
        for data_source in self.data_sources_flat:
            self.print_verbose("============")
            self.print_verbose(f"Data source '{data_source}': Processing")

            if data_source in self.plugins_source.keys():
                data_source_instance = self.plugins_source[data_source]()
                data_source_dataframe = data_source_instance.get_data(self, self.args)
            else:
                data_source_dataframe = None

            if data_source_dataframe is None or data_source_dataframe.empty:
                self.print_verbose(f"Data source '{data_source}': Returned an empty dataset")
            else:
                self.print_verbose(f"Data source '{data_source}': Appending dataset")
                data_frames.append(data_source_dataframe)

        # ================
        # Append dataframes
        # ================
        if not is_empty(data_frames):
            with warnings.catch_warnings():
                # TODO: (Pandas 2.1.0+) Fix deprecated functionality "FutureWarning: The behavior of DataFrame concatenation with empty or all-NA entries is deprecated."
                warnings.filterwarnings("ignore", category=FutureWarning)
                data = pd.concat(data_frames, ignore_index=True)
        else:
            data = None

        # ================
        # Check for empty
        # ================
        if data is None or data.empty:
            print("The data source(s) returned an empty dataset!")
            exit(1)

        # Calculate - Offset + DUP + Callsign based infos
        data['offset'] = data.apply(self.calc_offset, axis=1)
        data['dup'] = data.apply(self.calc_dup, axis=1)
        data['ctcss'] = data.apply(self.calc_ctcss, axis=1)
        data['simplex'] = data.apply(self.calc_simplex, axis=1)
        data['split'] = data.apply(self.calc_split, axis=1)
        data['multimode'] = data.apply(self.calc_multimode, axis=1)
        data['name_formatted'] = data.apply(self.calc_name_formatted, axis=1)
        #data.apply(self.calc_callsign_infos, axis=1)

        # Calculate - Locator and distance
        data['lat'] = data.apply(self.calc_lat, axis=1)
        data['long'] = data.apply(self.calc_long, axis=1)
        data['locator'] = data.apply(self.calc_locator, axis=1)
        if not is_empty(self.args.locator):
            self.print_verbose(f"Calculate distance to locator: {self.args.locator}")
            data['distance'] = data.apply(lambda row: self.calc_distance(row, self.args.locator), axis=1)
            data['heading'] = data.apply(lambda row: self.calc_heading(row, self.args.locator), axis=1)
        else:
            self.print_verbose("No locator specified for distance calculation")
            data['distance'] = np.nan
            data['heading'] = np.nan

        # ================
        # Process items for consistency
        # ================
        if self.args.consistency_check:
            data['issue_count'], data['issues'] = data.apply(self.calc_consistency, axis=1, result_type='expand').T.values

        # ================
        # Merge with external CSV
        # ================
        data = self.merge_data(data, self.data_merge_keys_flat, self.args.data_merge_input_file)

        # ================
        # Ensure data types
        # ================
        self.ensure_data_types(data)

        # ================
        # Filter
        # ================
        self.filter_data(data, self.args, True)

        # ================
        # Sort
        # ================
        self.sort_data(data, self.args.data_sorting, self.args.data_sorting_ascending, True)

        # ================
        # Clean
        # ================
        self.print_verbose("Clean dataset from duplicates")
        data.drop_duplicates(subset=self.col_duplicate_check, inplace=True)

        # ================
        # Reset index
        # ================
        data.reset_index(drop=True, inplace=True)

        # ================
        # Output
        # ================
        output_format = self.args.output_format

        if is_empty(self.args.output_file):
            output_file_path = None
            output_file_extension = None
        else:
            output_file_path = path_resolve(self.args.output_file.name)
            output_file_extension = file_extension(output_file_path)

        if self.args.verbose:
            print("============")
            print("Dataset:")
            data.info()
            print(data)

        if output_format in self.plugins_target.keys():
            self.print_verbose("============")
            self.print_verbose(f"Data target '{output_format}': Processing")
            data_target = self.plugins_target[output_format]()

            if not is_empty(output_file_extension) and data_target.check_output_file_extension(output_file_extension) == False:
                print(f"Data target '{output_format}': The extension ('{output_file_extension}') of the specified output file doesn't match the expected extension!")
                exit(1)

            data_target.store_data(self, output_file_path, self.args, data)
            self.print_verbose("============")
        else:
            print("No output format was specified")
            exit(1)

        exit(0)

    def ensure_data_types(self, data: DataFrame) -> DataFrame:
        for key, value_type in get_data_all_cols().items():
            if str.lower(value_type) == "int64":
                with warnings.catch_warnings():
                    # TODO: (Pandas 2.2.0+) Fix deprecated functionality "FutureWarning: Downcasting object dtype arrays on .fillna, .ffill, .bfill is deprecated and will change in a future version. Call result.infer_objects(copy=False) instead. To opt-in to the future behavior, set `pd.set_option('future.no_silent_downcasting', True)`"
                    warnings.filterwarnings("ignore", category=FutureWarning)
                    data[key] = data[key].fillna(-1).astype(int).replace(-1, None)

        return data

    def filter_data(self, data: DataFrame, args: argparse.Namespace, inplace: bool = False) -> DataFrame:
        filter_bands_flat = strip_duplicates(flatten_array(args.filter_bands))
        filter_countries_flat = strip_duplicates(flatten_array(args.filter_countries))
        filter_types_flat = strip_duplicates(flatten_array(args.filter_types))

        # Filtering - Initialize
        data_query = []

        # Filtering - include only values defined in args
        if not is_empty(filter_bands_flat):
            self.print_verbose(f"Filter for bands: {filter_bands_flat}")
            data_query.append(f"band in {filter_bands_flat}")

        # Filtering - include only values defined in args
        if not is_empty(filter_countries_flat):
            self.print_verbose(f"Filter for countries: {filter_countries_flat}")
            data_query.append(f"country_code in {filter_countries_flat}")

        # Filtering - item in distance
        if not is_empty(args.filter_distance_max):
            self.print_verbose(f"Filter for distance max: {args.filter_distance_max} km")
            data_query.append(f"(distance.isnull() or distance <= {args.filter_distance_max})")

        # Filtering - FM, D-STAR, DMR
        data_query_or = []
        if DATA_TYPE_FM in filter_types_flat:
            self.print_verbose("Filter for FM repeaters")
            data_query_or.append("fm")

        if DATA_TYPE_DSTAR in filter_types_flat:
            self.print_verbose("Filter for D-STAR repeaters")
            data_query_or.append("dstar")

        if DATA_TYPE_DMR in filter_types_flat:
            self.print_verbose("Filter for DMR repeaters")
            data_query_or.append("dmr")

        if not is_empty(data_query_or):
            data_query_or_str = '(' + ') or ('.join(data_query_or) + ')'
            data_query.append(data_query_or_str)

        # Filtering - apply custom filters
        if not is_empty(args.filter_custom):
            self.print_verbose(f"Apply custom filters: {args.filter_custom}")

            columns = get_data_filterable_cols()

            # Filter '=='
            filter_custom = args.filter_custom[EQUAL]
            for key in filter_custom:
                if key in columns.keys():
                    if columns[key] == 'object':
                        filter_query = f"{key} == \"{filter_custom[key]}\""
                    elif columns[key] in ['bool', 'float64', 'int64']:
                        filter_query = f"{key} == {filter_custom[key]}"
                    else:
                        filter_query = None

                    if not is_empty(filter_query):
                        data_query.append(filter_query)
                else:
                    self.print_verbose(f"Filtering for key '%s' not possible. Key not in: %s" % (key, "', '".join(get_data_filterable_col_names())))

            # Filter '!='
            filter_custom = args.filter_custom[NOT_EQUAL]
            for key in filter_custom:
                if key in columns.keys():
                    if columns[key] == 'object':
                        filter_query = f"{key} != \"{filter_custom[key]}\""
                    elif columns[key] in ['bool', 'float64', 'int64']:
                        filter_query = f"{key} != {filter_custom[key]}"
                    else:
                        filter_query = None

                    if not is_empty(filter_query):
                        data_query.append(filter_query)
                else:
                    self.print_verbose(f"Filtering for key '%s' not possible. Key not in: %s" % (key, "', '".join(get_data_filterable_col_names())))

            # Filter '~='
            filter_custom = args.filter_custom[LIKE]
            for key in filter_custom:
                if key in columns.keys():
                    if columns[key] == 'object':
                        filter_query = f"{key}.str.contains(\"{filter_custom[key]}\")"
                    elif columns[key] in ['bool', 'float64', 'int64']:
                        filter_query = f"{key} == {filter_custom[key]}"
                    else:
                        filter_query = None

                    if not is_empty(filter_query):
                        data_query.append(filter_query)
                else:
                    self.print_verbose(f"Filtering for key '%s' not possible. Key not in: %s" % (key, "', '".join(get_data_filterable_col_names())))

            # Filter '>'
            filter_custom = args.filter_custom[GREATER]
            for key in filter_custom:
                if key in columns.keys():
                    if columns[key] in ['float64', 'int64']:
                        filter_query = f"{key} > {filter_custom[key]}"
                    else:
                        filter_query = None

                    if not is_empty(filter_query):
                        data_query.append(filter_query)
                else:
                    self.print_verbose(f"Filtering for key '%s' not possible. Key not in: %s" % (key, "', '".join(get_data_filterable_col_names())))

            # Filter '>='
            filter_custom = args.filter_custom[GREATEREQUAL]
            for key in filter_custom:
                if key in columns.keys():
                    if columns[key] in ['float64', 'int64']:
                        filter_query = f"{key} >= {filter_custom[key]}"
                    else:
                        filter_query = None

                    if not is_empty(filter_query):
                        data_query.append(filter_query)
                else:
                    self.print_verbose(f"Filtering for key '%s' not possible. Key not in: %s" % (key, "', '".join(get_data_filterable_col_names())))

            # Filter '<'
            filter_custom = args.filter_custom[LOWER]
            for key in filter_custom:
                if key in columns.keys():
                    if columns[key] in ['float64', 'int64']:
                        filter_query = f"{key} < {filter_custom[key]}"
                    else:
                        filter_query = None

                    if not is_empty(filter_query):
                        data_query.append(filter_query)
                else:
                    self.print_verbose(f"Filtering for key '%s' not possible. Key not in: %s" % (key, "', '".join(get_data_filterable_col_names())))

            # Filter '<='
            filter_custom = args.filter_custom[LOWEREQUAL]
            for key in filter_custom:
                if key in columns.keys():
                    if columns[key] in ['float64', 'int64']:
                        filter_query = f"{key} <= {filter_custom[key]}"
                    else:
                        filter_query = None

                    if not is_empty(filter_query):
                        data_query.append(filter_query)
                else:
                    self.print_verbose(f"Filtering for key '%s' not possible. Key not in: %s" % (key, "', '".join(get_data_filterable_col_names())))

        # Finally build the query
        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            self.print_verbose(f"Filter dataset: {data_query_str}")
            return data.query(data_query_str, inplace=inplace)

        return data

    def merge_data(self, data: DataFrame, data_merge_keys: list, data_merge_input_file: str = None) -> DataFrame:
        if not is_empty(data_merge_input_file):
            input_file = os.path.realpath(data_merge_input_file)
            if is_empty(data_merge_keys):
                keys = ['callsign']
            else:
                keys = data_merge_keys

            merged_suffix = '___merged_suffix'

            self.print_verbose(f"Reading file '{input_file}'")
            with open(file=input_file, mode='rb') as f:
                data_to_merge = pd.read_csv(filepath_or_buffer=f, index_col=None, header='infer', quoting=QUOTE_MINIMAL, encoding='utf-8')

                if self.args.verbose:
                    print("============")
                    print("Dataset to merge:")
                    data_to_merge.info()
                    print(data_to_merge)
                    print(keys)

                self.print_verbose(f"Merge dataset from file '%s' using keys '%s'" % (input_file, "', '".join(keys)))
                result = pd.merge(left=data, right=data_to_merge, how="left", on=keys, suffixes=(None, merged_suffix))

                columns_to_drop = set()
                for key in result.keys():
                    if key.endswith(merged_suffix):
                        key_original = key[:-len(merged_suffix)]
                        key_merged = key
                        result[key_original] = result[key_merged]
                        columns_to_drop.add(key_merged)

                if len(columns_to_drop) > 0:
                    result.drop(columns=list(columns_to_drop), inplace=True)

                if self.args.verbose:
                    print("============")
                    print("Merged dataset:")
                    result.info()
                    print(result)

                return result

        return data

    def sort_data(self, data: DataFrame, data_sorting: list, ascending: bool = True, inplace: bool = False) -> DataFrame:
        if not is_empty(data_sorting):
            self.print_verbose(f"Sort dataset {'ascending' if ascending else 'descending'} using {data_sorting}")
            return data.sort_values(by=data_sorting, ascending=ascending, inplace=inplace)

        return data

    def calc_offset(self, row):
        if not is_empty(row['freq_rx']) and not is_empty(row['freq_tx']):
            if get_band_by_freq(row['freq_rx']) == get_band_by_freq(row['freq_tx']):
                return round(row['freq_rx'] - row['freq_tx'], 2)
            else:
                return 0
        else:
            return 0

    def calc_dup(self, row):
        if row['offset'] is None:
            result = None
        elif row['offset'] > 0:
            result = '-'
        elif row['offset'] < 0:
            result = '+'
        else:
            result = None

        return result

    def calc_ctcss(self, row):
        if (row['ctcss_tx'] is not None and row['ctcss_tx'] > 0) or (row['ctcss_rx'] is not None and row['ctcss_rx'] > 0):
            return True
        else:
            return False

    def calc_simplex(self, row):
        if not is_empty(row['freq_rx']) and not is_empty(row['freq_tx']):
            if (row['freq_rx'] - row['freq_tx']) == 0:
                return True
            else:
                return False
        else:
            return False

    def calc_split(self, row):
        if not is_empty(row['freq_rx']) and not is_empty(row['freq_tx']):
            return get_band_by_freq(row['freq_rx']) != get_band_by_freq(row['freq_tx'])
        else:
            return False

    def calc_multimode(self, row):
        count = 0
        if row['fm']:
            count += 1
        if row['dstar']:
            count += 1
        if row['dmr']:
            count += 1

        return count > 1

    def calc_name_formatted(self, row):
        result = None

        # - Label by callsign ------
        # --------------------------

        callsign = row['callsign']
        name = row['name']

        if self.args.name_format == NAME_FORMAT_CALLSIGN:
            if not is_empty(callsign):
                result = callsign

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_5_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_5_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_6_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_6_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_7_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_7_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_NAME:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_NAME_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_MODE_NAME:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_MODE_NAME_TEMPLATE, row)

        # - Label by name ----------
        # --------------------------

        elif self.args.name_format == NAME_FORMAT_NAME:
            if not is_empty(name):
                result = name

        elif self.args.name_format == NAME_FORMAT_NAME_5_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_5_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_NAME_6_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_6_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_NAME_7_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_7_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_NAME_CALLSIGN:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_CALLSIGN_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_NAME_MODE_CALLSIGN:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_MODE_CALLSIGN_TEMPLATE, row)

        # - Label by custom scheme -
        # --------------------------

        elif self.args.name_format == NAME_FORMAT_CUSTOM:
            if not is_empty(self.args.name_format_custom):
                result = self.calc_name_formatted_custom(self.args.name_format_custom, row)

        # --------------------------

        return result

    def calc_name_formatted_custom(self, template: str, row):
        from jinja2 import (
            Environment,
            BaseLoader,
            StrictUndefined,
        )

        env = Environment(
            loader=BaseLoader,
            extensions=['jinja2.ext.debug'],
            keep_trailing_newline=True,
            trim_blocks=False,
            lstrip_blocks=False,
            optimized=False,
        )

        env.undefined = StrictUndefined

        # Register custom functions/functions
        custom_jinja_functions = get_jinja2_filters()

        # Add as functions
        env.globals.update(custom_jinja_functions)

        # Add as filters
        env.filters.update(custom_jinja_functions)

        # Prepare variables
        custom_jinja_vars = dict()
        custom_jinja_vars.update(row.fillna('').to_dict())
        custom_jinja_vars['mode_short'] = row['band'][:1] if row['fm'] else 'D'

        # Process the string
        result = env.from_string(template).render(custom_jinja_vars)

        # Strip all newlines
        result = result.replace('\n', ' ').replace('\r', '')

        return result

    def calc_lat(self, row):
        if not is_empty(row['locator']) and pd.isna(row['lat']):
            try:
                lat, long = locator_to_latlong(row['locator'])
                result = round(lat, 6)
            except:
                result = None
        else:
            result = row['lat']

        return result

    def calc_long(self, row):
        if not is_empty(row['locator']) and pd.isna(row['long']):
            try:
                lat, long = locator_to_latlong(row['locator'])
                result = round(long, 5)
            except:
                result = None
        else:
            result = row['long']

        return result

    def calc_locator(self, row):
        if row['locator'] is None:
            if row['lat'] is not None and row['long'] is not None:
                try:
                    result = latlong_to_locator(row['lat'], row['long'])
                except:
                    result = None
            else:
                result = None
        else:
            result = row['locator']

        return result

    def calc_distance(self, row, locator):
        if row['locator'] is not None:
            try:
                result = round(calculate_distance(locator, row['locator']), 2)
            except:
                result = np.nan
        else:
            result = np.nan

        return result

    def calc_heading(self, row, locator):
        if row['locator'] is not None:
            try:
                result = round(calculate_heading(locator, row['locator']), 2)
            except:
                result = np.nan
        else:
            result = np.nan

        return result

    def calc_consistency(self, row) -> (int, str):
        result = []

        # Check frequencies
        if not is_empty(row['band']):
            band = row['band']
            band_min, band_max = get_band_frequency_ranges()[band]

            if not band_min <= row['freq_tx'] <= band_max:
                result.append("'freq_tx' out of band")

            if not band_min <= row['freq_rx'] <= band_max:
                result.append("'freq_rx' out of band")

            if not is_empty(row['offset']) and row['freq_tx'] > row['freq_rx']:
                result.append("'freq_tx' is unusually above 'freq_rx'")

            if not row['simplex'] and not is_empty(row['offset']):
                offset = row['offset']
                band_offset = get_offet_per_band()[band]

                if not offset == band_offset:
                    result.append("unusual value for 'offset'")
        else:
            result.append("'band' not set")

        # Check CTCSS TX
        if not is_empty(row['ctcss_tx']):
            ctcss_tx = row['ctcss_tx']

            if not ctcss_tx in get_ctcss_values():
                result.append(f"Unusual value for 'ctcss_tx'")

        # Check CTCSS RX
        if not is_empty(row['ctcss_rx']):
            ctcss_rx = row['ctcss_rx']

            if not ctcss_rx in get_ctcss_values():
                result.append(f"Unusual value for 'ctcss_rx'")

        return len(result), ", ".join(result)

    def calc_callsign_infos(self, row):
        #TODO: Get additional infos via pyhamlib
        result = {
            'test1': 0,
            'test2': 1
        }

        return pd.Series(result)

    def args_parser_prepare(self) -> argparse.ArgumentParser:
        parser = argparse.ArgumentParser(description='Process memory channels', allow_abbrev=True, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        # Input / Output
        parser.add_argument('--source', dest='data_sources', action='append', choices=sorted(self.plugins_source.keys()), help='The data source', nargs='+', type=str.lower, default=[])
        parser.add_argument('--output-file', dest='output_file', action='store', help='The output file', metavar="<file>", type=argparse.FileType('wb'))
        parser.add_argument('--output-format', dest='output_format', action='store', choices=sorted(self.plugins_target.keys()), help='The output format', type=str.lower, default='csv')
        parser.add_argument('--output-columns', dest='output_columns', action='store', help='The column names to output. If multiple are defines separate with ",".', metavar="<Column 1,Column 2,...>", type=str.lower, default=[])
        parser.add_argument('--output-no-header', dest='output_header', action='store_false', help='Disable output of header row with column names', default=True)

        # Filtering
        parser.add_argument('--band', dest='filter_bands', action='append', choices=get_bands(), help='Filter for a specific band', nargs='*', type=str.lower)
        parser.add_argument('--country', dest='filter_countries', action='append', help='Filter for a specific country', nargs='*', metavar="<country - ISO 3166-1 alpha-3>", type=str.upper, default=[])
        parser.add_argument('--type', dest='filter_types', action='append', choices=get_types(), help='Filter for a specific type', nargs='*', type=str.lower, default=[])
        parser.add_argument('--distance-max', dest='filter_distance_max', action='store', help='Filter for a specific maximum distance (in kilometers)', metavar="<distance>", type=check_positive, default=None)
        parser.add_argument('--filter', dest='filter_custom', action=ArgParseFilterCustom, help='Filter for specific key value combinations (Available keys: ' + ", ".join(get_data_filterable_col_names()) + ')', metavar="<key>=<value>", nargs='*', type=str, default=dict())

        # Sorting
        parser.add_argument('--sort', dest='data_sorting', action='append', choices=sorted(get_data_sortable_col_names()), help='The key used for sorting', nargs='+', type=str.lower, default=[])
        parser.add_argument('--sort-descending', dest='data_sorting_ascending', action='store_false', help='The sorting direction', default=True)

        # Merging
        parser.add_argument('--merge-on', dest='data_merge_keys', action='append', choices=sorted(get_data_all_col_names()), help='The key(s) used for merging', nargs='+', type=str, default=[])
        parser.add_argument('--merge-file', dest='data_merge_input_file', action='store', help='The merge csv input file', metavar="<file>", type=str)

        # Additional arguments
        parser.add_argument('--locator', dest='locator', action='store', help='The locator used as reference for calculating distances', metavar="<locator>", type=str, default=None)

        # Formatting
        parser.add_argument('--name-format', '--chirp-name', dest='name_format', action='store', choices=[NAME_FORMAT_CALLSIGN, NAME_FORMAT_CALLSIGN_5_CHAR, NAME_FORMAT_CALLSIGN_6_CHAR, NAME_FORMAT_CALLSIGN_7_CHAR, NAME_FORMAT_NAME, NAME_FORMAT_NAME_5_CHAR, NAME_FORMAT_NAME_6_CHAR, NAME_FORMAT_NAME_7_CHAR, NAME_FORMAT_CALLSIGN_NAME, NAME_FORMAT_CALLSIGN_MODE_NAME, NAME_FORMAT_NAME_CALLSIGN, NAME_FORMAT_NAME_MODE_CALLSIGN, NAME_FORMAT_CUSTOM], help='The name format', type=str.lower, default=NAME_FORMAT_CALLSIGN)
        parser.add_argument('--name-format-custom', dest='name_format_custom', action='store', help='The custom name format', metavar="<name format template>", type=str, default='')

        # Add additional arguments from data sources and targets
        for item in self.plugins_source.keys():
            data_source_instance = self.plugins_source[item]()
            data_source_instance.setup_args(parser)

        for item in self.plugins_target.keys():
            data_target_instance = self.plugins_target[item]()
            data_target_instance.setup_args(parser)

        # Flags
        parser.add_argument('--consistency-check', dest='consistency_check', action='store_true', help='Check consistency for all entries and print the result into the field \'issues\'', default=False)
        parser.add_argument('--import-formatted-names', dest='formatted_names_force_on_import', action='store_true', help='Force usage of already formatted names from files', default=False)
        parser.add_argument('--list-sources', dest='list_sources', action='store_true', help='List all available data sources', default=False)
        parser.add_argument('--list-targets', dest='list_targets', action='store_true', help='List all available data targets', default=False)
        parser.add_argument('--no-cache', dest='request_cache_enabled', action='store_false', help='Disable caching of web requests', default=True)
        parser.add_argument('--offline', dest='offline', action='store_true', help='Enable offline mode', default=False)
        parser.add_argument('--verbose', dest='verbose', action='store_true', help='Enable verbosity', default=False)
        parser.add_argument('--version', dest='version', action='store_true', help='Show the version', default=False)

        # Documentation (hidden)
        parser.add_argument('--asciidoc', dest='asciidoc', action='store', choices=[ASCIIDOC_DATA_SOURCE_COLUMNS, ASCIIDOC_DATA_TARGET_COLUMNS, ASCIIDOC_NAME_FORMATS, ASCIIDOC_JINJA2_FILTERS, ASCIIDOC_ARGUMENTS_ICOM, ASCIIDOC_ARGUMENTS_KENWOOD, ASCIIDOC_ARGUMENTS_RTSYSTEMS], help=argparse.SUPPRESS, type=str.lower, default=None)

        return parser

    def flatten_data(self, data: DataFrame, args: argparse.Namespace) -> DataFrame:
        if data is None:
            return None

        if data.empty:
            return data

        # Reset index
        df = data.reset_index(drop=True, inplace=False)

        # Expand data to only have one mode per row
        result_counter = 0

        # Copy structure of original data
        result = df.drop(df.index)

        # Convert data types
        result = result.astype(get_data_all_cols())  # , errors="ignore")

        # Process data
        with warnings.catch_warnings():
            # TODO: (Pandas 2.1.0+) Fix deprecated functionality "FutureWarning: The behavior of DataFrame concatenation with empty or all-NA entries is deprecated."
            warnings.filterwarnings("ignore", category=FutureWarning)
            for i, row in df.iterrows():
                if row['multimode']:
                    if row['fm']:
                        row_new = df.loc[i].copy(deep=True)
                        row_new['fm'] = True
                        row_new['dstar'] = False
                        row_new['dmr'] = False
                        row_new['multimode'] = False
                        result.loc[result_counter] = row_new
                        result_counter += 1
                    if row['dstar']:
                        row_new = df.loc[i].copy(deep=True)
                        row_new['fm'] = False
                        row_new['dstar'] = True
                        row_new['dmr'] = False
                        row_new['multimode'] = False
                        result.loc[result_counter] = row_new
                        result_counter += 1
                    if row['dmr']:
                        row_new = df.loc[i].copy(deep=True)
                        row_new['fm'] = False
                        row_new['dstar'] = False
                        row_new['dmr'] = True
                        row_new['multimode'] = False
                        result.loc[result_counter] = row_new
                        result_counter += 1
                else:
                    result.loc[result_counter] = df.iloc[i].copy(deep=True)
                    result_counter += 1

        # Recalculate values
        if result.size > 0:
            result['name_formatted'] = result.apply(self.calc_name_formatted, axis=1)

        # Replace "NaN" with "None" to allow proper parsing/filtering with jinja2 default filter
        for column in result.columns:
            self.print_verbose(f"Column: {column}")
            result[column] = result[column].replace(np.nan, None)

        # Re-Apply filters
        result = self.filter_data(result, args)

        # Reset index
        result.reset_index(drop=True, inplace=True)

        return result

    def print_verbose(self, output: AnyStr):
        """ Print out only if verbose is set """

        if self.args.verbose:
            print(output)

    def requests_session(self) -> Session:
        if self.args.request_cache_enabled:
            session = CachedSession("memory_channels_processor_cache", expire_after=10 * 60, backend='sqlite', serializer='json')
        else:
            session = Session()

        session.headers.update({"User-Agent": __user_agent__})

        return session

    def print_asciidoc(self, asciidoc_type: str):
        """ Print out AsciiDoc formatted content for documentation """

        if asciidoc_type == ASCIIDOC_DATA_SOURCE_COLUMNS:
            print("[%header,%autowidth,stripes=even,format=dsv,separator=;]")
            print("|===")
            print("Key;Type")
            columns = get_data_source_cols()
            for key in sorted(columns.keys()):
                print("# tag::" + key + "[]")
                print(key + ";" + columns[key])
                print("# end::" + key + "[]")
            print("|===")

        elif asciidoc_type == ASCIIDOC_DATA_TARGET_COLUMNS:
            print("[%header,%autowidth,stripes=even,format=dsv,separator=;]")
            print("|===")
            print("Key;Type")
            columns = get_data_sortable_cols()
            for key in sorted(columns.keys()):
                print("# tag::" + key + "[]")
                print(key + ";" + columns[key])
                print("# end::" + key + "[]")
            print("|===")

        elif asciidoc_type == ASCIIDOC_NAME_FORMATS:
            var_names = [
                "NAME_FORMAT_CALLSIGN",
                "NAME_FORMAT_CALLSIGN_5_CHAR",
                "NAME_FORMAT_CALLSIGN_6_CHAR",
                "NAME_FORMAT_CALLSIGN_7_CHAR",
                "NAME_FORMAT_CALLSIGN_NAME",
                "NAME_FORMAT_CALLSIGN_MODE_NAME",
                "NAME_FORMAT_NAME",
                "NAME_FORMAT_NAME_5_CHAR",
                "NAME_FORMAT_NAME_6_CHAR",
                "NAME_FORMAT_NAME_7_CHAR",
                "NAME_FORMAT_NAME_CALLSIGN",
                "NAME_FORMAT_NAME_MODE_CALLSIGN"
            ]

            print("[%header,%autowidth,stripes=even,format=dsv,separator=;,cols=\"1,3\"]")
            print("|===")
            print("Format;Template")
            for var_name in sorted(var_names):
                var_value = globals()[var_name]
                var_template_value = globals()[var_name + "_TEMPLATE"].replace(";", "\\;")

                print("# tag::" + var_value + "[]")
                print(var_value + ";" + var_template_value + "")
                print("# end::" + var_value + "[]")
            print("|===")

        elif asciidoc_type == ASCIIDOC_JINJA2_FILTERS:
            from .targets.base_jinja2 import Jinja2BaseTarget
            jinja2_base_target = Jinja2BaseTarget()
            jinja2_custom = jinja2_base_target.custom_jinja2_filters()

            print("[%header,%autowidth,stripes=even,format=dsv,separator=;,cols=\"1,3,3\"]")
            print("|===")
            print("Name;Signature;Description")

            for filter_name in sorted(jinja2_custom.keys()):
                filter_function = jinja2_custom.get(filter_name)
                from inspect import signature
                try:
                    filter_params = str(signature(filter_function))
                except ValueError:
                    filter_params = "()"

                filter_signature = f"{filter_name}{filter_params}"

                from inspect import getdoc
                filter_description = "" if is_empty(getdoc(filter_function)) else getdoc(filter_function).splitlines()[0]

                print("# tag::" + filter_name + "[]")
                print(filter_name + ";" + filter_signature + ";" + filter_description)
                print("# end::" + filter_name + "[]")

            print("|===")

        elif asciidoc_type == ASCIIDOC_ARGUMENTS_ICOM:
            from memory_channels_processor.targets.icom import IcomBaseTarget
            icom_base_target = IcomBaseTarget()
            dummy_parser = argparse.ArgumentParser(allow_abbrev=False)
            icom_base_target.setup_args(dummy_parser)

            print("[%header,%autowidth,stripes=even,format=dsv,separator=;,cols=\"3,3,2,2,5\"]")
            print("|===")
            print("Option;Default;Choices;Required;Description")

            from argparse import _HelpAction
            for arg in dummy_parser.__dict__['_actions']:
                if not isinstance(arg, _HelpAction) and not (str(arg.dest).startswith("jinja2_")):
                    print(f"{', '.join(arg.option_strings)};{arg.default};{'' if arg.choices is None else ', '.join(arg.choices)};{'X' if arg.required else '-'};{arg.help}")

            print("|===")

        elif asciidoc_type == ASCIIDOC_ARGUMENTS_KENWOOD:
            from memory_channels_processor.targets.kenwood import KenwoodTHD74Target
            kenwood_target = KenwoodTHD74Target()
            dummy_parser = argparse.ArgumentParser(allow_abbrev=False)
            kenwood_target.setup_args(dummy_parser)

            print("[%header,%autowidth,stripes=even,format=dsv,separator=;,cols=\"3,3,2,2,5\"]")
            print("|===")
            print("Option;Default;Choices;Required;Description")

            from argparse import _HelpAction
            for arg in dummy_parser.__dict__['_actions']:
                if not isinstance(arg, _HelpAction) and not (str(arg.dest).startswith("jinja2_")):
                    print(f"{', '.join(arg.option_strings)};{arg.default};{'' if arg.choices is None else ', '.join(arg.choices)};{'X' if arg.required else '-'};{arg.help}")

            print("|===")

        elif asciidoc_type == ASCIIDOC_ARGUMENTS_RTSYSTEMS:
            from memory_channels_processor.targets.rtsystems import RTSystemsWCS705Target
            rtsystems_target = RTSystemsWCS705Target()
            dummy_parser = argparse.ArgumentParser(allow_abbrev=False)
            rtsystems_target.setup_args(dummy_parser)

            print("[%header,%autowidth,stripes=even,format=dsv,separator=;,cols=\"3,3,2,2,5\"]")
            print("|===")
            print("Option;Default;Choices;Required;Description")

            from argparse import _HelpAction
            for arg in dummy_parser.__dict__['_actions']:
                if not isinstance(arg, _HelpAction) and not (str(arg.dest).startswith("jinja2_")):
                    print(f"{', '.join(arg.option_strings)};{arg.default};{'' if arg.choices is None else ', '.join(arg.choices)};{'X' if arg.required else '-'};{arg.help}")

            print("|===")


def path_resolve(path: AnyStr) -> AnyStr:
    """ Resolves the given path """

    return os.path.realpath(os.path.normpath(os.path.abspath(path)))


def path_relativize(path: str, start: str) -> str:
    """ Relativize the given path """

    return os.path.relpath(path, start)


def file_extension(path: AnyStr) -> AnyStr:
    """ Resolves the file extension of the given path """

    name, extension = os.path.splitext(path)
    return None if is_empty(extension) else extension


def is_empty(item: Any) -> bool:
    """ Check if the item is empty """

    if item is None:
        return True

    if isinstance(item, list) and len(item) == 0:
        return True
    elif isinstance(item, dict) and len(item) == 0:
        return True
    elif isinstance(item, int) and pd.isna(item):
        return True
    elif isinstance(item, float) and pd.isna(item):
        return True
    elif item == '':
        return True

    return False


def push_array_unique(my_dict: dict, key: str, element: Any) -> None:
    """ Push an element onto an array that may not have been defined in the dict only if the element isn't
    already in the array"""

    if key in my_dict:
        if element not in my_dict[key]:
            my_dict[key].append(element)
    else:
        my_dict[key] = [element]


def push_set(my_dict: dict, key: str, element: Any) -> None:
    """ Push an element onto an array that may not have been defined in the dict only if the element isn't
    already in the array"""

    if key in my_dict:
        my_dict[key].add(element)
    else:
        my_dict[key] = {element}


def is_collection(some_collection: Any) -> bool:
    return isinstance(some_collection, Iterable)


def strip_duplicates(items: Iterable) -> []:
    return [i for n, i in enumerate(items) if i not in items[:n]]


def get_data_source_cols() -> dict:
    cols = {
        'callsign': 'object',
        'name': 'object',
        'band': 'object',
        'freq_tx': 'float64',
        'freq_rx': 'float64',
        'ctcss_tx': 'float64',
        'ctcss_rx': 'float64',
        'dmr': 'bool',
        'dmr_id': 'int64',
        'dstar': 'bool',
        'dstar_rpt1': 'object',
        'dstar_rpt2': 'object',
        'fm': 'bool',
        'landmark': 'object',
        'state': 'object',
        'country': 'object',
        'country_code': 'object',
        'loc_exact': 'bool',
        'lat': 'float64',
        'long': 'float64',
        'locator': 'object',
        'sea_level': 'int64',
        'scan_group': 'int64',
        'source_id': 'object',
        'source_name': 'object',
        'source_provider': 'object',
        'source_type': 'object',
        'source_license': 'object',
        'source_url': 'object'
    }
    return cols


def get_data_source_col_names() -> []:
    return get_data_source_cols().keys()


def get_data_additional_cols() -> dict:
    cols = {
        'offset': 'float64',
        'dup': 'object',
        'ctcss': 'bool',
        'simplex': 'bool',
        'split': 'bool',
        'multimode': 'bool',
        'name_formatted': 'object',
        'distance': 'float64',
        'heading': 'int64'
    }
    return cols


def get_data_additional_col_names() -> []:
    return get_data_additional_cols().keys()


def get_data_optional_cols() -> dict:
    cols = {
        'issue_count': 'int64',
        'issues': 'object'
    }
    return cols


def get_data_optional_col_names() -> []:
    return get_data_optional_cols().keys()


def get_data_all_cols() -> dict:
    return {**get_data_source_cols(), **get_data_additional_cols()}


def get_data_all_col_names() -> []:
    return get_data_all_cols().keys()


def get_data_filterable_cols() -> dict:
    cols = {**get_data_all_cols(), **get_data_optional_cols()}
    cols = {key: val for key, val in cols.items() if val in ['object', 'bool', 'float64', 'int64']}
    return cols


def get_data_filterable_col_names() -> []:
    return get_data_filterable_cols().keys()


def get_data_sortable_cols() -> dict:
    return get_data_all_cols()


def get_data_sortable_col_names() -> []:
    return get_data_source_col_names() | get_data_additional_col_names()


def get_band_frequency_ranges() -> dict:
    frequency_ranges = {
        "13cm": [2320, 2450],
        "23cm": [1240, 1300],
        "pmr": [446.0, 446.2],
        "70cm": [430, 440],
        "2m": [144, 146],
        "radio": [87, 108],
        "6m": [50.4, 52],
        "10m": [28, 29.7],
        "cb": [26.96, 27.41],
        "12m": [24.89, 24.99],
        "15m": [21, 21.45],
        "20m": [14, 14.35],
        "40m": [7, 7.2],
        "80m": [3.5, 3.8]
    }
    return frequency_ranges


def get_offet_per_band() -> dict:
    offet_per_band = {
        "13cm": 0,
        "23cm": 28.0,
        "pmr": 0,
        "70cm": 7.6,
        "2m": 0.6,
        "radio": 0,
        "6m": 0.6,
        "10m": 0,
        "cb": 0,
        "12m": 0,
        "15m": 0,
        "20m": 0,
        "40m": 0,
        "80m": 0
    }
    return offet_per_band


def get_ctcss_values() -> list:
    # See https://de.wikipedia.org/wiki/CTCSS
    ctcss_values = [
        67.0,
        69.3,
        71.9,
        74.4,
        77.0,
        79.7,
        82.5,
        85.4,
        88.5,
        91.5,
        94.8,
        97.4,
        100.0,
        103.5,
        107.2,
        110.9,
        114.8,
        118.8,
        123.0,
        127.3,
        131.8,
        136.5,
        141.3,
        146.2,
        151.4,
        156.7,
        159.8,
        162.2,
        165.5,
        167.9,
        171.3,
        173.8,
        177.3,
        179.9,
        183.5,
        186.2,
        189.9,
        192.8,
        196.6,
        199.5,
        203.5,
        206.5,
        210.7,
        218.1,
        225.7,
        229.1,
        233.6,
        241.8,
        250.3,
        254.1
    ]
    return ctcss_values


def get_bands() -> []:
    return list(get_band_frequency_ranges().keys())


def get_band_by_freq(frequency: float) -> Any:
    if frequency is not None:
        keys = [k for k, v in get_band_frequency_ranges().items() if v[0] <= frequency <= v[1]]
        if not is_empty(keys):
            return keys[0]

    return None


def get_row_count(data: DataFrame) -> int:
    if data is not None:
        return len(data.index)

    return 0


def get_type_mappings() -> []:
    return {
        DATA_TYPE_FM: 'fm',
        DATA_TYPE_DSTAR: 'dstar',
        DATA_TYPE_DMR: 'dmr'
    }


def get_types() -> []:
    return list(get_type_mappings().keys())


def arg_is_defined(parser: argparse.ArgumentParser, option: str) -> bool:
    return any(option in x.option_strings for x in parser.__dict__['_actions'])


def arg_is_undefined(parser: argparse.ArgumentParser, option: str) -> bool:
    return not arg_is_defined(parser, option)


def flatten_array(items: Iterable) -> []:
    result = []
    if isinstance(items, str) and not is_empty(items):
        result.append(items)
    elif not is_empty(items):
        for i in items:
            if isinstance(i, str) and not is_empty(i):
                result.append(i)
            elif is_collection(i):
                for j in flatten_array(i):
                    if not is_empty(j):
                        result.append(j)
            else:
                print(type(i))

    return result


def round_float(x: float, precision: int = None) -> float:
    if math.isnan(x):
        return math.nan
    else:
        if precision is not None:
            return round(x, precision)
        else:
            return x


def force_text(data):
    if isinstance(data, str):
        return data
    if isinstance(data, bytes):
        return data.decode('utf8')
    return data


def get_jinja2_filters() -> dict:
    # Return custom filters/functions
    custom_jinja_filters = dict()
    custom_jinja_filters['ljust'] = lambda content, width, fillchar = ' ': str(content).ljust(width, fillchar)
    custom_jinja_filters['lstrip'] = lambda content, char = ' ': str(content).lstrip(char)
    custom_jinja_filters['replaceumlauts'] = filter_replace_umlauts
    custom_jinja_filters['replace_umlauts'] = filter_replace_umlauts
    custom_jinja_filters['removediacritic'] = filter_remove_diacritic
    custom_jinja_filters['remove_diacritic'] = filter_remove_diacritic
    custom_jinja_filters['removespaces'] = filter_remove_spaces
    custom_jinja_filters['remove_spaces'] = filter_remove_spaces
    custom_jinja_filters['removeprefix'] = filter_remove_prefix
    custom_jinja_filters['remove_prefix'] = filter_remove_prefix
    custom_jinja_filters['removesuffix'] = filter_remove_suffix
    custom_jinja_filters['remove_suffix'] = filter_remove_suffix
    custom_jinja_filters['rjust'] = lambda content, width, fillchar = ' ': str(content).rjust(width, fillchar)
    custom_jinja_filters['rstrip'] = lambda content, char = ' ': str(content).rstrip(char)
    custom_jinja_filters['safe'] = filter_safe
    custom_jinja_filters['strip'] = lambda content, char = ' ': str(content).strip(char)
    custom_jinja_filters['swapcase'] = lambda content: str(content).swapcase()
    custom_jinja_filters['substring'] = lambda content, start, end = None: str(content)[start:end]
    return custom_jinja_filters


def get_safe(content: Any) -> str:
    """
    Converts 'bad' characters in a string to underscores
    """
    if is_empty(content):
        return ''

    result = re.sub(r"[^A-Za-z0-9]", "_", str(content))

    # Remove duplicate underscores
    result = result.replace("__", "_").replace("__", "_")

    # Remove leading and trailing underscores
    result = result.strip("_")

    return result


def filter_safe(content: str) -> str:
    """
    Converts 'bad' characters in a string to underscores
    :param content: The string content
    :return: The content with 'bad' characters replaced
    """
    return get_safe(content)


def remove_spaces(content: str) -> str:
    """
    Remove spaces from given text
    :param content: The string content
    :return: The content with spaces removed
    """
    if is_empty(content):
        return ''

    return content.replace(' ', '')


def filter_remove_spaces(content: str) -> str:
    """
    Remove spaces from given text
    :param content: The string content
    :return: The content with spaces removed
    """
    return remove_spaces(content)


def remove_diacritic(content: str) -> str:
    """
    Remove accents (diacritics) from text
    :param content: The string content
    :return: The content with accents (diacritics) removed
    """
    from unidecode import unidecode

    if is_empty(content):
        return ""

    return unidecode(content)


def filter_remove_diacritic(content: str) -> str:
    """
    Remove accents (diacritics) from text
    :param content: The string content
    :return: The content with accents (diacritics) removed
    """
    return remove_diacritic(content)


def replace_umlauts(content: str) -> str:
    """
    Replace special German umlauts (vowel mutations) from text
        ä -> ae, Ä -> Ae...
        ü -> ue, Ü -> Ue...
        ö -> oe, Ö -> Oe...
        ß -> ss
    :param content: The string content
    :return: The content with German umlauts replaced
    """
    if is_empty(content):
        return ''

    vowel_char_map = {
        ord('ä'): 'ae', ord('ü'): 'ue', ord('ö'): 'oe', ord('ß'): 'ss',
        ord('Ä'): 'Ae', ord('Ü'): 'Ue', ord('Ö'): 'Oe'
    }
    return content.translate(vowel_char_map)


def filter_replace_umlauts(content: str) -> str:
    """
    Replace special German umlauts (vowel mutations) from text
        ä -> ae, Ä -> Ae...
        ü -> ue, Ü -> Ue...
        ö -> oe, Ö -> Oe...
        ß -> ss
    :param content: The string content
    :return: The content with German umlauts replaced
    """
    return replace_umlauts(content)


def filter_remove_prefix(content: str, prefix: str) -> str:
    """
    Removes a given prefix from text
    :param content: The string content
    :param prefix: The string prefix
    :return: The content with prefix removed
    """
    if PY3_9_OR_LATER:
        return content.removeprefix(prefix) if content else content
    else:
        if content and content.startswith(prefix):
            return content[len(prefix):]
        else:
            return content


def filter_remove_suffix(content: str, suffix: str) -> str:
    """
    Removes a given suffix from text
    :param content: The string content
    :param suffix: The string suffix
    :return: The content with suffix removed
    """
    if PY3_9_OR_LATER:
        return content.removesuffix(suffix) if content else content
    else:
        if content and content.endswith(suffix):
            return content[:-len(suffix)]
        else:
            return content


def check_positive(value):
    int_value = int(value)
    if int_value <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return int_value


def check_negative(value):
    int_value = int(value)
    if int_value >= 0:
        raise argparse.ArgumentTypeError("%s is an invalid negative int value" % value)
    return int_value