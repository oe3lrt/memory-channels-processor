"""
memory-channels-processor
==========

License: Apache, see LICENSE for more details.
"""

import sys
import importlib.metadata

__author__ = "OE3LRT"

__version__ = importlib.metadata.version("memory-channels-processor")
__git_url__ = "https://gitlab.com/oe3lrt/memory-channels-processor"
__user_agent__ = f"memory-channels-processor (v{__version__}; {__git_url__})"

PY3_OR_LATER = sys.version_info[0] >= 3
PY3_8_OR_LATER = sys.version_info[0] >= 3 and sys.version_info[1] >= 8
PY3_9_OR_LATER = sys.version_info[0] >= 3 and sys.version_info[1] >= 9

IN_CLI = False


def run_from_cli():
    global IN_CLI
    IN_CLI = True

    from .processor import MemoryChannelsProcessor
    proc = MemoryChannelsProcessor(args=None)
    proc.process()


def run_test(args=None):
    from .processor import MemoryChannelsProcessor
    proc = MemoryChannelsProcessor(args)
    proc.process()


__all__ = ['run_from_cli', 'run_test', '__version__', '__git_url__', '__user_agent__', 'PY3_OR_LATER', 'PY3_8_OR_LATER', 'PY3_9_OR_LATER', 'IN_CLI']
