import pytest

from memory_channels_processor import run_test, __version__


def setup_function(function):
    print(f"\nSetting up function '%s'" % function.__name__)


def teardown_function(function):
    print(f"\nTear down function '%s'" % function.__name__)


@pytest.mark.parametrize("args", ("--version", ))
def test_version(capsys, args):
    try:
        run_test([args])
    except SystemExit:
        pass
    out, err = capsys.readouterr()
    assert out.strip().startswith(__version__)


@pytest.mark.parametrize("args", ("-h", "--help"))
def test_help(capsys, args):
    try:
        run_test([args])
    except SystemExit:
        pass
    out, err = capsys.readouterr()
    assert out.startswith("usage: ")
