#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

APP_MCP="$(command -v memory-channels-processor)" || true

#/////////////////////////

if [ ! -e "$APP_MCP" ] ; then
    echo "The executable 'memory-channels-processor' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Check version and usage information"
"$APP_MCP" --version
"$APP_MCP" -h

#/////////////////////////

echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////