#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

APP_GIT="$(command -v git)" || true

#/////////////////////////

if [ ! -e "$APP_GIT" ] ; then
    echo "The executable 'git' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

IFS=$'\n'
for f in $(find "$SD" -maxdepth 1 -type f -iname '*.sh'); do
	cd "$(dirname $f)" && $APP_GIT update-index --chmod=+x "$(basename $f)"
done

$APP_GIT ls-tree HEAD

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////
