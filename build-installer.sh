#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "Building installer"
"$SD/src/pyinstaller.py" "$@"
echo "--------------------"

echo "Done"
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////