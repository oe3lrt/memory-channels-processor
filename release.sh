#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

APP_GIT="$(command -v git)" || true
APP_GIT_CLIFF="$(command -v git-cliff)" || true
APP_SED="$(command -v sed)" || true

#/////////////////////////

if [ ! -e "$APP_GIT" ] ; then
    echo "The executable 'git' wasn't found!"
    exit
fi

if [ ! -e "$APP_GIT_CLIFF" ] ; then
    echo "The executable 'git-cliff' wasn't found!"
    exit
fi

if [ ! -e "$APP_SED" ] ; then
    echo "The executable 'sed' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

# Check if current working dir is clean before switching branches
STATUS=$($APP_GIT status --porcelain --untracked-files=no)
if [ -n "$STATUS" ]; then
    echo "There are modified files present. Aborting!"
    echo ""
    echo "Files affected:"
    echo "$STATUS"

    exit 1
fi

#/////////////////////////

# Switch to development branch
$APP_GIT fetch --all --tags
$APP_GIT checkout "development"

echo "----------------------------------"
$APP_GIT status

#/////////////////////////

echo "----------------------------------"
echo "Preparing release..."
echo ""

# Get latest release tag
if [[ -n $($APP_GIT tag --sort=-v:refname | grep -E -m 1 '^[0-9]+.[0-9]+.[0-9]+$') ]]; then
    latestReleaseTag=$($APP_GIT tag --sort=-v:refname | grep -E -m 1 '^[0-9]+.[0-9]+.[0-9]+$')
    echo "Latest local release tag: ${latestReleaseTag}"
    echo "----------------------------------"
    echo "Please enter new release tag:"

    # Read user input
    read -r newReleaseTag

    # Check if new tag is greater then latest tag
    greatestReleaseTag=$(echo -e "$newReleaseTag\n$latestReleaseTag" | sort --version-sort | tail -n 1)
    #echo "INFO: Greatest release tag: $greatestReleaseTag"
    if [[ $greatestReleaseTag != "$newReleaseTag" ]] ; then
        echo "----------------------------------"
        echo "ERROR: New version $newReleaseTag has to be greater than latest version $greatestReleaseTag!"
        echo "----------------------------------"
        exit 1
    fi
else
    # First release tag
    echo "Please enter the first release tag:"

    # Read user input
    read -r newReleaseTag
fi

echo "----------------------------------"

#/////////////////////////

# Check if version is valid
reg='^[0-9]+.[0-9]+.[0-9]+$'
if ! [[ $newReleaseTag =~ $reg ]] ; then
    echo "ERROR: New release tag $newReleaseTag is not valid! Format: [0-9]+.[0-9]+.[0-9]+"
    echo "----------------------------------"
    exit 1
fi

# Check if a release tag already exists for version
if [[ $($APP_GIT tag -l "$newReleaseTag") ]]; then
    echo "ERROR: Git release tag for version $newReleaseTag already exists!"
    echo "----------------------------------"
    exit 1
fi

#/////////////////////////

echo "Update Changelog..."

"$APP_GIT_CLIFF" --workdir "$SD/" --repository "$SD/" --config "$SD/cliff.toml" --tag="${newReleaseTag}" --output CHANGELOG.adoc
$APP_GIT add CHANGELOG.adoc

"$APP_GIT_CLIFF" --workdir "$SD/" --repository "$SD/" --config "$SD/cliff.toml" --tag="${newReleaseTag}" --output docs/modules/ROOT/partials/changelog.adoc --strip all
$APP_GIT add docs/modules/ROOT/partials/changelog.adoc


echo "Update version information and reset variables..."

# Update Python version information
$APP_SED -i "/\(^version = .*$\)/c\version = \"${newReleaseTag}\"" pyproject.toml
$APP_GIT add pyproject.toml

# Update Antora version information
$APP_SED -i "/\(^version: .*$\)/c\version: \"${newReleaseTag}\"" docs/antora.yml
$APP_SED -i "/\(^display_version: .*$\)/c\display_version: \"${newReleaseTag}\"" docs/antora.yml
$APP_SED -i "/\(^prerelease: .*$\)/c\prerelease: false" docs/antora.yml
$APP_GIT add docs/antora.yml

# Reset variables
$APP_SED -i '5,20s/SKIP_TESTS: "true"/SKIP_TESTS: "false"/g' .gitlab-ci.yml
$APP_GIT add .gitlab-ci.yml

$APP_GIT status
echo "----------------------------------"

echo "Commit and push..."

$APP_GIT -c core.askpass=true commit -m "Release: Update version"
$APP_GIT push origin -o ci.skip # Prevent triggering the pipeline again

echo "----------------------------------"

echo "Try to merge branch 'development' into 'main'..."
$APP_GIT checkout "main"
$APP_GIT pull

{ # try
    $APP_GIT merge --ff-only "development"
} || { # catch
    echo "----------------------------------"
    echo "ERROR: Merge to branch 'main' failed!"
    echo "----------------------------------"
    exit 1
}
echo "----------------------------------"

# Create tag
echo "Creating tag ${newReleaseTag}..."
$APP_GIT tag -a -m "Release: Create tag '${newReleaseTag}'" "${newReleaseTag}"
$APP_GIT push origin "main"
$APP_GIT push origin "${newReleaseTag}"
echo "----------------------------------"

#/////////////////////////

echo "Switch back to development branch..."
$APP_GIT checkout "development"
echo "----------------------------------"

#/////////////////////////

versionDevPython="${newReleaseTag}.dev"
versionDevAntora="snapshot"

#/////////////////////////

echo "Update version information..."

# Update Python version information
$APP_SED -i "/\(^version = .*$\)/c\version = \"${versionDevPython}\"" pyproject.toml
$APP_GIT add pyproject.toml

# Update Antora version information
$APP_SED -i "/\(^version: .*$\)/c\version: \"${versionDevAntora}\"" docs/antora.yml
$APP_SED -i "/\(^display_version: .*$\)/c\display_version: \"${versionDevAntora}\"" docs/antora.yml
$APP_SED -i "/\(^prerelease: .*$\)/c\prerelease: false" docs/antora.yml
$APP_GIT add docs/antora.yml

$APP_GIT status
echo "----------------------------------"

echo "Commit and push..."

$APP_GIT -c core.askpass=true commit -m "Release: Update version to \"${versionDevPython}\" / \"${versionDevAntora}\""
$APP_GIT push origin

echo "----------------------------------"

#/////////////////////////

echo "Finished!"
echo "----------------------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////