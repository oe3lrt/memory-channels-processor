#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

DATE_FORMAT="%Y-%m-%d %H:%M:%S"

#/////////////////////////

if [ "$(whoami)" != "root" ] ; then
	echo "[$(date +"$DATE_FORMAT")] Sorry, you are not root"
	exit 1
fi

#/////////////////////////

# For details on file permission issues see https://devblogs.microsoft.com/commandline/chmod-chown-wsl-improvements/
echo "Set 'metadata' flag for WSL mounts"
if [ -d "/mnt/c" ] ; then
	umount /mnt/c || true && mount -t drvfs C: /mnt/c -o metadata || true
fi
if [ -d "/mnt/d" ] ; then
	umount /mnt/d || true && mount -t drvfs D: /mnt/d -o metadata || true
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

# Directories
echo "Set permissions for '$SD'"
chown -R "${SUDO_USER:-$USER}":"${SUDO_USER:-$USER}" "$SD"
chmod -R "ug=rwx" "$SD/"*.sh

echo "Set permissions for '$SD/venv'"
mkdir -p "$SD/venv"
chown "${SUDO_USER:-$USER}":"${SUDO_USER:-$USER}" "$SD/venv"
chmod -R ug+rwx "$SD/venv"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////
