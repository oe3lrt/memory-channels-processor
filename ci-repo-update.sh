#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

APP_GIT="$(command -v git)" || true

#/////////////////////////

if [ ! -e "$APP_GIT" ] ; then
    echo "The executable 'git' wasn't found!"
    exit 1
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

# Check if running in GitLabCI
if [ -n "${GITLAB_CI:-}" ] && [ -n "${CODEPLUGS_GIT_COMMIT_SHA:-}" ] ; then
  echo "Update git repo to commit '$CODEPLUGS_GIT_COMMIT_SHA'..."

  $APP_GIT fetch origin
  $APP_GIT checkout -B "$CI_COMMIT_REF_NAME" "$CODEPLUGS_GIT_COMMIT_SHA"
  $APP_GIT status

  echo "--------------------"
fi

echo "Done"
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////