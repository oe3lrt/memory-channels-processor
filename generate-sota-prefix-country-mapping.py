#!/usr/bin/python3

"""
Fetch and transform SOTA country mapping
=================================

Changelog:
    - 2024-08-16 oe3lrt: Initial version

"""

# (c) 2024, OE3LRT <lukas@oe3lrt.at>

######################################################################

import argparse
import json
from _csv import QUOTE_MINIMAL
from typing import AnyStr, Any
from builtins import str

import pycountry

import pandas as pd


class SotaCountryMapping(object):
    def __init__(self):

        """ Main execution path """

        # Parse CLI arguments
        parser = self.args_parser_prepare()
        self.args = parser.parse_args()

        summits_prefix_mapping = pd.read_csv(filepath_or_buffer="https://raw.githubusercontent.com/manuelkasper/sotlas-api/master/jobs/isocodes.txt", sep=",", index_col=None, header=None, names=['association', 'alpha_2', 'continent'], quoting=QUOTE_MINIMAL, encoding='utf-8', skiprows=1)
        if summits_prefix_mapping is None:
            print("Unable to read summits mapping ('isocodes.txt')")
            exit(1)

        # Calculate data
        summits_prefix_mapping['alpha_3'] = summits_prefix_mapping.apply(self.calc_country_code, axis=1)

        # Extract data
        summits_prefix_mapping = summits_prefix_mapping[['association', 'alpha_3']]

        if self.args.verbose:
            self.print_verbose("Summits mapping overview:")
            summits_prefix_mapping.info()
            print(summits_prefix_mapping)

        # Convert to dict
        summits_prefix_mapping_dict = dict(zip(summits_prefix_mapping.association, summits_prefix_mapping.alpha_3))

        print("sota_summit_prefix_country_mapping = " + json.dumps(summits_prefix_mapping_dict, indent=4))

        self.print_verbose("Done")
        exit(0)

    def args_parser_prepare(self) -> argparse.ArgumentParser:
        parser = argparse.ArgumentParser(description='Process SOTA log files', allow_abbrev=True, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        # Flags
        parser.add_argument('--offline', dest='offline', action='store_true', help='Enable offline mode', default=False)
        parser.add_argument('--verbose', dest='verbose', action='store_true', help='Enable verbosity', default=False)

        return parser

    def print_verbose(self, output: AnyStr):
        """ Print out only if verbose is set """

        if self.args.verbose:
            print(output)

    def calc_country_code(self, row) -> str:
        result = ''
        if not is_empty(row['alpha_2']):
            country = pycountry.countries.get(alpha_2=row['alpha_2'])
            if not is_empty(country):
                result = country.alpha_3
            else:
                subdivision = pycountry.subdivisions.get(code=row['alpha_2'])
                if not is_empty(subdivision):
                    result = subdivision.country.alpha_3

        return result


def is_empty(item: Any) -> bool:
    """ Check if the item is empty """

    if item is None:
        return True

    if isinstance(item, list) and len(item) == 0:
        return True
    elif item == '':
        return True

    return False


def main():
    mapping = SotaCountryMapping()


if __name__ == "__main__":
    main()
