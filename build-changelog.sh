#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

APP_GIT_CLIFF="$(command -v git-cliff)" || true

#/////////////////////////

if [ ! -e "$APP_GIT_CLIFF" ] ; then
    echo "The executable 'git-cliff' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "Update Changelog"
"$APP_GIT_CLIFF" --workdir "$SD/" --repository "$SD/" --config "$SD/cliff.toml" --output CHANGELOG.adoc "$@"
"$APP_GIT_CLIFF" --workdir "$SD/" --repository "$SD/" --config "$SD/cliff.toml" --output docs/modules/ROOT/partials/changelog.adoc --strip all "$@"
echo "--------------------"

echo "Done"
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////