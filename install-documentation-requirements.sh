#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

if [ "$(whoami)" != "root" ] ; then
	echo "Sorry, you are not root"
	exit 1
fi

#/////////////////////////

APP_APT="$(command -v apt)" || true
APP_BUNDLE="$(command -v bundle)" || true
APP_CURL="$(command -v curl)" || true
APP_GPG="$(command -v gpg)" || true
APP_MKDIR="$(command -v mkdir)" || true
APP_TEE="$(command -v tee)" || true

#/////////////////////////

if [ ! -e "$APP_APT" ] ; then
    echo "The executable 'apt' wasn't found!"
    exit
fi

if [ ! -e "$APP_BUNDLE" ] ; then
    echo "The executable 'bundle' wasn't found!"
    exit
fi

if [ ! -e "$APP_CURL" ] ; then
    echo "The executable 'curl' wasn't found!"
    exit
fi

if [ ! -e "$APP_GPG" ] ; then
    echo "The executable 'gpg' wasn't found!"
    exit
fi

if [ ! -e "$APP_MKDIR" ] ; then
    echo "The executable 'mkdir' wasn't found!"
    exit
fi

if [ ! -e "$APP_TEE" ] ; then
    echo "The executable 'tee' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Install Ruby requirements"
su "${SUDO_USER:-$USER}" -c "$APP_BUNDLE config set path '.bundle/gems'"
su "${SUDO_USER:-$USER}" -c "$APP_BUNDLE install"

echo "--------------------"

echo "Check Node.js:"

if [ ! -e "/etc/apt/keyrings/nodesource.gpg" ] ; then
  echo " - Install Node.js apt keys"
	$APP_APT update
	$APP_APT install -y ca-certificates
	$APP_MKDIR -p /etc/apt/keyrings
	$APP_CURL -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | $APP_GPG --dearmor -o /etc/apt/keyrings/nodesource.gpg
else
  echo " - Node.js apt keys already installed"
fi

if [ ! -e "/etc/apt/sources.list.d/nodesource.list" ] ; then
  echo " - Install Node.js apt sources"
	NODE_MAJOR=20
	echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | $APP_TEE /etc/apt/sources.list.d/nodesource.list
else
  echo " - Node.js apt sources already installed"
fi

if [ ! -e "$(command -v node)" ] ; then
  echo " - Install Node.js"
	$APP_APT update
	$APP_APT install nodejs -y
else
  echo " - Node.js already installed"
fi

echo "--------------------"

APP_NPM="$(command -v npm)"
APP_NPX="$(command -v npx)"

echo "Install Antora"
# Install dependencies defined in 'package.json'
su "${SUDO_USER:-$USER}" -c "$APP_NPM update"
su "${SUDO_USER:-$USER}" -c "$APP_NPM ci"
su "${SUDO_USER:-$USER}" -c "$APP_NPX antora -v"
echo "--------------------"

echo "Done"
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////
