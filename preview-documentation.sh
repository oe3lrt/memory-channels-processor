#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

set -eu

#/////////////////////////

APP_NPM="$(command -v npm)" || true
APP_NPX="$(command -v npx)" || true
APP_ZIP="$(command -v zip)" || true

#/////////////////////////

if [ ! -e "$APP_NPM" ] ; then
    echo "The executable 'npm' wasn't found!"
    exit
fi

if [ ! -e "$APP_NPX" ] ; then
    echo "The executable 'npx' wasn't found!"
    exit
fi

if [ ! -e "$APP_ZIP" ] ; then
    echo "The executable 'zip' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

# Install dependencies defined in 'package.json'
echo "Install dependencies defined in 'package.json'"
$APP_NPM ci
$APP_NPM i http-server
echo "--------------------"

# Fake resources (for development) that are generated during the CI workflows
if [ ! -e "$SD/docs/modules/ROOT/attachments/memory-channels-processor-windows.zip" ] ; then
  echo "Fake resource 'docs/modules/ROOT/attachments/memory-channels-processor-windows.zip'"
  $APP_ZIP -r -D -v "$SD/docs/modules/ROOT/attachments/memory-channels-processor-windows.zip" "$SD/LICENSE.txt"
  echo "--------------------"
fi

# Run Antora
echo "Run Antora"
$APP_NPX antora --fetch --cache-dir "$SD/.cache/antora" --redirect-facility=gitlab --to-dir="$SD/public" --stacktrace --attribute generated_timestamp="$(date -u)" antora-playbook.yml "$@"
echo "--------------------"

# Preview site
echo "Preview site"
$APP_NPX http-server "$SD/public" -c-1
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////