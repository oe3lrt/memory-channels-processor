Memory Channels Processor
====

This repository contains code for the "Memory Channels Processor" and preprocessed codeplugs for various amateur radio devices.

The "Memory Channels Processor" is a tool to process data from various sources holding information about amateur radio repeaters and convert them to formats that can be used for importing into amateur radio devices (via specific configuration software)

An extended description of the tool and its usage can be found under https://oe3lrt.gitlab.io/memory-channels-processor
